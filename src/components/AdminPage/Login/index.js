import React from 'react';
import { inject, observer } from 'mobx-react';
import Button from '../Components/Button';

@inject("authStore")
@observer
export default class Login extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      login: "",
      password: ""
    }
  }

  render() {
    const { login, password } = this.state;
    return (
      <div align="center" style={{marginTop:"5%"}}>
        <h3>Please, login as Administrator</h3>
        <div style={{margin:"2%"}}>
          <input required  style={{fontSize:"18px"}}
            placeholder="Enter Username"
            type="text"
            name="login"
            value={login}
            onChange={this.handleChange} />
          <input required  style={{fontSize:"18px"}}
            placeholder="Enter Password"
            type="password"
            name="password"
            value={password}
            onChange={this.handleChange} />
          <Button style={{fontSize:"18px", margin:"1%"}}
                  onClick={this.submitForm}
                  variant="outline-dark"
                  disabled={!(login && password)}
          >
                  Login
          </Button>
        </div>
      </div>
    );
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({
      [name] : value
    });
  }

  submitForm = () => {
    const { login, password } = this.state;
    this.props.authStore.login(login, password);
  }
}
