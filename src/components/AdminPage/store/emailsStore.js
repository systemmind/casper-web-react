import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete } from "../../../helpers";
import commonStore from './commonStore';
import authStore from './authStore';

class Email{
  @observable email = observable.map()
  constructor(obj={}){
    this.email.merge(obj);
  }

  @computed get id(){
    return this.email.get("id");
  }

  sync(){
    const obj = Object.assign({}, this.email.toJSON());
    const { id } = obj;
    delete obj.id;
    return Put(`${commonStore.server}/users/${authStore.user}/notifications/emails/${id}`, authStore.token, obj)
      .catch(error => commonStore.addError(error));
  }

  del(){
    return Delete(`${commonStore.server}/users/${authStore.user}/notifications/emails/${this.id}`, authStore.token)
      .catch(error => commonStore.addError(error));
  }
}

class Emails {
  @observable emails = [];
  @observable selected = null;

  load(){
    return Get(`${commonStore.server}/users/${authStore.user}/notifications/emails`, authStore.token)
      .then(action(items => this.emails = items.map(item => new Email(item))))
      .catch(error => commonStore.addError(error));
  }

  create(email){
    return Post(`${commonStore.server}/users/${authStore.user}/notifications/emails`, authStore.token, email)
      .then(action((obj) => this.emails.push(new Email(Object.assign({}, obj, email)))))
      .catch(error => commonStore.addError(error));
  }

  del(email){
    return email.del()
      .then(action(() => this.emails = this.emails.filter(item => item !== email)))
      .then(action(() => this.selected = null));
  }

  @action setSelected(email){
    this.selected = email;
  }
}

export default new Emails();