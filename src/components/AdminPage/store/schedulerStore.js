import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete } from "../../../helpers";
import commonStore from './commonStore';
import authStore from './authStore';
import dateFormat from 'dateformat';

function utc2Local(time){
  const chunks = time.split(":")
  const date = new Date();

  date.setUTCHours(parseInt(chunks[0]));
  date.setUTCMinutes(parseInt(chunks[1]));
  date.setUTCSeconds(parseInt(chunks[2]));

  const h = date.getHours().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
  const m = date.getMinutes().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
  const s = date.getSeconds().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});

  return `${h}:${m}:${s}`;
}

function local2UTC(time){
  const chunks = time.split(":");
  if (chunks.length !== 3){
    throw this.props.commonStore.addError(new Error(`Invalid date format: ${time}`));
  }

  const date = new Date();
  date.setHours(parseInt(chunks[0]));
  date.setMinutes(parseInt(chunks[1]));
  date.setSeconds(parseInt(chunks[2]));
  return `${date.getUTCHours()}:${date.getUTCMinutes()}:${date.getUTCSeconds()}`;
}

class Scheduler {
  @observable scheduler = observable.map();
  @observable emails = [];
  @observable selected = false;
  @observable testEmail = "";
  @observable testUser = null;
  measurement = -1;

  constructor(measurement, obj={}){
    if (obj.begin && obj.end){
      obj.begin = utc2Local(obj.begin);
      obj.end = utc2Local(obj.end);
    }

    this.scheduler.merge(obj);
    this.measurement = measurement;
  }

  @computed get id(){
    return this.scheduler.get("id");
  }

  @computed get questions(){
    return this.scheduler.get("questions");
  }

  @action setSelected(value){
    this.selected = value;
  }

  setQuestions(questions){
    this.scheduler.set("questions", questions);
    return this.sync();
  }

  addQuestion(question){
    const questions = this.scheduler.get("questions");
    this.scheduler.set("questions", [...questions, question]);
  }

  removeQuestion(index){
    const questions = this.scheduler.get("questions");
    this.scheduler.set("questions", questions.filter((item, idx) => (idx !== index)));
  }

  @action setTestEmail(email)
  {
    this.testEmail = email;
  }

  @action setTestUser(user)
  {
    this.testUser = parseInt(user);
  }

  sync(){
    const scheduler = Object.assign({}, this.scheduler.toJSON());
    scheduler.begin = local2UTC(scheduler.begin);
    scheduler.end = local2UTC(scheduler.end);
    delete scheduler.id;
    return Put(
      `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/schedulers/${this.id}`,
      authStore.token,
      scheduler
    )
    .catch(error => commonStore.addError(error));
  }

  @action load(){
    return Get(
      `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/schedulers/${this.id}`,
      authStore.token
    )
    .then(obj => this.scheduler.replace(obj))
    .catch(error => commonStore.addError(error));
  }

  @action loadEmails(){
    return Get(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/schedulers/${this.id}/emails`, authStore.token)
      .then(action(emails => this.emails = emails));
  }

  addEmails(emails){
    const promises = emails.map(id => (
      Post(
        `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/schedulers/${this.id}/emails/${id}`,
        authStore.token, {}
      )
    ));

    return Promise.all(promises)
        .then(action((records) => {
          const newEmails = emails.map((email, index) => Object.assign({}, {email}, records[index]))
          this.emails = [...this.emails, ...newEmails];
        }))
        .catch(error => commonStore.addError(error));
  }

  delEmails(emails){
    const promises = emails.map(email => {
      return Delete(
        `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/schedulers/${this.id}/emails/${email}`,
        authStore.token, {}
      )
    });

    return Promise.all(promises)
        .then(() => console.log(JSON.stringify(this.emails.filter(email => emails.indexOf(email.email) < 0))))
        .then(action(() => this.emails = this.emails.filter(email => emails.indexOf(email.email) < 0)))
        .catch(error => commonStore.addError(error));
  }

  async test(){
    const begin = new Date();
    const end = new Date();
    end.setDate(end.getDate() + 1);
    const dtformat = "UTC:yyyy-mm-dd";
    const survey = await Post(
      `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/employees/${this.testUser}/schedulers/${this.id}/surveys`,
      authStore.token,
      {
       begin: dateFormat(begin, dtformat),
       end: dateFormat(end, dtformat),
      }
    );

    const link = await Get(
      `${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/surveys/${survey.id}/links?employee=${this.testUser}&loader=${this.scheduler.get("loader")}`,
      authStore.token
    );

    window.open(link.link, "Test Schedule Template");
  }

  async schedule(){
    return Post(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/action/schedule`, authStore.token, {scheduler: this.id})
      .catch(error => commonStore.addError(error));
  }
}

class SchedulerStore {
  @observable schedulers = [];
  @observable newScheduler = observable.map({
    remind: 15,
    survey_expiration: 30,
    schedule_discrete: 60,
    type: 'undefined',
    loader: 'undefined'
  });

  @observable questionsPopup = false;
  @observable editPopup = false;
  @observable addPopup = false;

  load(measurement){
    return Get(`${commonStore.server}/users/${authStore.user}/measurements/${parseInt(measurement)}/schedulers`, authStore.token)
      .then(action(items => this.schedulers = items.map(item => new Scheduler(measurement, item))))
      .catch(error => commonStore.addError(error));
  }

  @action setQuestionsPopup(value){
    this.questionsPopup = value;
  }

  @action setEditPopup(value){
    this.editPopup = value;
  }

  @action setAddPopup(value){
    this.addPopup = value;
  }

  @computed get selected(){
    return this.schedulers.find(item => item.selected);
  }

  @action select(scheduler){
    this.schedulers.map(item => item.setSelected(false));
    scheduler.setSelected(true);
  }

  create(measurement){
    const scheduler = Object.assign({}, this.newScheduler.toJSON());
    scheduler.begin = local2UTC(scheduler.begin);
    scheduler.end = local2UTC(scheduler.end);
    return Post(`${commonStore.server}/users/${authStore.user}/measurements/${measurement}/schedulers`, authStore.token, scheduler)
      .then(action((obj) => this.schedulers.push(new Scheduler(Object.assign({}, this.newScheduler, obj)))))
      .catch(error => commonStore.addError(error));
  }

  del(measurement, scheduler){
    return Delete(`${commonStore.server}/users/${authStore.user}/measurements/${measurement}/schedulers/${scheduler.id}`, authStore.token)
      .then(action(() => this.schedulers = this.schedulers.filter(item => item !== scheduler)))
      .catch(error => commonStore.addError(error));
  }
}

export default new SchedulerStore();
