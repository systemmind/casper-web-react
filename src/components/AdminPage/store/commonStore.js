import { observable, action } from 'mobx';

class Common {
  @observable server = process.env.casperApi;
  @observable errors = [];

  addError = action((error) => {
    this.errors.push(error);
  });

  @action resetErrors(){
    this.errors = [];
  }
}

export default new Common();
