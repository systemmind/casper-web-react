import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete } from "../../../helpers";
import commonStore from './commonStore';
import authStore from './authStore';

class Question {
  @observable question = observable.map();

  constructor(obj={}){
    this.question.merge(obj);
  }

  @computed get name(){
    return this.question.get("name");
  }
}

class QuestionsStore {
  @observable questions = [];

  load(){
    return Get(`${commonStore.server}/users/${authStore.user}/questions`, authStore.token)
      .then(action(items => this.questions = items.map(item => new Question(item))))
      .catch(error => commonStore.addError(error));
  }
}

export default new QuestionsStore();
