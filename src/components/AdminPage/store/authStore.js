import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete, Fetch } from "../../../helpers";
import commonStore from './commonStore';

class AuthStore {
  @observable token = "";
  @observable user = null;
  @observable userObj = observable.map();
  @observable changePasswordPopup = false;
  @observable addUserPopup = false;
  @observable newUserPopup = false;
  @observable newUserId = null;
  @observable newUserName = "";
  @observable newUserPassword = "";

  constructor(){
    if (process.env.loginUser && process.env.loginPassword)
    {
      this.login(process.env.loginUser, process.env.loginPassword);
    }
  }

  login(user, passwd){
    return Fetch(`${commonStore.server}/login`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        login: user,
        password: passwd
      })
    })
    .then(action(obj => { this.token = obj.token; this.user = obj.user; } ))
    .then(() => Get(`${commonStore.server}/users/${this.user}`, this.token))
    .then(u => this.userObj.replace(u))
    .catch(error => commonStore.addError(error));
  }

  @computed get isLoggedIn(){
    return this.token != "" && this.user != null;
  }

  @computed get isRoot(){
    const str_name  = 'user';
    return this.user && this.userObj.has(str_name) && ((this.userObj.get(str_name) == 'admin') || (this.userObj.get(str_name) == 'root'))
  }

  showChangePasswordPopup = action(value => this.changePasswordPopup = !!value);

  changePassword = action(
    password => Put(`${commonStore.server}/users/${this.user}/auth/password`, this.token, {password})
      .then(({rowcount}) => { if (rowcount != 1) throw new Error("could not update pasword") })
      .catch(error => commonStore.addError(error))
    );

  showAddUserPopup = action(value => this.addUserPopup = !!value);

  createUser = action(
    (user, password) => Post(`${commonStore.server}/users`, this.token, {user, password})
      .then(action(({id}) => { this.newUserId = id; this.newUserName = user, this.newUserPassword = password; } ))
      .catch(error => commonStore.addError(error))
  );

  getUser = id => Get(`${commonStore.server}/users/${id}`, this.token)
    .catch(error => commonStore.addError(error));

  delUser = id => Delete(`${commonStore.server}/users/${id}`, this.token)
    .catch(error => commonStore.addError(error));

  resetNewUser = action(() => {
    this.newUserId = null;
    this.newUserName = "";
    this.newUserPassword = "";
  });

  showNewUserPopup = action(value => this.newUserPopup = !!value);
}

export default new AuthStore();
