import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete } from "../../../helpers";
import commonStore from './commonStore';
import authStore from './authStore';

class Measurement {
  @observable measurement = observable.map();

  constructor(obj={}){
    this.measurement.merge(obj);
  }

  edit = () => {
    const obj = this.measurement.toJSON();
    delete obj.id;
    return Put(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement.get('id')}`, authStore.token, obj)
      .catch(error => commonStore.addError(error));
  }

  start = () => {
    return Post(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement.get("id")}/action/start`, authStore.token, {})
      .then(response => this.measurement.merge(response))
      .catch(error => commonStore.addError(error));
  }

  stop = () => {
    return Post(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement.get("id")}/action/stop`, authStore.token, {})
      .then(response => this.measurement.merge(response))
      .catch(error => commonStore.addError(error));
  }
}

class MeasurementStore {
  @observable measurements = [];
  @observable editMeasurement = null;
  @observable newName = "";
  @observable newCompanyUrl = "";
  @observable newStartDate = "";
  @observable newStopDate = "";
  @observable newManagerEmail = "";
  @observable newPopup = false;

  @action setNewName(n){ this.newName = n; }
  @action setNewCompanyUrl(v){ this.newCompanyUrl = v; }
  @action setNewStartDate(d){ this.newStartDate = d; }
  @action setNewStopDate(d){ this.newStopDate = d; }
  @action setNewManagerEmail(u){ this.newManagerEmail = u; }
  @action showNewPopup(v){ this.newPopup = v; }

  @action setEditMeasurement(u){ this.editMeasurement = u; }

  load = () => {
    return Get(`${commonStore.server}/users/${authStore.user}/measurements`, authStore.token)
      .then(action(items => this.measurements = items.map(item => new Measurement(item))))
      .catch(error => commonStore.addError(error));
  }

  del = (measurement) => {
    return Delete(`${commonStore.server}/users/${authStore.user}/measurements/${measurement.measurement.get("id")}`, authStore.token)
      .then(action(() => this.measurements = this.measurements.filter(item => item !== measurement)))
      .catch(error => commonStore.addError(error));
  }

  create = (obj) => {
    const newMeasurement = obj || {
      name: this.newName,
      company_url: this.newCompanyUrl,
      start_date: this.newStartDate,
      stop_date: this.newStopDate,
      manager_email: this.newManagerEmail
    }

    return Post(`${commonStore.server}/users/${authStore.user}/measurements`, authStore.token, newMeasurement)
      .then(action(item => {
        this.measurements = this.measurements.concat([new Measurement(item)])
      }))
      .catch(error => commonStore.addError(error));
  }
}

export default new MeasurementStore();