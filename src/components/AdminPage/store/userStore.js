import { observable, action, computed } from 'mobx';
import { Get, Put, Post, Delete } from "../../../helpers";
import commonStore from './commonStore';
import authStore from './authStore';

class User {
  @observable user = observable.map();
  @observable selected = false;
  @observable newEmail = "";

  measurement = -1;

  constructor(measurement, obj={}){
    this.user.merge(obj);
    this.measurement = measurement;
  }

  @action select(v){ this.selected = v; }
  @action setNewEmail(v){ this.newEmail = v; }

  edit = () => {
    const user = this.user.toJSON();
    delete user.id;
    delete user.date;
    return Put(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/employees/${this.user.get('id')}`, authStore.token, user)
      .catch(error => commonStore.addError(error));
  }
}

class UserStore {
  @observable allUsers = [];
  @observable editUser = null;
  @observable measurement = -1;
  @observable authorized = true;
  @observable sortKey = 'id';
  @observable allChecked = false;

  @action setMeasurement(m){ this.measurement = parseInt(m); }
  @action setAuthorized(v){ this.authorized = v; }
  @action setSort(k){ this.sortKey = k; }
  @action setEditUser(u){ this.editUser = u; }

  @computed get users(){
    return this.allUsers.filter((item)=>(this.authorized ? item.user.get('authorized') : true))
    .sort((item1, item2)=>(
      !item1.user.get(this.sortkey) && item2.user.get(this.sortkey) ? -1 :
      !item2.user.get(this.sortkey) && item1.user.get(this.sortkey) ?  1 :
        (item1.user.get(this.sortkey) > item2.user.get(this.sortkey)) ? 1 :
        (item1.user.get(this.sortkey) < item2.user.get(this.sortkey)) ? -1 : 0
    ));
  }

  load = () => {
    return Get(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/employees?emails=1&devices=1`, authStore.token)
      .then(action(items => this.allUsers = items.map(item => new User(this.measurement, item))))
      .catch(error => commonStore.addError(error));
  }

  del(user){
    return Delete(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/employees/${user.user.get("id")}`, authStore.token)
      .then(action(() => this.allUsers = this.users.filter(item => item !== user)))
      .catch(error => commonStore.addError(error));
  }

  create = ({emails, phones}) => {
    const impl = items => {
      const body = items.map(item => Object.assign({authorized: true}, item));
      return Post(`${commonStore.server}/users/${authStore.user}/measurements/${this.measurement}/employees`, authStore.token, body)
        .then(action(items => {
          console.log(this.allUsers);
          this.allUsers = this.allUsers.concat(items.map(id => new User(this.measurement, {id})))
          console.log(this.allUsers);
        }));
    }

    const promises = [];
    if (emails){
      promises.push(impl(emails.map(item =>({emails: [item]}))));
    }

    if (phones){
      promises.push(impl(phones.map(phone => ({phone}))));
    }

    return Promise.all(promises).catch(error => commonStore.addError(error));
  }

  sendNotification = (body) => {
    const receivers = this.users.filter(item => item.selected).map(user => user.user.get('id'));
    Object.keys(body).map(key => body[key].users = receivers);  // ----------------> use reduce?
    return Post(`${commonStore.server}/users/${authStore.user}/notifications`, authStore.token, body)
      .catch(error => commonStore.addError(error));
  }

  selectAll = action((v) => {
    this.users.map(u => u.select(v));
    this.allChecked = v;
  });
}

export default new UserStore();