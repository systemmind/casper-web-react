import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'reactjs-popup';
const { generate } = require('generate-password');
import Button from '../../Components/Button';


export default class AddUserPopup extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    onCreate: PropTypes.func.isRequired
  }

  state = {
    name: "",
    password: ""
  };

  render(){
    const { open } = this.props;
    const { name, password } = this.state;
    return (
      <Popup
        open={open}
        closeOnDocumentClick
      >
        <center>
          <label>Enter new user name</label>
          <input type="text" name="name" placeholder="New user name" value={name} onChange={this.handleChange} />
          {
            password ? (
              <div><label>User's password</label><div>{password}</div><br/></div>
            ) : (
              <Button onClick={this.handleMakePassword}>Create password</Button>
            )
          }
          <Button onClick={this.handleCreate} disabled={!(name && password)}>Create</Button>
        </center>
      </Popup>
    );
  }

  handleChange = event => {
    const {name, value} = event.target;

    this.setState({
      [name] : value
    });
  }

  handleMakePassword = el => {
    const password = generate({length: 10, numbers: true, lowercase: true, uppercase: true})
    this.setState({password});
  }

  handleCreate = () => {
    const { name, password } = this.state;
    this.props.onCreate(name, password);
  }
}
