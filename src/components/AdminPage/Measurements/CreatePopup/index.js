import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'reactjs-popup';
import Button from '../../Components/Button';

export default class CreatePopup extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    onCreate: PropTypes.func.isRequired
  }

  constructor(props){
    super(props);
    this.initialState = {
      name: '',
      company_url: '',
      start_date: null,
      stop_date: null,
      started: 0,
      manager_email: '',
      welcome_email: "no email yet"
    };

    this.state = this.initialState;
  }

  render(){
    const { open } = this.props;
    const { name, company_url, start_date, stop_date, manager_email } = this.state;
    return (
      <Popup
        open={open}
        closeOnDocumentClick
      >
        <center>
          <label>Company's Name</label>
          <input type="text" name="name" value={name} onChange={this.handleChange} />
          <label>Company's Website</label>
          <input type="text" name="company_url" value={company_url} onChange={this.handleChange} />
          <label>Measurement's Starting Date</label>
          <input type="date" name="start_date" value={start_date} onChange={this.handleChange} />
          <label>Measurement's Ending Date</label>
          <input type="date" name="stop_date" value={stop_date} onChange={this.handleChange} />
          <label>Company's Manager E-mail</label>
          <input type="text" name="manager_email" value={manager_email} onChange={this.handleChange}/>
          <Button onClick={this.handleCreate}>Create</Button>
        </center>
      </Popup>
    );
  }

  handleChange = event => {
    const {name, value} = event.target;

    this.setState({
      [name] : value
    });
  }

  handleCreate = () => {
    const { onCreate } = this.props;
    onCreate(this.state);
    this.setState(this.initialState);
  }
}
