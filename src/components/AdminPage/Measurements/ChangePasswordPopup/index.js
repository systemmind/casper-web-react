import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'reactjs-popup';
import Button from '../../Components/Button';

export default class CreatePopup extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    onChange: PropTypes.func.isRequired
  }

  state = {
    currentPassword: null,
    newPassword: null,
    repeatPassword: null,
    showPasswords: false
  };

  render(){
    const { open } = this.props;
    const { currentPassword, newPassword, repeatPassword, showPasswords } = this.state;
    return (
      <Popup
        open={open}
        closeOnDocumentClick
      >
        <center>
          <label>Enter current password</label>
          <input type={showPasswords ? "text" : "password"} name="currentPassword" placeholder="Current password" value={currentPassword} onChange={this.handleChange} />
          <label>Enter new password</label>
          <input type={showPasswords ? "text" : "password"} name="newPassword" placeholder="New password" value={newPassword} onChange={this.handleChange} />
          <label>Repeat new password</label>
          <input type={showPasswords ? "text" : "password"} name="repeatPassword" placeholder="Repeat new password" value={repeatPassword} onChange={this.handleChange} />
          <label><input type="checkbox" value={showPasswords} onClick={this.handleShowPasswords}/> Show passwords</label>
          <Button onClick={this.handlePasswordChange} disabled={!(currentPassword && newPassword && repeatPassword && newPassword == repeatPassword)}>Change</Button>
        </center>
      </Popup>
    );
  }

  handleChange = event => {
    const {name, value} = event.target;

    this.setState({
      [name] : value
    });
  }

  handlePasswordChange = () => {
    const { onChange } = this.props;
    onChange(this.state.newPassword);
    this.setState({currentPassword: null, newPassword: null, repeatPassword: null});
  }

  handleShowPasswords = el => {
    const { target: { checked } } = el;
    this.setState({showPasswords: checked});
  }
}
