import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import Button from '../Components/Button';
import CreatePopup from './CreatePopup';
import ChangePasswordPopup from './ChangePasswordPopup';
import AddUserPopup from './AddUserPopup';
import NewUserPopup from './NewUserPopup';
import './style.css';

@inject("authStore")
@inject("measurementStore")
@withRouter
@observer
export default class Measurements extends React.Component {
  componentDidMount(){
    const { measurementStore } = this.props;
    measurementStore.load();
  }

  render(){
    const { measurementStore, authStore } = this.props;
    return (
      <div className='casper-measurement'>
        <Button onClick={() => measurementStore.showNewPopup(true)}>Add</Button>
        <Button onClick={measurementStore.load}>Update</Button>
        <Button onClick={this.handleEmails}>Emails</Button>
        <Button onClick={() => authStore.showChangePasswordPopup(true)}>Change password</Button>
        {
          authStore.isRoot && <Button onClick={() => authStore.showAddUserPopup(true)}>Add user</Button>
        }
        <div className='casper-measurement-current-user'>You are logged in as <b>{authStore.userObj.get('user')}</b>, your user id is <b>{authStore.user}</b></div>
        <table>
          { this.renderTableHead() }
          { this.renderTableBody() }
        </table>
        <CreatePopup open={measurementStore.newPopup} onCreate={this.handleCreate}/>
        <ChangePasswordPopup open={authStore.changePasswordPopup} onChange={this.handleChangePassword}/>
        <AddUserPopup open={authStore.addUserPopup} onCreate={this.handleAddUser}/>
        <NewUserPopup open={authStore.newUserPopup} />
      </div>
    );
  }

  renderTableHead(){
    const { isRoot } = this.props.authStore;
    return (
      <thead>
        <tr>
          <th>Id</th>
          { isRoot && <th>User</th> }
          <th>Name</th>
          <th>Start</th>
          <th>End</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
    );
  }

  renderTableBody(){
    const { measurements } = this.props.measurementStore;
    return (
      <tbody>
        {
          measurements.map((item) => this.renderTableRow(item))
        }
      </tbody>
    );
  }

  renderTableRow(item){
    const { measurementStore, authStore } = this.props;
    const { isRoot } = authStore;
    return (
      <tr key={item.measurement.get('id')}>
        <td>{item.measurement.get('id')}</td>
        { isRoot && <td>{item.measurement.get('user')}</td> }
        <td>{item.measurement.get('name')}</td>
        <td>{item.measurement.get('start_date')}</td>
        <td>{item.measurement.get('stop_date')}</td>
        <td>{item.measurement.get('started') == 1? 'active' : 'inactive'}</td>
        <td>
          <Button onClick={item.measurement.get('started') ? item.stop : item.start}>
            {item.measurement.get('started') ? 'Stop' : 'Start'}
          </Button>
          <Button type='routelink' to={`/measurement/${item.measurement.get('id')}`}>Edit</Button>
          <Button type='routelink' to={`/measurement/${item.measurement.get('id')}/users`}>Employees</Button>
          <Button type="routelink" to={`/measurement/${item.measurement.get('id')}/schedulers`}>Schedulers</Button>
          <Button onClick={() => measurementStore.del(item)}>Delete</Button>
        </td>
      </tr>
    );
  }

  handleCreate = (item) => {
    const { measurementStore } = this.props;
    measurementStore.create(item);
    measurementStore.showNewPopup(false);
  }

  handleEmails = () => {
    this.props.history.push('/emails');
  }

  handleChangePassword = creds => {
    const { authStore } = this.props;
    authStore.changePassword(creds)
      .then(() => authStore.showChangePasswordPopup(false))
  }

  handleAddUser = (name, password) => {
    const { authStore } = this.props;
    authStore.createUser(name, password)
      .then(() => authStore.getUser(authStore.newUserId))
      .then(() => authStore.showAddUserPopup(false))
      .then(() => authStore.showNewUserPopup(true));
  }
}
