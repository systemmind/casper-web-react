import React from 'react';
import Popup from 'reactjs-popup';
import { inject, observer } from 'mobx-react';
import Button from '../../Components/Button';


@inject("authStore")
@observer
export default class NewUserPopup extends React.Component {
  render(){
    const { authStore } = this.props;
    const { newUserPopup, newUserId, newUserName, newUserPassword } = authStore;
    return (
      <Popup
        open={newUserPopup}
        closeOnDocumentClick
        onClose={() => authStore.showNewUserPopup(false)}
      >
        <center>
          <h1>New user created</h1>
          <label>Name</label>
          <label>{ newUserName }</label>
          <label>Password</label>
          <label>{ newUserPassword }</label>
          <Button onClick={this.handleDeleteUser}>Delete User</Button>
        </center>
      </Popup>
    );
  }

  handleDeleteUser = (el) => {
    const { authStore } = this.props;
    const { newUserId } = authStore;
    return authStore.delUser(newUserId)
      .then(authStore.resetNewUser)
      .then(() => authStore.showNewUserPopup(false));
  }
}
