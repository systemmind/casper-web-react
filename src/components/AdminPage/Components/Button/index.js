import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';
import "./style.css";

export default class Button extends React.Component {
  static propTypes = {
    type: PropTypes.string,
    to: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
  }

  constructor(props){
    super(props);
  }

  render(){
    const { type } = this.props;

    switch(type){
      case "hyperlink": return this.renderExtHyperLink()
      case "routelink": return this.renderRouteLink()
      default: return this.renderDefault()
    }
  }

  renderExtHyperLink(){
    const { to, children, disabled } = this.props;
    return (
      <div className="casper-admin-button-block">
        [<a disabled={disabled} href={to} target="_blank">{children}</a>]
      </div>
    );
  }

  renderRouteLink(){
    const { to, children, disabled } = this.props;
    return (
      <div className="casper-admin-button-block">
        [<Link disabled={disabled} to={to}>{children}</Link>]
      </div>
    );
  }

  renderDefault(){
    const { onClick, children, disabled } = this.props;
    const buttonClass = cn({
      "casper-admin-button-default": !disabled,
      "casper-admin-button-default-disabled": disabled
    });

    return (
      <div className="casper-admin-button-block">
        [<span disabled={disabled} className={buttonClass} onClick={!disabled ? onClick : undefined}>{children}</span>]
      </div>
    )
  }
}