import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Button from '../Button';
import './style.css';

export default class List extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    className: PropTypes.string,
    items: PropTypes.array.isRequired,
    highlight: PropTypes.bool,
    btn: PropTypes.string,
    onSelect: PropTypes.func,
    onBtnClick: PropTypes.func
  }

  constructor(props){
    super(props);
    this.state = {}
  }

  render(){
    const { className, items, title } = this.props;
    return (
      <div className={className}>
        <center><h1>{ title }</h1></center>
        {
          items.map((item, index) => this.renderItem(item, index))
        }
      </div>
    );
  }

  renderItem(item, index){
    const { highlight, btn } = this.props;
    const className = cn({
      'casper-list-item': true,
      'casper-list-item-selected': highlight && this.state.hasOwnProperty(item) && this.state[item]
    });

    const key = `${item}_${index}`;
    return (
      <div key={key} className={className} onClick={() => this.handleItemClick(item, index)}>
        {
          btn && (<Button onClick={() => this.handleBtnClick(item, index)}>{ btn }</Button>)
        }
        <div>{ item }</div>
      </div>
    );
  }

  handleItemClick = (item, index) => {
    Object.keys(this.state).map(key => this.state[key] = false);
    this.setState({
      [item]: true
    });

    const { onSelect } = this.props;
    if (onSelect) onSelect(index, item);
  }

  handleBtnClick = (item, index) => {
    const { onBtnClick } = this.props;
    if (onBtnClick) onBtnClick(index, item);
  }
}