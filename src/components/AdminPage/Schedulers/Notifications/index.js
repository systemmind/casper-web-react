import React from 'react';
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import Button from '../../Components/Button';
import './style.css';

@inject("measurementStore")
@inject("schedulerStore")
@inject("emailsStore")
@withRouter
@observer
export default class Notifications extends React.Component {
  constructor(props){
    super(props);

    const { selected } = this.props.schedulerStore;
    if (!selected.emails.length){
      selected.loadEmails();
    }

    const { emails } = this.props.emailsStore;
    if (!emails.length){
      this.props.emailsStore.load();
    }
  }

  render(){
    const { emails } = this.props.emailsStore;
    const { measurements } = this.props.measurementStore;
    const { selected } = this.props.schedulerStore;
    const measurement = measurements.find(item => item.measurement.get('id') == selected.measurement);
    return (
      <div>
        <Button onClick={this.handleBack}>Back</Button>
        <Button onClick={this.handleUpdate}>Update</Button>
        <div>
          <table>            
            <thead>
              <tr>
                <td className="casper-sched-notif-checkbox"><input type="checkbox" name="checkall" onChange={this.handleChangeAll}/></td>
                <td className="casper-sched-notif-id">email</td>
                <td className="casper-sched-notif-title">title</td>
                <td>body</td>
              </tr>
            </thead>
            <tbody>
              {
                emails.filter(item => item.email.get('user') == measurement.measurement.get('user')).map(email => this.renderRow(email))
              }
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  renderRow(email){
    const checked = this.props.schedulerStore.selected.emails.find(
      item => {
        return item.email == email.id;
      }
    ) ? true : false;

    return (
      <tr key={email.id}>
        <td><input type="checkbox" name={email.id} checked={checked} onChange={this.handleChange}/></td>
        <td>{email.id}</td>
        <td>{email.email.get("title")}</td>
        <td>{email.email.get("body")}</td>
      </tr>
    );
  }

  handleChangeAll = () => {
    const { checked } = event.target;
    const { schedulerStore, emailsStore } = this.props;
    const scheduler = schedulerStore.selected;
    const toRemove = [];
    const toAdd = [];
    const allEmailsIds = emailsStore.emails.map(email => email.id);
    const schedEmailsId = scheduler.emails.map(e => e.email);
    allEmailsIds.map(
      id => schedEmailsId.indexOf(id) < 0 ? toAdd.push(id) : toRemove.push(id)
    );

    if (checked){
      scheduler.addEmails(toAdd);
    }
    else {
      scheduler.delEmails(toRemove);
    }
  }

  handleChange = (event) => {
    const { name, checked } = event.target;
    if (checked){
      this.props.schedulerStore.selected.addEmails([parseInt(name)]);
    }
    else {
      this.props.schedulerStore.selected.delEmails([parseInt(name)]);
    }
  }

  handleUpdate = () => {
    this.props.schedulerStore.selected.loadEmails();
  }

  handleBack = () => {
    this.props.history.goBack();
  }
}
