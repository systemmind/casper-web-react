import React from 'react';
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import Button from '../../Components/Button';
import './style.css';

@inject("schedulerStore")
@withRouter
@observer
export default class Templates extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    const { measurement } = this.props.match.params;
    const { selected } = this.props.schedulerStore;
    const { id, scheduler, testUser } = selected;
    return (
      <div>
        <Button onClick={this.handleBack}>Back</Button>
        <Button onClick={this.handleUpdate}>Update</Button>
        <h4>Measurement #{measurement} | Scheduler #{id} | Template</h4>
        <textarea className="casper-scheduler-template-area" value={scheduler.get("template")} onChange={this.handleChange} />
        <div className="casper-scheduler-template-controls">
          <Button onClick={this.handleSave}>Save</Button>
          <Button onClick={this.handleTest} disabled={!testUser}>Check</Button>
          User ID:<input className="casper-scheduler-template-test-input" type="number" placeholder="1" value={testUser} onChange={this.handleTestUserChange}></input>
        </div>
      </div>
    );
  }

  handleChange = (e) => {
    const { value } = e.target;
    this.props.schedulerStore.selected.scheduler.set("template", value);
  }

  handleUpdate = () => {
    this.props.schedulerStore.selected.load();
  }

  handleSave = () => {
    this.props.schedulerStore.selected.sync();
  }

  handleTest = () => {
    this.props.schedulerStore.selected.test();
  }

  handleTestEmailChange = (e) => {
    const { value } = e.target;
    this.props.schedulerStore.selected.setTestEmail(value);
  }

  handleTestUserChange = (e) => {
    const { value } = e.target;
    this.props.schedulerStore.selected.setTestUser(value);
  }

  handleBack = () => {
    this.props.history.goBack();
  }
}
