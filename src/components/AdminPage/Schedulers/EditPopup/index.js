import React from 'react';
import Popup from 'reactjs-popup';
import Button from '../../Components/Button';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import './style.css';

@inject("commonStore")
@inject("schedulerStore")
@withRouter
@observer
export default class EditPopup extends React.Component {
  render(){
    const { editPopup, addPopup, selected, newScheduler } = this.props.schedulerStore;
    const scheduler = editPopup ? selected.scheduler : newScheduler;
    const handleChange = editPopup ? this.handleEditChange : this.handleAddChange;
    const begin = scheduler.get("begin");
    const end = scheduler.get("end");
    return (
      <Popup
        open={editPopup || addPopup}
        onClose={this.handleClose}
        closeOnDocumentClick
      >
        <center>
          <div>
            <div className="casper-sheduler-popup-left-col">
              <span>Begin, hh:mm:ss</span>
              <input type="text" placeholder="hh:mm:ss" name="begin" value={begin} onChange={handleChange} />
              <span>End, hh:mm:ss</span>
              <input type="text" placeholder="hh:mm:ss" name="end" value={end} onChange={handleChange} />
              <span>Survey expiration, minutes</span>
              <input type="text" name="survey_expiration" value={scheduler.get("survey_expiration")} onChange={handleChange} />
              <span>Remind, minutes</span>
              <input type="text" name="remind" value={scheduler.get("remind")} onChange={handleChange} />
            </div>
            <div className="casper-sheduler-popup-right-col">
              <span>Schedule discrete, seconds</span>
              <input type="text" name="schedule_discrete" value={scheduler.get("schedule_discrete")} onChange={handleChange} />
              <span>Type</span>
              <select name="type" value={scheduler.get("type")} onChange={handleChange}>
                <option value="undefined"></option>
                <option value="onetime">one-time</option>
                <option value="cyclic">cyclic</option>
                <option value="manual">manual</option>
              </select>
              <span>Loader</span>
              <select name="loader" value={scheduler.get("loader")} onChange={handleChange}>
                <option value="undefined"></option>
                <option value="morning">morning</option>
                <option value="afternoon">afternoon</option>
                <option value="evening">evening</option>
                <option value="singleevening">singleevening</option>
              </select>
            </div>
          </div>
          <div>
            <Button disabled={!(this.isValid(scheduler))} onClick={editPopup ? this.handleEdit : this.handleAdd}>
              {editPopup ? "Edit" : "Add"}
            </Button>
          </div>
        </center>
      </Popup>
    );
  }

  isValid = (scheduler) => {
    const begin = scheduler.get("begin");
    const end = scheduler.get("end");
    const expiration = scheduler.get("survey_expiration");
    const remind = scheduler.get("remind");
    const descrete = scheduler.get("schedule_discrete");
    const type = scheduler.get("type");
    const loader = scheduler.get("loader");

    const timeRegex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/g;
    return (
      begin && begin.match(timeRegex)) && (end && end.match(timeRegex) &&
      expiration > 0 && remind > 0 && descrete > 0 &&
      type !== 'undefined' && loader !== 'undefined'
    );
  }

  handleEditChange = event => {
    const {name, value} = event.target;
    this.props.schedulerStore.selected.scheduler.set(name, value);
  }

  handleAddChange = event => {
    const {name, value} = event.target;
    this.props.schedulerStore.newScheduler.set(name, value);
  }

  handleEdit = (obj) => {
    this.props.schedulerStore.selected.sync()
      .then(() => {
        this.props.schedulerStore.setEditPopup(false);
        this.props.schedulerStore.setAddPopup(false);
      });
  }

  handleAdd = () => {
    const { measurement } = this.props.match.params;
    this.props.schedulerStore.create(measurement)
      .then(() => {
        this.props.schedulerStore.setEditPopup(false);
        this.props.schedulerStore.setAddPopup(false);
      });
  }

  handleClose = () => {
    this.props.schedulerStore.setEditPopup(false);
    this.props.schedulerStore.setAddPopup(false);
  }
}
