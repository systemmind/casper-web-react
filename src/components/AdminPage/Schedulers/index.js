import React from 'react';
import EditPopup from './EditPopup';
import Button from '../Components/Button';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import './style.css';

@inject("schedulerStore")
@withRouter
@observer
export default class Scheduler extends React.Component {
  componentDidMount(){
    const { schedulerStore } = this.props;
    const { measurement } = this.props.match.params;
    schedulerStore.load(measurement);
  }

  render(){
    const { measurement } = this.props.match.params;
    return (
      <div>
        <Button onClick={this.handleBack}>Back</Button>
        <Button onClick={this.handleUpdate}>Update</Button>
        <Button onClick={this.handleAdd}>Add</Button>
        <h4>Measurement #{measurement} | Schedulers</h4>
        <table>
          {this.renderTableHeader()}
          {this.renderTableBody()}
        </table>
        <EditPopup />
      </div>
    );
  }

  renderTableHeader(){
    return (
      <thead>
        <tr>
          <td>Id</td>
          <td>Type</td>
          <td>Begin</td>
          <td>End</td>
          <td>Survey expiration</td>
          <td>Schedule discrete</td>
          <td>Remind</td>
          <td>Loader</td>
          <td className="casper-scheduler-col-controls"></td>
        </tr>
      </thead>
    )
  }

  renderTableBody(){
    const { schedulers } = this.props.schedulerStore;
    return (
      <tbody>
        { schedulers.map(item => this.renderTableRow(item)) }
      </tbody>
    );
  }

  renderTableRow(sched){
    const { id, scheduler } = sched;
    const type = scheduler.get("type");
    return (
      <tr key={id}>
        <td>{id}</td>
        <td>{type}</td>
        <td>{scheduler.get("begin")}</td>
        <td>{scheduler.get("end")}</td>
        <td>{scheduler.get("survey_expiration")}</td>
        <td>{scheduler.get("schedule_discrete")}</td>
        <td>{scheduler.get("remind")}</td>
        <td>{scheduler.get("loader")}</td>
        <td>
          <Button onClick={() => this.handleEdit(sched)}>Edit</Button>
          <Button onClick={() => this.handleTemplate(sched)}>Template</Button>
          <Button onClick={() => this.handleNotifications(sched)}>Notifications</Button>
          <Button onClick={() => this.handleDelete(sched)}>Delete</Button>
          {
            type == 'manual' && (
              <Button onClick={() => sched.schedule()}>Schedule</Button>
            )
          }
        </td>
      </tr>
    );
  }

  handleAdd = () => {
    this.props.schedulerStore.setAddPopup(true);
  }

  handleEdit = (scheduler) => {
    this.props.schedulerStore.select(scheduler);
    this.props.schedulerStore.setEditPopup(true);
  }

  handleDelete = scheduler => {
    const { measurement } = this.props.match.params;
    this.props.schedulerStore.del(measurement, scheduler);
  }

  handleQuestions = scheduler => {
    const { measurement } = this.props.match.params;
    const { id } = scheduler;
    this.props.schedulerStore.select(scheduler);
    this.props.history.push(`/measurement/${measurement}/schedulers/${id}/questions`);
  }

  handleTemplate = scheduler => {
    const { measurement } = this.props.match.params;
    const { id } = scheduler;
    this.props.schedulerStore.select(scheduler);
    this.props.history.push(`/measurement/${measurement}/schedulers/${id}/template`);
  }

  handleNotifications = scheduler => {
    const { measurement } = this.props.match.params;
    const { id } = scheduler;
    this.props.schedulerStore.select(scheduler);
    this.props.history.push(`/measurement/${measurement}/schedulers/${id}/notifications`);
  }

  handleUpdate = () => {
    const { measurement } = this.props.match.params;
    this.props.schedulerStore.load(measurement);
  }

  handleBack = () => {
    this.props.history.goBack();
  }
}
