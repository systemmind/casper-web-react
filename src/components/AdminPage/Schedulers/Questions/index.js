import React from 'react';
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import List from '../../Components/List';
import Button from '../../Components/Button';
import './style.css';

@inject("schedulerStore")
@inject("questionsStore")
@withRouter
@observer
export default class Questions extends React.Component {
  constructor(props){
    super(props);

    const { questionsStore } = this.props;
    if (questionsStore.questions.length == 0){
      questionsStore.load();
    }
  }

  render(){
    const { selected } = this.props.schedulerStore;
    const { questions } = this.props.questionsStore;
    const scheduler = selected || undefined;
    return (
      scheduler && (
        <div>
          <Button onClick={this.handleBack}>Back</Button>
          <div>
            <List
              title="Scheduler questions"
              className="casper-scheduler-questions"
              items={scheduler.questions}
              btn="x"
              onBtnClick={(index) => this.handleDelete(index)}
            />
            <List
              title="Available questions"
              className="casper-questions"
              items={questions.map(item => item.name)}
              btn="+"
              onBtnClick={(index) => this.handleAdd(index)}
            />
          </div>
        </div>
      )
    );
  }

  renderQuestion = (question, order, isLast, scheduler) => {
    const { name } = question;
    const checked = scheduler.questions.indexOf(name) > -1;
    return (
      <tr key="question">
        <td>{order}</td>
        <td><input type="checkbox" name={name} checked={checked} onChange={this.handleCheck}/></td>
        <td>{name}</td>
        <td>
          <Button disabled={order === 0} onClick={() => this.handleUp(order)}>Up</Button>
          <Button disabled={isLast} onClick={() => this.handleDown(order)}>Down</Button></td>
      </tr>
    );
  }

  handleAdd = (index) => {
    const question = this.props.questionsStore.questions[index];
    const { selected } = this.props.schedulerStore;
    selected.addQuestion(question.name);
    selected.sync();
  }

  handleDelete = (index) => {
    const { selected } = this.props.schedulerStore;
    selected.removeQuestion(index);
    selected.sync();
  }

  handleBack = () => {
    this.props.history.goBack();
  }
}
