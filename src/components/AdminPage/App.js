import React from 'react';
import { inject, observer } from 'mobx-react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from './Login';
import Measurement from './Measurement';
import Measurements from './Measurements';
import Users from './Users';
import Schedulers from './Schedulers';
import SchedulerQuestions from './Schedulers/Questions';
import SchedulerNotifications from './Schedulers/Notifications';
import SchedulerTemplate from './Schedulers/Template';
import Emails from './Emails';
import ErrorPopup from './ErrorPopup';
import '../../index.css';
import authStore from './store/authStore';


@inject("commonStore")
@inject("questionsStore")
@inject("authStore")
@observer
export default class App extends React.Component{
  render(){
    return (
      <div>
        {
          !this.props.authStore.isLoggedIn ? <Login/> : this.renderApp()
        }
        <ErrorPopup />
      </div>
    );
  }

  componentDidMount(){
    const isDev = process.env.NODE_ENV || process.env.NODE_ENV === 'development';
    if (isDev && process.env.login && process.env.password){
      this.props.authStore.login(process.env.login, process.env.password);
    }
  }

  renderApp(){
    return (
      <Router>
        <Switch>
          <Route exact path='/admin' render={this.renderMeasurements} />
          <Route exact path='/measurement/:measurement' render={this.renderEdit}/>
          <Route path='/measurement/:measurement/users' render={this.renderUsers}/>
          <Route exact path='/measurement/:measurement/schedulers' render={this.renderSchedulers}/>
          <Route path='/measurement/:measurement/schedulers/:scheduler/questions' component={SchedulerQuestions} />
          <Route path='/measurement/:measurement/schedulers/:scheduler/notifications' component={SchedulerNotifications} />
          <Route path='/measurement/:measurement/schedulers/:scheduler/template' component={SchedulerTemplate} />
          <Route path='/emails' component={Emails} />
        </Switch>
      </Router>
    );
  }

  renderMeasurements = (props) => {
    const { token } = this.props.authStore;
    const { server } = this.props.commonStore;
    return (
      <Measurements
        server={server}
        token={token}
        onError={this.handleError}
        {...props}
      />
    );
  }

  renderEdit = (props) => {
    const { token } = this.props.authStore;
    const { server } = this.props.commonStore;
    return (
      <Measurement
        server={server}
        token={token}
        onError={this.handleError}
        {...props}
      />
    );
  }

  renderUsers = (props) => {
    const { token } = this.props.authStore;
    const { server } = this.props.commonStore;
    return (
      <Users
        server={server}
        token={token}
        onError={this.handleError}
        {...props}
      />
    )
  }

  renderSchedulers = (props) => {
    const { token } = this.props.authStore;
    const { server } = this.props.commonStore;
    return (
      <Schedulers
        server={server}
        token={token}
        onError={this.handleError}
        {...props}
      />
    );
  }

  handleError = (error) => {
    this.props.commonStore.addError(error);
  }
}
