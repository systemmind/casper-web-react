import React from 'react';
import Popup from 'reactjs-popup';
import Button from '../../Components/Button';
import PropTypes from 'prop-types';
import GroupBox from './GroupBox';
import '../style.css';

const popupContentStyle = {
  width: "70%",
  textAlign: "center"
}

export default class NotificationButton extends React.Component {
  static propTypes = {
    onSend: PropTypes.func.isRequired,
    disabled: PropTypes.bool
  }

  constructor(props){
    super(props);

    this.state = {
      staticEnabled: false,
      googleEnabled: false,
      xmppEnabled: false,
      emailEnabled: false,

      staticExpired: 60*30,
      googleLink: null,
      xmppType: "notification",
      emailType: "plain",

      showPopup: false,
      title: "",
      body: ""
    };
  }

  render(){
    const { disabled } = this.props;
    const {
      showPopup, body, title,
      staticEnabled, googleEnabled, xmppEnabled, emailEnabled
    } = this.state;
    return (
      <div className="bc-user-notif">
        <Button disabled={disabled} onClick={this.handleShow}>Notify</Button>
        <Popup
          open={showPopup}
          closeOnDocumentClick
          onClose={this.handleClose}
          contentStyle={popupContentStyle}
        >
          <input name="title" type="text" value={title} onChange={this.handleChange} placeholder="Title"/>
          <textarea name="body" className="bc-user-notif-body" value={body} onChange={this.handleChange} placeholder="Body"/>
          <div className="bc-users-notif-groups">
            { this.renderStaticGroup() }
            { this.renderGoogleGroup() }
            { this.renderXmppGroup() }
            { this.renderEmailGroup() }
          </div>
          <Button onClick={this.handleSend} disabled={!(staticEnabled || googleEnabled || xmppEnabled || emailEnabled)}>Send</Button>
        </Popup>
      </div>
    );
  }

  renderStaticGroup = () => {
    const { staticEnabled, staticExpired } = this.state;
    return (
      <GroupBox value="Static notifications" name="staticEnabled" visible={staticEnabled} onEnable={this.handleChecked}>
        <div className="bc-user-notif-group-label">Expired</div>
        <div className="bc-user-notif-group-input">
          <input type="number" name="staticExpired" value={staticExpired} onChange={this.handleChange}/>
        </div>
        <div className="bc-user-notif-group-label">sec</div>
      </GroupBox>
    );
  }

  renderGoogleGroup = () => {
    const { googleEnabled, googleLink } = this.state;
    return (
      <GroupBox value="Google notifications" name="googleEnabled" visible={googleEnabled} onEnable={this.handleChecked}>
        <div className="bc-user-notif-group-label">Link</div>
        <div className="bc-user-notif-group-input">
          <input type="text" name="googleLink" value={googleLink} onChange={this.handleChange}/>
        </div>
      </GroupBox>
    );
  }

  renderXmppGroup = () => {
    const { xmppEnabled, xmppType } = this.state;
    return (
      <GroupBox value="Xmpp notifications" name="xmppEnabled" visible={xmppEnabled} onEnable={this.handleChecked}>
        <div className="bc-user-notif-group-label">Type</div>
        <div className="bc-user-notif-group-input">
          <select className="bc-user-notif-email-input" name="xmppType" value={xmppType} onChange={this.handleChange}>
            <option value="notification">notification</option>
            <option value="data">data</option>
          </select>
        </div>
      </GroupBox>
    );
  }

  renderEmailGroup = () => {
    const { emailEnabled, emailType, body, title } = this.state;
    return (
      <GroupBox value="Email notifications" disabled={!(title && body)} name="emailEnabled" visible={emailEnabled} onEnable={this.handleChecked}>
        <div className="bc-user-notif-group-label">Type</div>
        <div className="bc-user-notif-group-input">
          <select className="bc-user-notif-email-input" name="emailType" value={emailType} onChange={this.handleChange}>
            <option value="plain">plain</option>
            <option value="html">html</option>
          </select>
        </div>
      </GroupBox>
    );
  }


  handleChange = (event) => {
    const { target } = event;
    const { name, value } = target;
    this.setState({
      [name]: value
    });
  }

  handleChecked = (event) => {
    const { target } = event;
    const { name, checked } = target;
    this.setState({
      [name]: checked
    });
  }

  handleSend = () => {
    const {
      title, body,
      staticEnabled, staticExpired,
      googleEnabled, googleLink,
      xmppEnabled, xmppType,
      emailEnabled, emailType
    } = this.state;

    const obj = {};

    if (staticEnabled){ Object.assign(obj, {static: {expired: staticExpired}}) }
    if (googleEnabled){ Object.assign(obj, {google: {link: googleLink}}) }
    if (xmppEnabled)  { Object.assign(obj, {xmpp: {type: xmppType}}) }
    if (emailEnabled) { Object.assign(obj, {email: {type: emailType}}) };

    Object.keys(obj).map(key => Object.assign(obj[key], {title, body}))

    this.props.onSend(obj);
    this.handleClose();
  }

  handleShow = () => this.setState({showPopup: true})
  handleClose = () => this.setState({showPopup: false})
}
