import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import '../style.css';

export default class GroupBox extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    visible: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    onEnable: PropTypes.func.isRequired
  }

  constructor(props){
    super(props);
  }

  render(){
    const { value, name, visible, disabled, onEnable } = this.props;
    const boxClass = cn({
      'bc-user-notif-group-box': true,
      'bc-user-notif-group-box-hidden': !visible
    });

    return (
      <div className="bc-user-notif-group-item">
        <label className="bc-user-label">
          <input name={name} disabled={disabled} type="checkbox" onChange={onEnable}/>
          {value}
        </label>
        <div className={boxClass}>
          { this.props.children }
        </div>
      </div>
    );
  }
}