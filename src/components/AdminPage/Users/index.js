import React from 'react';
import UserLoaderButton from './UserLoaderButton';
import NotificationButton from './NotificationButton';
import Button from '../Components/Button';
import EditUserPopup from './EditUserPopup';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import "./style.css"


@inject("userStore")
@inject("commonStore")
@withRouter
@observer
export default class Users extends React.Component {
  async componentDidMount(){
    const { userStore } = this.props;
    const { measurement } = this.props.match.params;
    userStore.setMeasurement(measurement);
    userStore.load();
  }

  render(){
    const { userStore } = this.props;
    const { allChecked } = userStore;
    const items = userStore.users.map(item => this.renderUser(item));

    return (
      <div>
        { this.renderHeader() }
        <table>
          <thead>
            <tr>
              <td className='blotcycler-col-user-checkbox'>
                <input type="checkbox" name="allChecked" checked={allChecked} onChange={({target: {checked}}) => userStore.selectAll(checked)}/>
              </td>
              <td className='blotcycler-col-user-id' onClick={()=>(userStore.setSort("id"))}>Id</td>
              <td className='blotcycler-col-user-name'>Name</td>
              <td className='blotcycler-col-user-authorized'>Authorized</td>
              <td className='blotcycler-col-user-phone'>Phone</td>
              <td className='blotcycler-col-user-email' onClick={()=>(userStore.setSort("emails"))}>Email</td>
              <td className='blotcycler-col-user-email' onClick={()=>(userStore.setSort("devices"))}>Devices</td>
              <td className='blotcycler-col-user-token' onClick={()=>(userStore.setSort("token"))}>Token</td>
              <td className='blotcycler-col-user-controls'></td>
            </tr>
          </thead>
          <tbody>{items}</tbody>
        </table>
        <EditUserPopup />
      </div>
    );
  }

  renderHeader = () => {
    const { userStore, commonStore, history } = this.props;
    const { users, authorized, measurement } = userStore;
    const selected = users.filter(item => item.selected).length > 0;

    return (
      <div>
        <Button onClick={() => history.goBack()}>Back</Button>
        <Button onClick={userStore.load}>Update</Button>
        <UserLoaderButton text="Add emails" type="email" />
        <UserLoaderButton text="Add phones" type="phone" />
        <NotificationButton disabled={!selected} onSend={userStore.sendNotification} onError={commonStore.addError} />
        <label className="blotcycler-label-userlist">Total: {users.length}</label>|
        <label className="blotcycler-label-userlist">
          Autorized &nbsp;
          <input type="checkbox" name="authorized" checked={authorized} onChange={({target: {checked}}) => userStore.setAuthorized(checked)}/>
        </label>
        <h4>Measurement #{measurement} | Users</h4>
      </div>
    );
  }

  renderUser(user){
    const { userStore } = this.props;
    const selected = user.selected;
    const token = user.user.get("token") || "";
    const emails = (user.user.get("emails") || []).join(", ");
    const devices = (user.user.get("devices") || []).map(
      device => `${device.slice(0, 3)}...${device.slice(-4)}`
    ).join(", ");

    return (
      <tr key={user.user.get('id')}>
        <td><input type="checkbox" name={user.user.get('id')} checked={selected} onChange={({target: {checked}}) => user.select(checked)}/></td>
        <td className='blotcycler-col-user-id'>{user.user.get('id')}</td>
        <td className='blotcycler-col-user-name'>{user.user.get('name')}</td>
        <td className='blotcycler-col-user-id'>{user.user.get('authorized') ? "true" : "false"}</td>
        <td className='blotcycler-col-user-phone'>{user.user.get('phone')}</td>
        <td className='blotcycler-col-user-email'>{emails}</td>
        <td className='blotcycler-col-user-email'>{devices}</td>
        <td className='blotcycler-col-user-token'>
          {`${token.slice(0,6)}...${token.slice(-3)}`}
        </td>
        <td className='blotcycler-col-user-controls'>
          <Button onClick={()=> userStore.setEditUser(user)}>Edit</Button>
          <Button onClick={()=> userStore.del(user)}>Delete</Button>
        </td>
      </tr>
    );
  }
}
