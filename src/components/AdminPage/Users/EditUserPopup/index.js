import React from 'react';
import Popup from 'reactjs-popup';
import { inject, observer } from 'mobx-react';
import Button from '../../Components/Button';
import '../style.css';

const popupContentStyle = {
  width: "70%",
  textAlign: "center"
}

@inject("userStore")
@observer
export default class EditUserPopup extends React.Component {
  render(){    
    const { userStore } = this.props;
    const { editUser } = userStore;
    const emails = editUser ? editUser.user.get('emails').join(", ") : [];
    return editUser && (
      <div className="bc-user-loader">
        <Popup
          open={editUser !== null}
          closeOnDocumentClick
          onClose={this.handleClose}
          contentStyle={popupContentStyle}
        >
          <div>Name</div>
          <input value={editUser.user.get('name')} onChange={({target: {value}}) => editUser.user.set('name', value)}/>
          <div><br/>Phone</div>
          <input value={editUser.user.get('phone')} onChange={({target: {value}}) => editUser.user.set('phone', value)}/>
          <div><br/>Emails</div>
          <br/>
          {
            this.renderEmails(editUser.user.get('emails') || [])
          }
          <br/>
          <div>
            <input value={editUser.newEmail} onChange={({target: {value}}) => editUser.setNewEmail(value)}/>
          </div>
          <div>
            <Button onClick={this.handleEmailAdd}>add email</Button>
          </div>
          <br/>
          <Button onClick={this.handleUserEdit}>Edit</Button>
        </Popup>
      </div>
    );
  }

  renderEmails = (emails) => {
    const delEmail = (email) => {
      const { user } = this.props.userStore.editUser;
      user.set('emails', user.get('emails').filter(item => item != email));
    }

    return (
      <div>
        {
          emails.map(email => (
            <div key="email">
              { email } <Button onClick={() => delEmail(email)}>x</Button>
            </div>
          ))
        }
      </div>
    );
  }

  handleEmailAdd = () => {
    const { editUser } = this.props.userStore;
    editUser.user.set('emails', editUser.user.get('emails').concat([editUser.newEmail]));
    editUser.setNewEmail("");
  }

  handleUserEdit = () => {
    const { userStore } = this.props;
    userStore.editUser.edit();
    userStore.setEditUser(null);
  }

  handleClose = () => this.props.userStore.setEditUser(null);
}
