import React from "react";
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import UserLoaderChild from './Child';


@inject("userStore")
@observer
export default class UserLoaderButton extends React.Component {
  static propTypes = {
    type: PropTypes.oneOf(["email, phone"]),
  }

  render(){
    return (
      <UserLoaderChild onLoad={this.handleLoad} text={this.props.text}/>
    );
  }

  handleLoad = async (items) => {
    const { type, userStore } = this.props;
    if (type == 'email'){
      return userStore.create({emails: items});
    }

    if (type == 'phone'){
      return userStore.create({phones: items});
    }
  }
}
