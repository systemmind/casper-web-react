import React from 'react';
import Popup from 'reactjs-popup';
import Button from '../../Components/Button';
import PropTypes from 'prop-types';
import '../style.css';

const popupContentStyle = {
  width: "70%",
  textAlign: "center"
}

export default class UserLoaderChild extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onLoad: PropTypes.func.isRequired
  }

  constructor(props){
    super(props);

    this.state = {
      emails: "",
      showPopup: false
    };
  }

  render(){
    const { text } = this.props;
    const { emails, showPopup } = this.state;
    return (
      <div className="bc-user-loader">
        <Button onClick={this.handleAdd}>{ text }</Button>
        <Popup
          open={showPopup}
          closeOnDocumentClick
          onClose={this.handleClose}
          contentStyle={popupContentStyle}
        >
          <textarea className="bc-user-loader-text" value={emails} onChange={this.handleChange}></textarea>
          <Button onClick={this.handleLoad} disabled={emails === ""}>Load</Button>
        </Popup>
      </div>
    );
  }

  handleChange = (event) => {
    this.setState({
      emails: event.target.value
    });
  }

  handleLoad = () => {
    const { onLoad } = this.props;
    const { emails } = this.state;
    const emailList = emails.split(/\,\s*/);
    onLoad(emailList.filter((item) => (item !== "")));
    this.setState({showPopup: false});
  }

  handleAdd = () => this.setState({showPopup: true})

  handleClose = () => this.setState({showPopup: false})
}
