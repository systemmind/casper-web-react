import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Provider } from "mobx-react";

import App from './App';

import authStore from './store/authStore';
import commonStore from './store/commonStore';
import measurementStore from './store/measurementStore';
import userStore from './store/userStore';
import schedulerStore from './store/schedulerStore';
import questionsStore from './store/questionsStore';
import emailsStore from './store/emailsStore';

const stores = {
  authStore,
  commonStore,
  userStore,
  measurementStore,
  schedulerStore,
  questionsStore,
  emailsStore
}

// For easier debugging
window._____APP_STATE_____ = stores;

ReactDOM.render(
  <Provider {...stores}>
    <Router history={createBrowserHistory()}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
