import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Components/Button';

export default class MeasurementChild extends React.Component {
  static propTypes = {
    measurement: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    company_url: PropTypes.string.isRequired,
    start_date: PropTypes.string.isRequired,
    stop_date: PropTypes.string.isRequired,
    manager_email: PropTypes.string.isRequired,
    onApply: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
  }

  render(){
    const { measurement, name, company_url, start_date, stop_date, manager_email, onApply, onBack} = this.props;
    return (
      <div>
        <Button onClick={onBack}>Back</Button>
        <Button onClick={onApply}>Apply Changes</Button>
        <Button type='routelink' to={`/measurement/${measurement}/users`}>Users</Button>
        <Button type='routelink' to={`/measurement/${measurement}/schedulers`}>Schedulers</Button>
        <Button type='hyperlink' to={`https://${window.location.hostname}:${window.location.port}/setup/${measurement}`} target="_blank">Setup Page</Button>
        <Button type='hyperlink' to={`https://${window.location.hostname}:${window.location.port}/subscribe/${measurement}`} target="_blank">Subscribe page</Button>
        <Button type='hyperlink' to={`https://${window.location.hostname}:${window.location.port}/mobilesetup/${measurement}`} target="_blank">Mobile setup page</Button>
        <h4>Measurement #{measurement} | Edit </h4>
        <label>Company's Name</label>
        <input type="text" name="name" value={name} onChange={this.handleChange} />
        <label>Company's Website</label>
        <input type="url" name="company_url" value={company_url} onChange={this.handleChange} />
        <label>Measurement's Starting Date</label>
        <input
            type="date"
            name="start_date"
            value={start_date}
            onChange={this.handleChange}
        />
        <label>Measurement's Ending Date</label>
        <input
              type="date"
              name="stop_date"
              value={stop_date}
              onChange={this.handleChange}
        />
        <label>Company's Manager E-mail</label>
        <input
            type="email"
            name="manager_email"
            value={manager_email}
            onChange={this.handleChange}
        />
      </div>
    );
  }

  handleChange = (event) => {
    const { target } = event;
    const { name, value } = target;
    const { onChange } = this.props;
    onChange({[name]: value});
  }
}
