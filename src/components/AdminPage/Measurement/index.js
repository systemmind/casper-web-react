import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import MeasurementChild from './Child.js';
import { Get, Put } from '../../../helpers';

@inject("authStore")
@withRouter
@observer
export default class Measurement extends React.Component {
  static propTypes = {
    server: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired,
    // measurement: PropTypes.object.isRequired, // see this.props.match.params
    onError: PropTypes.func.isRequired
  }

  constructor(props){
    super(props);
    this.state = {
      id: -1,
      name: "",
      company_url: "",
      start_date: "",
      stop_date: "",
      manager_email: ""
    };
  }

  render(){
    const { measurement } = this.props.match.params;
    const { name, company_url, start_date, stop_date, manager_email } = this.state;

    return (
      <MeasurementChild
        measurement={parseInt(measurement)}
        name={name}
        company_url={company_url}
        start_date={start_date}
        stop_date={stop_date}
        manager_email={manager_email}
        onApply={this.handleApply}
        onChange={this.handleChange}
        onBack={this.handleBack}
      />
    );
  }

  async componentDidMount(){
    const { server, token, authStore } = this.props;
    const { measurement } = this.props.match.params;
    this.setState(await Get(`${server}/users/${authStore.user}/measurements/${measurement}`, token));
  }

  handleApply = async ()=>{
    const { server, token, authStore, onError } = this.props;
    try{
      const { id } = this.state;
      const response = await Put(`${server}/users/${authStore.user}/measurements/${id}`, token, this.state);
    }
    catch(error){
      onError(error);
    }
  }

  handleChange = (obj) => {
    this.setState(obj);
  }

  handleBack = () => {
    this.props.history.goBack();
  }
}
