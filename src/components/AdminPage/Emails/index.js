import React from 'react';
import Button from '../Components/Button';
import List from '../Components/List';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import './style.css';

@inject("authStore")
@inject("emailsStore")
@withRouter
@observer
export default class Emails extends React.Component {
  constructor(props){
    super(props);

    const { emails } = props.emailsStore;
    if (emails.length === 0){
      props.emailsStore.load();
    }

    props.emailsStore.setSelected(null);

    this.state = {
      title: "",
      body: "",
      type: 'undefined'
    }
  }

  render(){
    const { emailsStore, authStore } = this.props;
    const { isRoot } = authStore;
    const { emails, selected } = emailsStore;
    const { title, body, type } = this.state;
    const listItems = emails.map(item => isRoot ? `${item.id}|${item.email.get('user')}|${item.email.get("title")}` : `${item.id}|${item.email.get("title")}`);
    const listTitle = `Email templates${isRoot ? ": id|user|title" : "" }`;
    return (
      <div>
        <Button onClick={this.handleBack}>Back</Button>
        <Button onClick={this.handleUpdate}>Update</Button>
        <h1>{listTitle}</h1>
        <div className="casper-emails-panel">
          <List
            className="casper-emails-list"
            items={listItems}
            highlight={true}
            btn="x"
            onSelect={(index, item) => this.handleSelect(index)}
            onBtnClick={(index, item) => this.handleDelete(index)}
          />
          <div className="casper-emails-controls">
            <input className="casper-email-input" name="title" value={title} onChange={this.handleChange}/>
            <textarea
              className="casper-email-textarea"
              name="body"
              value={body}
              onChange={this.handleChange}
            />
            <select name="type" value={type} onChange={this.handleChange}>
              <option value="undefined"></option>
              <option value="plain">plain</option>
              <option value="html">html</option>
            </select>
          </div>
        </div>
        <div>
          <Button disabled={!(title && body && (type !== 'undefined'))} onClick={this.handleAdd}>Add</Button>
          <Button disabled={!(selected && title && body && (type !== 'undefined'))} onClick={this.handleSync}>Sync</Button>
        </div>
      </div>
    );
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  handleSelect = index => {
    const { emailsStore } = this.props;
    const { emails } = emailsStore;
    const email = emails[index]
    emailsStore.setSelected(email);

    this.setState({
      title: email.email.get("title"),
      body: email.email.get("body"),
      type: email.email.get("type")
    });
  }

  handleDelete = index => {
    const { emailsStore } = this.props;
    const { emails } = emailsStore;
    emailsStore.del(emails[index]);
  }

  handleAdd = () => {
    const { title, body, type } = this.state;
    this.props.emailsStore.create({title: title, body: body, type: type});
  }

  handleSync = () => {
    const { title, body, type } = this.state;
    const { selected } = this.props.emailsStore;
    selected.email.set("title", title);
    selected.email.set("body", body);
    selected.email.set("type", type);
    selected.sync();
  }

  handleUpdate = () => this.props.emailsStore.load();

  handleBack = () => {
    this.props.history.goBack();
  }
}
