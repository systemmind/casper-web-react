import React from 'react';
import Popup from 'reactjs-popup';
import { inject, observer } from 'mobx-react';
import Button from '../Components/Button';
import './style.css';

const contentStyle = {
  padding: 0,
  borderRadius: "5px",
  boxShadow: "0px 5px 20px 0px rgba(126, 137, 140, 0.20)",
  width: "600px"
}

@inject("commonStore")
@observer
export default class ErrorPopup extends React.Component {
  render(){
    try{
      const errors = this.props.commonStore.errors;
      return (
        <Popup
          open={errors.length > 0}
          closeOnDocumentClick
          onClose={this.close}
          className="casper-popup"
          contentStyle={contentStyle}
        >
          <div className="modal" className="casper-popup-content">
            <div className="casper-popup-error-header">Error</div>
            <div className="casper-popup-body">
              { errors.map((err, index) => <div key={index}>{err.message}</div>) }
            </div>
            <div className="casper-popup-footer">
              <Button onClick={this.close} className="casper-popup-close">Close</Button>
            </div>
          </div>
        </Popup>
      );
    }
    catch(error){
      console.error(error);
      return <div></div>
    }
  }

  close = () => {
    this.props.commonStore.resetErrors();
  }
}
