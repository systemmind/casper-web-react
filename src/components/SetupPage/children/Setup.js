import React from 'react';
import QRCode from 'qrcode.react';
import { PolicyModal } from '../../_general/Modal';
import greetingSVG from '../../../assets/img/greeting.png';
import celebratorySVG from '../../../assets/img/celebratory.png';
import h3 from '../../_general/Checkbox';
import Divider from '../../_general/Divider';
import AlertMessage from '../../_general/Alert';
import { getAppURL } from './helpers';
import { appAuthURL, appQRCodeURL } from './constangs';
import YouTube from '../../_general/YouTube';


class SetupPage extends React.Component {
  state = {
    tokenAutorized: false,
    token: null,
    isOpen: false,
    isClose: false,
    isDownloading: false,
    isTermsAccepted: false,
    isClicked: false,
    needDownload: false,
    isFailed: false
  }

  isDev = true;

  componentDidMount() {
    const policyStatus = sessionStorage.getItem('casper_policy');

    if (policyStatus === 'true') {
      this.generateQRCode();
    }
  }

  render = () => (!this.state.tokenAutorized) ? this.renderSetupPage() : this.renderSuccessPage();

  renderSetupPage = () => {
    const { isClicked, isOpen, isClose, isDownloading, token, isTermsAccepted, isFailed } = this.state;
    const { generateQRCode, handleOpen, handleClose } = this;
    const href = getAppURL(this.isDev);
    return (
      <div className='setup-page'>
        <a ref={(ref) => this.downloadBTN = ref} id='casper-download-btn' href={href} style={{display: 'none'}}>Download</a>
        {isFailed && <AlertMessage>Sorry, but we are having trouble getting your request. Please try again later or contact us</AlertMessage>}
        {isClose && !isTermsAccepted && isDownloading && <AlertMessage>We need you to accept Data Privacy Policy in order to get surveys. You can accept them <b style={{cursor: 'pointer'}} onClick={this.handleOpen}>here</b></AlertMessage>}
        <PolicyModal handleClose={handleClose} acceptFunc={generateQRCode} isOpen={isOpen} />
        <div className='setup-img-block'>
          <img className='casper-img-greeting' src={greetingSVG} />
        </div>
        <div className='setup-text-block'>
          <h1 id='setup-title'>Get surveys on your device.</h1>
          <h1>No personal data required</h1>
          <h2>Follow this 3-step guide to get started</h2>
          <div className='setup-checkboxes'>
            <h3>1. Click Download button and install the app</h3>
            <button className='casper-btn setup-btn' onClick={this.clickByDownload}>
              Download
            </button>
            <h3>2. Read and accept <a onClick={handleOpen}>Data Privacy Policy</a> on popup</h3>
            <h3>3. Open the app, drag it over the QR below and scan it</h3>
            {token !== null && token && <QRCode value={token} size={164}/>}
            {/* <Divider />
            <div className='setup-tutor'>
              <p className='setup-tutor-p'>Still having trouble?</p>
              <p className='setup-tutor-p'>Watch our tutorial video</p>
            </div>
            <YouTube link='' /> */} {/* 'MGYj6jS55DE' */}
          </div>
        </div>
      </div>
    )
  };

  clickByDownload = (e) => {
    this.setState({ isDownloading: true, isOpen: true, isClose: false, isClicked: true, needDownload: true });
  }

  renderSuccessPage = () => {
    return (
      <div className='setup-success-page'>
        <img className='casper-img-celebratory' src={celebratorySVG} />
        <div>
          <h1>Thank you for installing our app!</h1>
          <h1>Your demographic surveys will be sent to the app</h1>
        </div>
      </div>
    )
  }

  generateQRCode = async (e) => {
    try {
      const { measurement } = this.props;
      const { token, needDownload } = this.state;
      const response = await fetch(appQRCodeURL(measurement, this.isDev), {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({})
      });
  
      if (needDownload) {
        this.downloadBTN.click();
        this.setState({ needDownload: false });
      }
  
      const json = await response.json();
  
      sessionStorage.setItem('casper_policy', 'true');
  
      const tmp = {
        "measurement": json.measurement,
        "user": {
          "id": json.id,
          "token": json.token
        }
      };
  
      if (JSON.stringify(token) !== JSON.stringify(tmp)) {
        this.setState({ token: JSON.stringify(tmp) });
      }
  
      this.setState({ isTermsAccepted: true, isOpen: false, isClose: true })
      this.ping(json);
    } catch(err) {
      console.error(err);
      this.setState({ isFailed: true, isOpen: false, isClose: true });
    }
  };

  ping(obj) {
    setTimeout(async () =>{ 
      console.log("ping...");
      const { measurement } = this.props;
      const url = appAuthURL(measurement, obj.id, this.isDev);
      const response = await fetch(url, {
        method: 'GET',
        headers: { "token-Authorization": obj.token },
      });
  
      const json = await response.json();
      if ( !json.authorized ){
        this.ping(obj);
      }
      else {
        this.setState({
          tokenAutorized: true
        });
      }
    }, 1000);
  };

  handleClose = (e) => this.setState({ isOpen: false, isClose: true });
  handleOpen = (e) => this.setState({ isOpen: true, isClicked: true });
}

export default SetupPage;