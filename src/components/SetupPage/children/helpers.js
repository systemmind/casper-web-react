import { appURLMacOS, appURLWindowsOS } from './constangs';

const getOS = () => {
  var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

  if (macosPlatforms.indexOf(platform) !== -1) {
    os = 'Mac OS';
  } else if (iosPlatforms.indexOf(platform) !== -1) {
    os = 'iOS';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    os = 'Windows';
  } else if (/Android/.test(userAgent)) {
    os = 'Android';
  } else if (!os && /Linux/.test(platform)) {
    os = 'Linux';
  }

  return os;
}

export const getAppURL = (isDev) => {
  const OS = getOS();
  if (OS === 'Mac OS') {
    return appURLMacOS(isDev);
  } else if (OS === 'Windows'){
    return appURLWindowsOS(isDev);
  } else {
    return '/404'; //This OS not supported
  };
}