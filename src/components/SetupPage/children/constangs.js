const host = (isDev) => (isDev) ? `${process.env.appDomain}` : window.location.host;

export const appURLMacOS = (isDev = false) => `https://${host(isDev)}/releases/Setup_CASPER_Ver_1.0.dmg`;
export const appURLWindowsOS = (isDev = false) => `https://${host(isDev)}/releases/Setup_CASPER_Ver_1.0.exe`;

export const appAuthURL = (measurement, user, isDev = false) => `https://${host(isDev)}:5000/api/measurements/${measurement}/users/${user}`;
export const appQRCodeURL = (measurement, isDev = false) => `https://${host(isDev)}:5000/api/measurements/${measurement}/users`;
