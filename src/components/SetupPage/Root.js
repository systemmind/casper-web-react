import React from 'react';
import { addRouting } from '../../decorators';
import Setup from './children/Setup';

const SetupPage = (props) => {
  return <Setup measurement={props.id}/>;
}

const route = [ SetupPage, '/setup/:id', false, false ];
const error = [ '/404', '/setup', true, true ];

export default addRouting([ route, error ]);