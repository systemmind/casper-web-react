import React from 'react';
import PropTypes from 'prop-types';
import Spiner from '../_general/Loader';
import ExpiredPage from './ExpiredPage';
import FailedPage from './FailedPage';
import QuestionLoader from './QuestionLoader';
import addLayout from './Decorators/addLayout';
import addControlsHOC from './Decorators/addControlsHOC';
import addQuestion from './Decorators/addQuestion';
import i18n from './i18n';
import { Get, Post, Put } from '../../helpers';

export default class Controller extends React.Component {
  static propTypes = {
    Loader: PropTypes.object, //or null
    user: PropTypes.number.isRequired,
    survey: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    location: PropTypes.object //from route
  }
  
  state = {
    questions: [],
    status: null,
    introOpened: false,
    isSurveyLoaded: false,
    survey: null
  }

  index = 0;

  async componentDidMount() {
    try{
      const survey = await this.getSurvey();
      if (survey && survey.template && survey.template.language){
        await i18n.changeLanguage(survey.template.language);
      }

      if (!(survey.template && (typeof survey.template.questions === 'object') && survey.template.questions.length)){
        throw new Error("invalid survey template");
      }

      this.setState({ isSurveyLoaded: true });
      if (this.isSurveyExpired()){
        return this.setState({status: 'expired'})
      }
    }
    catch(error){
      this.setState({status: 'failed'});
      console.error(error);
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { introOpened, isSurveyLoaded, status, survey } = this.state;
    
    if (
      !prevState.introOpened && introOpened && isSurveyLoaded && !status &&
      survey.template && survey.template.questions && survey.template.questions.length
    ) {
      this.changeQuestion();
    }
  }

  getSurvey = async () => {
    const { survey, url, token, user } = this.props;
    const result = await Get(`${url}/employees/${user}/surveys/${survey}`, token);
    result.template = JSON.parse(result.template)
    this.setState({survey: result});
    return result;
  }

  isSurveyExpired = () => {
    const { questions, survey } = this.state;
    if (questions.length >= survey.template.questions.length){
      return !this.state.questions.find(item => (!item.answered && !item.skipped))
    }

    return false;
  }

  getQuestions = async () => {
    const { questions } = this.state.survey.template;
    const { survey, url, token, user } = this.props;
    const result = await Get(`${url}/employees/${user}/surveys/${survey}/questions`, token);
    this.setState({questions: result});
    return result;
  }

  createQuestion = async (ordern) => {
    const { url, user, survey, token } = this.props;
    const apiUrl = `${url}/employees/${user}/surveys/${survey}/questions`;
    const obj = {
      ordern: ordern,
      answered: false,
      skipped: false,
      postponed: false
    };

    const result = await Post(apiUrl, token, obj);
    return Object.assign(obj, result);
  }

  answerQuestion = async () => {
    const { id } = this.state.status;
    const { url, user, survey, token } = this.props;
    const apiUrl = `${url}/employees/${user}/surveys/${survey}/questions/${id}`;
    return await Put(apiUrl, token, {answered: true});
  }

  changeQuestion = async () => {
    const questions = await this.getQuestions();
    this.setState({questions});
    const question = questions.find(q => (!q.answered && !q.skipped))
    if (question){
      this.index = questions.indexOf(question);
      this.setState({status: question});
    }
    else {
      if (questions.length < this.state.survey.template.questions.length){
        this.index = questions.length;
        const question = await this.createQuestion(questions.length);
        return this.setState({status: question});
      }

      this.setState({status: 'completed'});
    }
  }

  handleNext = async (e) => {
    await this.answerQuestion();
    this.index++;
    if (this.index < this.state.survey.template.questions.length) {
      this.setState({ status: null }, async () => {
        await this.changeQuestion();
      });
    } else {
      this.setState({ status: 'completed' });
    }
  }

  render() {
    const { status, questions, introOpened, isSurveyLoaded } = this.state;
    const { Loader } = this.props;
    const { Intro, addState, SuccessPage } = Loader;

    /* SPLIT INTRO LOGIC TO THE DECORATORS */
    /* TODO: why do we need this? */
    if ( ( ( (!status || status === '') && !isSurveyLoaded ) /*|| !questions.length*/) 
        && (status !== 'failed' && status !== 'expired' && status !== 'completed') ) {
      return <Spiner />;
    }

    if (!introOpened && Intro && status !== 'expired' && status !== 'failed') {
      return <Intro onClick={(e) => this.setState({ introOpened: true })} />;
    } else {
      switch (status) {
        case 'completed': return <SuccessPage />;
        case 'expired': return <ExpiredPage />;
        case 'failed': return <FailedPage />;
        case null: return <Spiner />;
        default:
          let Component = addLayout(addQuestion(QuestionLoader));
          Component = (addState) ? addState(Component) : Component;
          Component = this.addProps(addControlsHOC(Component));
          return Component;
      }
    }
  }

  addProps = (Component) => {
    const { questions, survey, status } = this.state;
    const { Loader } = this.props;
    const template = survey.template;
    const templateProps = template.questions[this.index];

    return (
      <Component
        {...this.props}
        {...Loader}
        {...templateProps}
        template={template}
        question={status.id}
        progress={+this.index + 1}
        total={survey.template.questions.length}
        questions={questions}
        onNext={this.handleNext}
      />
    )
  }
}


