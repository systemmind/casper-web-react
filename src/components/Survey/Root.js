import React from 'react';
import { withRouter } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFoundPage from '../404/Root';
import Survey from './Controller';
import Loader from '../_general/Loader';
import ExpiredPage from './ExpiredPage';
import Firebase from '../_Firebase';

export default class Root extends React.Component {
  state = {
    BodyLoader: null
  }

  url = process.env.casperApi; //getURL();
  firebase = (/iPad|iPhone|iPod/.test(navigator.userAgent)) ? null : new Firebase();

  getQuery = (param, location) => {
    const urlParams = new URLSearchParams(location.search);

    return urlParams.get(param);
  };

  componentDidMount()
  {
    // const language = this.getQuery('language', window.location);
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/survey" render={this.renderSurvey} />;
          <Route path="/404" component={NotFoundPage} />
        </Switch>
      </Router>
    )
  }

  load = (type, name) => {
    return import(`./${type}/${name}.js`)
      .then(BodyLoader => this.setState({ BodyLoader }))
      .catch(this.showError);
  }

  showError = (err) => console.error(`${err}\n${err.stack}`);

  renderSurvey = ({ location }) => {
    const { BodyLoader } = this.state;
    const WithRoute = withRouter(Survey);
    const survey = parseInt(this.getQuery('survey', location));
    const user = parseInt(this.getQuery('user', location));
    const token = this.getQuery('token', location);
    const loader = this.getQuery('loader', location);

    if (!survey || !token || !loader) return <ExpiredPage />;

    if (!BodyLoader) {
      this.load('Loaders', loader);
      return <Loader />;
    } else {
      return (
        <React.Suspense fallback={<Loader />}>
          <WithRoute
            {...this.props}
            Loader={BodyLoader}
            user={user}
            survey={survey}
            token={token}
            location={location}
            url={this.url}
            onError={this.handleError}
          />
        </React.Suspense>
      );
    }
  }

  handleError = (error) => console.error(error);
}
