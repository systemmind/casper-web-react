import React from 'react';
import PropTypes from 'prop-types';

export default class QuestionLoader extends React.Component {
  static propTypes = {
    //from HOC
    user: PropTypes.number.isRequired,        // id of user
    survey: PropTypes.number.isRequired,      // id of survey
    question: PropTypes.number.isRequired,    // question id - do we need object?
    token: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    //from parent
    progress: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    onError: PropTypes.func.isRequired
  }

  state = {
    Question: null
  }

  componentDidMount(){
    const { type } = this.props;
    const Question = this.load(type);
    this.setState({Question});
  }

  load = (name) => React.lazy(() => import(`./Questions/controls/${name}.js`).catch(this.props.onError));

  render() {
    const { Question } = this.state;
    return (
      Question && (
        <Question {...this.props} />
      )
    );
  }
}
