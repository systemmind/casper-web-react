import React from 'react';
import expiredPNG from '../../assets/img/expired.png';

export default () => {
  return (
    <div className='casper-success-page casper-expired-page'>
      <img className='casper-img-expired' src={expiredPNG} />
      <div>
          <h1>Sorry, this survey is no longer available.</h1>
          <h1>Keep an eye on your notifications for another one!</h1>
      </div>
    </div>
  )
}