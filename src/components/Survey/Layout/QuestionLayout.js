import React from 'react';
import Spiner from '../../_general/Loader';
import PropTypes from 'prop-types';

export default class QuestionLayout extends React.Component {
  static propTypes = {
    //from HOC
    total: PropTypes.number.isRequired,
    progress: PropTypes.number.isRequired,
    Header: PropTypes.func, //or null
    ProgressBar: PropTypes.func, //or null
    //from decorator
    Question: PropTypes.func.isRequired,
    tooltipText: PropTypes.string
  }

  state = { isLoading: false, ready: false };

  startLoading = (e) => this.setState({ isLoading: true });
  stopLoading = (e) => this.setState({ isLoading: false });

  render() {
    const { Header, Question, ProgressBar, total, progress, tooltipText } = this.props;
    const { isLoading } = this.state;
    const barBody = [];
    for (let i = 0; i < total; i++) {
      const mainStyle = 'progress-block';
      const activeStyle = 'progress-block progress-block-active';
      const currentStyle = (progress > i) ? activeStyle : mainStyle;
      const cssStyle = { width: 240 / +total };

      barBody[i] = <div key={i} className={currentStyle} style={cssStyle}></div>;
    }

    const qStyle = isLoading ? {display: "none"} : {}
    return (
      <div className='casper-question-layout'>
        <Header tooltipText={tooltipText} />
        {isLoading && <Spiner />}
        <div className='casper-question-layout-body'>
          {ProgressBar && <ProgressBar {...this.props} />}
          <div>
            <Question
              startLoading={this.startLoading}
              stopLoading={this.stopLoading}
              {...this.props}
            />
          </div>
        </div>
      </div>
    )
  }
}
