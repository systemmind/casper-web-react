import React from 'react';
import { Trans } from 'react-i18next';
import failedPNG from '../../assets/img/failed.png';

export default () => {
  return (
    <div className='casper-failed-page'>
      <img className='casper-img-failed' src={failedPNG} />
      <div>
          <h1><Trans>We’ve encountered error while processing your survey</Trans></h1>
          <p>If you see this screen, try refreshing the page or opening the survey again later.</p>
          <p>We will appreciate if you message our support via this email:</p>
          <a href='mailto:casperproject2019@gmail.com'>casperproject2019@gmail.com</a>
      </div>
    </div>
  )
}