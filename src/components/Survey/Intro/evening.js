import React from 'react';
import { Trans } from 'react-i18next';
import PNG from '../../../assets/img/Evening Illustration.png';

export default ({ onClick }) => (
  <div className='casper-intro-screen'>
    <div className='casper-intro-text-block'>
      <h1><Trans>Your evening diary</Trans></h1>
      <span><Trans>Time to complete - 3 mins</Trans></span>
      <h2><Trans>Let’s look at how you felt today</Trans></h2>
      <button name="submit" className='casper-btn casper-btn-alt' onClick={onClick}>
        <Trans>Start</Trans>
      </button>
    </div>
    <img src={PNG} alt='survey intro picture' className='casper-intro-img' />
  </div>
);