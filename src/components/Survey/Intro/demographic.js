import React from 'react';
import PNG from '../../../assets/img/Demographic Illustration.png';

export default ({ onClick }) => (
  <div className='casper-intro-screen'>
    <div className='casper-intro-text-block'>
      <h1>Demographic survey</h1>
      <span>Time to complete: 1 minute</span>
      <h2>Only 3 questions to get started</h2>
      <button className='casper-btn casper-btn-alt' onClick={onClick}>
        Start
      </button>
    </div>
    <img src={PNG} alt='survey intro picture' className='casper-intro-img' style={{marginRight: '4%'}} />
  </div>
);