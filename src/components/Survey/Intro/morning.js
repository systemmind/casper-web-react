import React from 'react';
import PNG from '../../../assets/img/Morning Illustration.png';
import { Trans } from 'react-i18next';

export default ({ onClick }) => (
  <div className='casper-intro-screen'>
    <div className='casper-intro-text-block'>
      <h1><Trans>Your morning diary</Trans></h1>
      <span><Trans>Time to complete - 2 mins</Trans></span>
      <h2><Trans>Mornings are tough, but so are you!</Trans></h2>
      <button className='casper-btn casper-btn-alt' onClick={onClick}>
        <Trans>Start</Trans>
      </button>
    </div>
    <img src={PNG} alt='survey intro picture' className='casper-intro-img' />
  </div>
);