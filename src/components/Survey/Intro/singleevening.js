import React from 'react';
import PNG from '../../../assets/img/Evening Illustration.png';

export default ({ onClick }) => (
  <div className='casper-intro-screen' id='casper-singleevening-intro'>
    <div className='casper-intro-text-block'>
      <h1>Evening survey</h1>
      <span>Time to complete: 10 minutes</span>
      <h2>The main purpose of this survey is to help you boost your mental wellbeing and productivity. The survey is fully anonymous. We only ask what 3 important activities you did today and how you felt. Thank you for participating and we appreciate your efforts!</h2>
      <button name="start" className='casper-btn casper-btn-alt' onClick={onClick}>
        Start
      </button>
    </div>
    <img src={PNG} alt='survey intro picture' className='casper-intro-img' />
  </div>
);