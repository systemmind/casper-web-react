import Header from '../Header/demographic';
import Intro from '../Intro/demographic';
import SuccessPage from '../SuccessDemographicPage';
import { DefaultBar as ProgressBar } from '../Elements/Bar';

export { Header, Intro, SuccessPage, ProgressBar };