import Header from '../Header/afternoon';
import Intro from '../Intro/afternoon';
import SuccessPage from '../SuccessPage';
import { DefaultBar as ProgressBar } from '../Elements/Bar';

export { Header, Intro, SuccessPage, ProgressBar };

/* export { 
  Components: [ Header, Intro ], 
  Loader: addPreIntroController(Controller) 
}; */