import React from 'react';
import { Trans } from 'react-i18next';
import celebratorySVG from '../../assets/img/celebratory.png';

export default () => {
  return (
    <div className='casper-success-page'>
      <img className='casper-img-celebratory' src={celebratorySVG} />
      <div>
          <h1><Trans>Thank you for completing your today’s diary</Trans>.</h1>
          <h1><Trans>Keep an eye on your notifications for the next one to follow your self-reflection</Trans></h1>
      </div>
    </div>
  )
}