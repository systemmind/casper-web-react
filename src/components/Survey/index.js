import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import 'babel-polyfill';
import '../../index.css';
import './style.css';
import 'tippy.js/dist/svg-arrow.css';

ReactDOM.render(
  <div>
    <Root />
  </div>, document.getElementById('root')
);
