import React from 'react';
import PropTypes from 'prop-types';
import dateFormat from 'dateformat';
import { Get, Post } from '../../../helpers';

export default (Component) => class ComponentHOC extends React.Component {
  static propTypes = {
    user: PropTypes.number.isRequired,        // id of user
    survey: PropTypes.number.isRequired,      // id of survey
    question: PropTypes.number.isRequired,    // question id - do we need object?
    token: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    progress: PropTypes.number.isRequired,
    onError: PropTypes.func.isRequired
  }

  // TODO: remove indent?
  createRecord = async (controlType, controlOrder, name=undefined, value=undefined) => {
    const { url, user, survey, question, token, progress, onError } = this.props;
    const apiURL = `${url}/employees/${user}/surveys/${survey}/questions/${question}/controls`;
    const indent = `${survey}_${+progress - 1}`;
    const query = {
      filters: [{name: `${controlType}s.ordern`, op: "=", value: controlOrder}, {name: `${controlType}s.indent`, op: "=", value: indent}],
      controlType: controlType
    };

    const getURL = `${apiURL}?q=${JSON.stringify(query)}`;

    try{
      let result = await Get(getURL, token);

      if (result.length > 1){
        console.warn("ambiguous records: ", result)
        // use the item that has biggest index, maybe it is better to compare timestamps in future
        result = result.reduce((max, item) => item.id >= max.id ? item : max);
      }
      else if (result.length == 0){
        const q = { controlType };
        const body = Object.assign({ indent, ordern: controlOrder }, name ? {name} : {}, value ? {value} : {});
        result = await Post(`${apiURL}?q=${JSON.stringify(q)}`, token, body);
        result = await Get(`${apiURL}/${result.id}?q=${JSON.stringify(q)}`, token);    // TODO: think how to avoid excess GET request
      }
      else {
        result = result[0];
      }

      return result;
    }
    catch(error){
      onError(error)
    }
  }

  pullRecords = async (query, all=false) => {
    const { url, user, survey, question, token, progress } = this.props;
    const q = JSON.parse(JSON.stringify(query));
    const { filters } = q;
    if (filters){
      filters.forEach(item => {
        switch (item.value) {
          case "${curdate}":
            const now = new Date();
            item.value = dateFormat(now, "UTC:yyyy-mm-dd");
            break;
          case "${questionOrder}":
            item.value = progress - 1;
            break;
          case "${survey}":
            item.value = survey;
            break;
        }
      });
    }

    const path = all ? "" : `/surveys/${survey}/questions/${question}`;
    const apiURL = `${url}/employees/${user}${path}/controls?q=${JSON.stringify(q)}`;
    return await Get(apiURL, token);
  }

  track = async () => {
    // TODO: add questions/shown request processing to backend and send request here
  }

  render() {
    return (
      <Component
        {...this.props}
        createRecord={this.createRecord}
        pullRecords={this.pullRecords}
        track={this.track}
      />
    );
  }
}
