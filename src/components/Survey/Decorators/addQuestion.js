import React from 'react';
import PropTypes from 'prop-types';
import Spiner from '../../_general/Loader';

/* needRecord argument is atavism, but maybe it may be usefull in future */
export default (Question, NeedRecord = true) => class QuestionHOC extends React.Component {
  static propTypes = {
    controlOrder: PropTypes.number,
    track: PropTypes.func.isRequired,
    createRecord: PropTypes.func.isRequired,
    repeatFetch: PropTypes.func
  }

  state = {
    isLoaded: false,
    record: null
  }

  async componentDidMount() {
    try {
      await this.checkForRecord();
    } catch(err) {
      console.log(err);
    }
  }

  checkForRecord = async () => {
    const { needRecord } = this.props;
    const need = (typeof needRecord !== 'undefined') ? needRecord : NeedRecord;
    if (need) {
      const { track, createRecord, controlOrder, controlType } = this.props;
      const record = await createRecord(controlType, controlOrder || 0);
      this.setState({ isLoaded: true, record });
      await track();
    } else {
      this.setState({ isLoaded: true });
    }
  }

  render() {
    const { isLoaded, record } = this.state;

    /* if (!isLoaded) {
      return <Spiner />;
    }*/
    
    return <Question {...this.props} recordData={record} />
  }
}
