import React from 'react';

function doOnOrientationChange() {
  let or = null;

  switch(window.orientation) {  
    case -90 || 90:
      or = 'landscape';
      break; 
    default:
      or = 'portrait';
      break; 
  }
  
  return or;
}

export default (Component) => class DeviceInfoHOC extends React.Component {
  state = { 
    mobile: false, 
    orientation: 'landscape'
  };

  componentDidMount() {
    if (typeof window === 'object') {
      const orientation = window.screen.msOrientation || (window.screen.orientation || window.screen.mozOrientation || {}).type;
      if (orientation !== "portrait-secondary" 
          && orientation !== "portrait-primary" 
          && orientation !== "landscape-primary" 
          && orientation !== "landscape-secondary"
          && (/iPhone/i.test(window.navigator.userAgent))
        ) {
        // iphone code
        if (doOnOrientationChange() === "portrait") {
          this.setState({ mobile: true, orientation: 'portrait' });
        } else {
          this.setState({ mobile: true, orientation: 'landscape' });
        }
  
        window.addEventListener("orientationchange", () => {
          if (this.state.orientation === 'landscape') {
            this.setState({ mobile: true, orientation: 'portrait' });
          } else {       
            this.setState({ mobile: true, orientation: 'landscape' });
          }
        });
      } else {
        //another code
        if (orientation === "portrait-secondary" || orientation === "portrait-primary") {
          if (window.screen.width <= 500 && window.screen.height <= 900) {
            this.setState({ mobile: true, orientation: 'portrait' });
          } else {
            this.setState({ mobile: false, orientation: 'portrait' });
          } 
        } else {
          if (window.screen.width <= 900 && window.screen.height <= 500) {
            this.setState({ mobile: true, orientation: 'landscape' });
          } else {
            this.setState({ mobile: false, orientation: 'landscape' });
          } 
        }
  
        window.addEventListener("orientationchange", () => {
          if (this.state.orientation === 'landscape') {
            if (window.screen.width <= 500 && window.screen.height <= 900) {
              this.setState({ mobile: true, orientation: 'portrait' });
            } else {
              this.setState({ mobile: false, orientation: 'portrait' });
            } 
          } else {
            if (window.screen.width <= 900 && window.screen.height <= 500) {
              this.setState({ mobile: true, orientation: 'landscape' });
            } else {
              this.setState({ mobile: false, orientation: 'landscape' });
            } 
          }
        });
      }    
    }
  }

  render() {
    const { orientation, mobile } = this.state;

    return <Component {...this.props} isMobile={mobile} orientation={orientation} />
  }
}