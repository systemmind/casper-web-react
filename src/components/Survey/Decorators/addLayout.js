import React from 'react';
import PropTypes from 'prop-types';
import QuestionLayout from '../Layout/QuestionLayout';

export default (Question, Layout=QuestionLayout) => class LayoutHOC extends React.Component {
  static propTypes = {
    tooltip: PropTypes.string.isRequired,
  }

  render() {
    const { tooltip } = this.props;
    return <Layout {...this.props} Question={Question} tooltipText={tooltip} />
  }
}
