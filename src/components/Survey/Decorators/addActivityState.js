import React from 'react';
import PropTypes from 'prop-types';
import Spinner from '../../_general/Loader';

// TODO: remove spiner?
export default (Component) => class ActivityStateHOC extends React.Component {
  static propTypes = {
    //from HOC
    survey: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    template: PropTypes.shape({
      state: PropTypes.shape({
        query: PropTypes.object,                // add shape here?
        field: PropTypes.string.isRequired,
        selfName: PropTypes.string.isRequired
      }).isRequired,
      questions: PropTypes.array.isRequired
    }).isRequired,
    pullRecords: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired
  }

  state = { 
    currentState: null,
    currentStateIndex: -1,
    statesNumber: 0
  };

  componentDidMount(){
    const { currentState } = this.state;
    const { name, template, onError } = this.props;
    const { selfName } = template.state;

    if (!currentState && name !== selfName) {
      this.getCurrentState()
        .catch(error => onError(error));
    }
    else if (name === selfName){
      this.setState({
        currentStateIndex: 0,
        statesNumber: this.getStatesNumber()
      });
    }
  }

  handleChangeState = (currentState, currentStateIndex) => this.setState({ currentState, currentStateIndex });

  render() {
    const { currentState, currentStateIndex, statesNumber } = this.state;
    const { name, template } = this.props;
    const { selfName } = template.state;

    if ((currentState === null) && name !== selfName){
      return <Spinner />;
    }

    return (
      <Component
        {...this.props}
        onChangeState={this.handleChangeState}
        currentState={currentState}
        currentStateIndex={currentStateIndex}
        statesNumber={statesNumber}
      />
    );
  }

  getCurrentState = async () => {
    const { template, pullRecords } = this.props;
    const { query, field, indexField } = template.state;

    const result = await pullRecords(query, true);
    const cur = result.slice(-1).pop();
    const newState = {
      currentState: cur ? cur[field] : undefined,
      currentStateIndex: result.length - 1,
      statesNumber: this.getStatesNumber()
    }

    this.setState(newState);
  }

  getStatesNumber(){
    const { questions, state } = this.props.template;
    const { selfName } = state;
    const count = questions.reduce((acc, q) => ((q.name === selfName) ? ++acc : acc), 0);
    return count;
  }
}
