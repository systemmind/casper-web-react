import React from 'react';
import tippy, { roundArrow } from 'tippy.js';

export default (Component) => class TooltipForHeader extends React.Component {
  state = {
    mouseEntered: false
  }

  tooltipBtn = null;

  componentDidMount() {
    this.tooltipBtn = this.getTooltip();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.mouseEntered !== prevState.mouseEntered) {
      const tooltipBtn = this.tooltipBtn;

      if (typeof tooltipBtn === 'object') {
        if (this.state.mouseEntered) {
          if (tooltipBtn.show) {
            tooltipBtn.enable();
          }
        } else {
          if (tooltipBtn.show) {
            tooltipBtn.hide();
          }
        }
      }
    }
  }

  getTooltip = () => {
    const { tooltipText } = this.props;
    const el = window.document.querySelector('#casper-header-help-icon');

    if (typeof tooltipText !== 'string' || !el) {
      return null
    }

    return tippy(el, {
      content: tooltipText,
      theme: 'casper',
      placement: "bottom-end",
      maxWidth: '240px',
      arrow: roundArrow
    });
  }

  handleEnter = (e) => this.setState({ mouseEntered: true });
  handleLeave = (e) => this.setState({ mouseEntered: false });

  render() {
    const { tooltipText } = this.props;

    return (tooltipText) 
      ? <Component {...this.props} handleEnter={this.handleEnter} handleLeave={this.handleLeave} />
      : <Component {...this.props} />
  }
}