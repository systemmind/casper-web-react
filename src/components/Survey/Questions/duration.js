import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Checkbox from '../../_general/Checkbox/ResearchCheckbox';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  new Array("Less than 5 minutes", "0-5m"),
  new Array("5-15 minutes", "5-15m"),
  new Array("15-30 minutes", "15-30m"),
  new Array("30 minutes – 1 hour", "30m-1h"),
  new Array("1-2 hours", "1h-2h"),
  new Array("2-4 hours", "2h-4h"),
  new Array("4-6 hours", "4h-6h"),
  new Array("More than 6 hours", "6h-oo")
]

class QuestionDuration extends React.Component {
  constructor(props) {
    super(props);
    const { duration } = props.recordData[0];

    this.state = {
      value: '',
      isLoading: false
    };

    options.forEach((name) => {
      this.state[name[0]] = false;
    });

    if (duration) {
      const option = options.find(el => el[1] === duration);
      this.state.value = duration;
      this.state[option[0]] = true;
    }
  }

  getTitle = () => {
    const { questionType } = this.props;

    switch (questionType) {
      case 'past': return 'How long time did the activity last for?';
      default: return 'How long have you been working on this activity?';
    }
  }

  renderBody = () => {
    const { currentActivity } = this.props;
    const checkboxes = options.map(name => {
      const value = this.state[name[0]];

      return <Checkbox key={name[0]} checked={value} handleChange={this.handleChange(name[0])}>{name[0]}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 className='full-size' id='question-title-duration'>{this.getTitle()}</h1>
        <div className='question-container-checkers'>
          <div className='question-container-checkers-options' style={{ maxHeight: 160 }}>
            {checkboxes}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { value, isLoading } = this.state;
    const body = this.renderBody();
    const style = (!value.length) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => {
    const value = options.find((event) => event[0] === name);
    const newState = options.reduce((acc, val) => {
      acc[val[0]] = (val[0] === name) ? true : false;
      return acc;
    }, {})
    newState.value = value[1];

    this.setState(newState);
  }

  handleClick = (e) => {
    const { value, isLoading } = this.state;
    
    if (!value.length) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const apiURL = `${url}/survey/${survey}/question/duration/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ duration: value })
    }

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Select the appropriate time</p>';

export default addQuestion(QuestionDuration, QuestionLayout, tooltipText);