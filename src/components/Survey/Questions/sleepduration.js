import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Checkbox from '../../_general/Checkbox/ResearchCheckbox';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  new Array("Less then 1 hour", "Less then 1 hour"),
  new Array("1-2 hours", "1-2 hours"),
  new Array("3-5 hours", "3-5 hours"),
  new Array("6-8 hours", "6-8 hours"),
  new Array("9+ hours", "9+ hours")
]

class QuestionSleepDuration extends React.Component {
  constructor(props) {
    super(props);
    const { value } = props.recordData[0];

    this.state = {
      value: '',
      isLoading: false
    };

    options.forEach((name) => {
      this.state[name[0]] = false;
    });

    if (value) {
      const option = options.find(el => el[1] === value);
      this.state.value = value;
      this.state[option[0]] = true;
    }
  }

  getTitle = () => {
    const { questionType } = this.props;

    switch (questionType) {
      case 'past': return 'How long time did the activity last for?';
      default: return 'On average, how long did you sleep for last night?';
    }
  }

  renderBody = () => {
    const { currentActivity } = this.props;
    const checkboxes = options.map(name => {
      const value = this.state[name[0]];

      return <Checkbox key={name[0]} checked={value} handleChange={this.handleChange(name[0])}>{name[0]}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 id='question-title-sleepduration'>{this.getTitle()}</h1>
        <div className='question-container-checkers'>
          <div className='question-container-checkers-options' style={{ maxHeight: 'max-content' }}>
            {checkboxes}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { value, isLoading } = this.state;
    const body = this.renderBody();
    const style = (!value.length) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => {
    const value = options.find((event) => event[0] === name);
    const newState = options.reduce((acc, val) => {
      acc[val[0]] = (val[0] === name) ? true : false;
      return acc;
    }, {})
    newState.value = value[1];

    this.setState(newState);
  }

  handleClick = (e) => {
    const { value, isLoading } = this.state;
    
    if (!value.length) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const apiURL = `${url}/survey/${survey}/question/sleepduration/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ value })
    }

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Please think about the quality of your sleep overall, such as how many hours of sleep you got, how easily you fell asleep, how often you woke up during the night (except to go to the bathroom), how often you woke up earlier than you had to in the morning, and how refreshing your sleep was</p>';

export default addQuestion(QuestionSleepDuration, QuestionLayout, tooltipText);