import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Bubble from '../../_general/Bubble';
import addQuestion from '../Decorators/addQuestion';
import addMobileProps from '../Decorators/addMobileProps';
import Spinner from '../../_general/Loader';

class GoodActivities extends React.Component {
  state = { 
    isLoading: false,
    isLoaded: false,
    options: []
  }

  getSurveys = async () => {
    const { url, token, survey, repeatFetch } = this.props;
    try {
      const urlActivity = `${url}/survey/${survey}`
      const response = await fetch(urlActivity, {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      });
  
      if (!response.ok) {
        throw new Error(response.statusText);
      }
  
      const result = await response.json();
      return result;
    } catch(err) {
      console.log(err);
      return await repeatFetch(this.getSurveys.bind(this));
    }
  }

  getDailySurveys = async () => {
    const surveys = await this.getSurveys();
    let date = new Date();
    date.setHours(0, 0, 0);
    let list = surveys.filter((survey)=>{
      return date < (new Date(survey.end));
    });
    return list;
  }

  getSurveyData = async (survey) => {
    const { url, token, repeatFetch } = this.props;
    try {
      const apiURL = `${url}/survey/${survey}/questions`;
      const options = {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      }

      const questions = await fetch(apiURL, options);

      if (!questions.ok) {
        throw new Error(response.statusText);
      } 

      const questionData = await questions.json();

      const activities = questionData.reduce((acc, el, i) => {
        if (el === 'activity') acc.push(i);  
        return acc;
      }, []);

      return await this.getActivitiesFromSurvey(survey, activities);
    } catch (err) {
      console.log(err);
      return await repeatFetch(this.getSurveyData.bind(this, survey));
    }
  }

  async getActivitiesFromSurvey(survey, activities) {
    const { repeatFetch } = this.props;
    
    try {
      const arr = [];
      for (let i = 0; i < activities.length; i++) {
        await this.getActivityByIndent(survey, arr, i, activities);
      }

      return arr;
    } catch (err) {
      console.log(err);
      return await repeatFetch(this.getActivitiesFromSurvey.bind(this, survey, activities));
    }
  }

  getActivityByIndent = async (survey, arr, i, activities) => {
    const { url, token, repeatFetch } = this.props;
    
    try {
      const index = activities[i];
      const indent = `${survey}_${index}`;
      const urlGetData = `${url}/survey/${survey}/question/activity?indent=${indent}`;
      const options = {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      };
      const response = await fetch(urlGetData, options);
  
      if (response.status == 404){
        return null;
      }
  
      const res = await response.json();
      arr.push(res[0]);
    } catch(err) {
      console.log(err);
      return await repeatFetch(this.getActivityByIndent.bind(this, survey, arr, i, activities));
    }
  }

  async getGoodActivitiesRecords() {
    const { url, survey, token, repeatFetch } = this.props;
    try {
      const apiURL = `${url}/survey/${survey}/question/goodactivities`;
      const options = {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      };
  
      const response = await fetch(apiURL, options);
  
      if (response.status === 404 || !response.ok){
        return [];
      }
  
      return await response.json();
    } catch(err) {
      console.log(err);
      return await repeatFetch(this.getGoodActivitiesRecords.bind(this));
    }
  };

  async componentDidMount() {
    const { startLoading } = this.props;

    startLoading();
    await this.startQuestionLoading();
  };

  startQuestionLoading = async () => {
    try {
      const records = await this.getGoodActivitiesRecords();

      if (records.length) {
        const { isLoading, isLoaded, options } = this.state;
        const activities = [];
        
        const newState = records.reduce((acc, { activity, id }) => {
          acc[activity] = id;
          activities.push(activity);
          return acc;
        }, {});

        newState.activities = activities;
        newState.isLoading = isLoading;
        newState.isLoaded = isLoaded;
        newState.options = options;

        this.setState(newState, async () => {
          await this.getAllActivities();
        })
      } else {
        await this.getAllActivities();
      }
    } catch (err) {
      const { repeatFetch } = this.props;
      console.log(err);
      return await repeatFetch(this.startQuestionLoading.bind(this));
    }
  }

  getAllActivities = async () => {
    const { stopLoading, repeatFetch } = this.props;
    try {
      let surveys = await this.getDailySurveys();
      surveys = surveys.map((survey)=>{ return survey["id"]; });
      let activities = [];

      for (let i = 0; i < surveys.length; i++) {
        const survey = surveys[i];

        const data = await this.getSurveyData(survey);

        if (data) {
          if (Array.isArray(data)) {
            data.forEach(async (el) => {
              el.activity && !activities.includes(el.activity) && activities.push(el.activity);
            })
          } else {
            data.activity && !activities.includes(data.activity) && activities.push(data.activity);
          }
        }
      }

      const newState = {};
      newState.options = activities;

      activities.forEach((activity)=>{
        if (!this.state[activity]) {
          newState[activity] = null;
        } else {
          newState[activity] = this.state[activity];
        }
        const isLoading = activity + 'IsLoading';
        newState[isLoading] = false;
      });

      newState.isLoaded = true;
      newState.isLoading = false;

      stopLoading();
      this.setState(newState);
    } catch(err) {
      console.error('rubbishbin: ', err);
      return await repeatFetch(this.getAllActivities.bind(this));
    }
  }

  renderBody = () => {
    const { options } = this.state;
    const { isMobile } = this.props;
    let isLoading = false;
    const checkboxes = options.map((name, i) => {
      const color = '#FFCD00';
      const value = this.state[name];
      const title = name[0].toUpperCase() + name.substr(1);
      const onClick = (value) ? this.removeRecord(name, value) : this.addRecord(name);
      const scale = (isMobile || window.innerWidth < 460) ? 0.6 : 0.7;
      const loadingName = name + 'IsLoading';
      const isActive = !value ? false : true;

      if (this.state[loadingName]) {
        isLoading = true;
      }

      return <Bubble scale={scale} key={title} title={title} isActive={!value} color={color} onClick={onClick} />;
    });

    return (
      <div className='question-container-all'>
        <h1 id='question-title-goodactivities'>Which of the activities did you most enjoy?</h1>
        <div className='question-container-checkers question-container-bubbles'>
          {checkboxes}
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  addRecord = (name) => (e) => {
    const loadingName = name + 'IsLoading';
    const isLoading = this.state[loadingName];

    if (isLoading) {
      return false;
    }

    this.setState({ [loadingName]: true });

    const { url, survey, progress } = this.props;
    const urlRubbishBin = `${url}/survey/${survey}/question/goodactivities`;
    const indent = `${survey}_${+progress - 1}`;
    const body = JSON.stringify({ "activity": name, indent });

    this.startFetching('POST', urlRubbishBin, name, body);
  }

  removeRecord = (name, id) => (e) => {
    const loadingName = name + 'IsLoading';
    const isLoading = this.state[loadingName];

    if (isLoading) {
      return false;
    }

    this.setState({ [loadingName]: true });

    const { url, survey } = this.props;
    const urlRubbishBin = `${url}/survey/${survey}/question/goodactivities/${id}`;

    this.startFetching('DELETE', urlRubbishBin, name);
  }

  startFetching = (type, url, name, body) => {
    const { token, repeatFetch } = this.props;
    const loadingName = name + 'IsLoading';
    const options = {
      method: type,
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      }
    };

    if (typeof body !== 'undefined') {
      options.body = body;
    }

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => this.setState({ [loadingName]: false, [name]: (type === 'DELETE') ? null : res.id }))
      .catch(err => {
        repeatFetch(this.startFetching.bind(this, type, url, name, body));
        console.error(err)
      });
  }

  render() {
    const { isLoaded } = this.state;

    if (!isLoaded) {
      return '';
    }

    const body = this.renderBody();
    const style = 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
      </div>
    )
  }

  handleClick = (e) => {
    const { handleNext } = this.props;
    
    handleNext();
  }
}

const tooltipText = '<p>Of the activities you did today, click on those which you would like to spend more time doing</p>';

export default addMobileProps(addQuestion(GoodActivities, QuestionLayout, tooltipText, false));