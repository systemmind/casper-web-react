import React from 'react';
import { Trans } from 'react-i18next';
import { getButtonStyle } from './style';

export default function NextButton({ onClick, isActive, show }) {
  const buttonStyle = getButtonStyle(isActive);

  return (!show) ? false : (
    <div className='casper-question-buttons'>
      <button name='submit' className={buttonStyle} onClick={onClick}>
        <Trans>Next</Trans>
      </button>
    </div>
  );
};
