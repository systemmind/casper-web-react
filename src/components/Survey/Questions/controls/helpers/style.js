import cn from 'classnames';

export const getButtonStyle = (condition) => cn({
  'casper-btn': true,
  'casper-btn-alt': true,
  'casper-btn-passive': condition
});