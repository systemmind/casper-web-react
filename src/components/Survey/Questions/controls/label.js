import React from 'react';
import PropTypes from 'prop-types';
import QuestionLoader from '../../QuestionLoader';
import addQuestion from '../../Decorators/addQuestion';
import State from '../../../_general/State';
import NextButton from './helpers/NextButton';
import { Trans } from 'react-i18next';

const QuestionComponent = addQuestion(QuestionLoader);

class QuestionLabel extends React.Component {
  static propTypes = {  
    user: PropTypes.number.isRequired,
    survey: PropTypes.number.isRequired,
    question: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    props: PropTypes.object.isRequired,
    startLoading: PropTypes.func.isRequired,
    stopLoading: PropTypes.func.isRequired,
    onNext: PropTypes.func.isRequired,
    track: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    const { props: { children }, select, alwaysShow = false } = this.props;
    
    this.state = {
      title: null,
      forHTML: (select === 'single') 
        ? [ { changed: false, ref: null, show: alwaysShow } ]
        : children.map((value) => ({ changed: false, ref: null, show: alwaysShow }))
    };
  }

  controlsShowed = () => this.state.forHTML.some((el) => el.show);

  render() {
    const { title, hideButtons } = this.props;
    const body = this.renderBody();
    const elShowed = this.controlsShowed();

    return (
      <div className='question-container'>
        <State {...this.props} />
        <div className='question-container-all'>
          {title && (<h1 id='question-title-container'>{title}</h1>)}
          { body }
          <NextButton 
            onClick={this.handleNext} 
            isActive={!(this.state.forHTML.some(input => input.changed) && elShowed)}
            show={!hideButtons} 
          />
        </div>
      </div>
    )
  }

  renderBody = () => {
    const { props: { classes } } = this.props;

    return (
      <div className={classes.container}>
        { this.renderChildren() }
        { this.renderForHTML() }
      </div>
    )
  }

  renderChildren = () => {
    const { props, select } = this.props;
    const children = props.children.map((name, index) => {
      const { forHTML, title } = this.state;
      const indexForChanges = (select === 'single') ? 0 : index;
      const { show } = forHTML[indexForChanges];
      const className = ((select === 'single') ? (show && name === title) : show) ? 'active-label-btn' : null;
      return (
        <button onClick={this.handleShow(name, index)} className={className} key={name}>
          <Trans>{name}</Trans>
        </button>
      );
    });
    
    return (
      <div className={props.classes.children + ' question-label-buttons'}>
        {children}
      </div>
    );
  }

  renderForHTML = () => {
    const { props: { children, forHTML, classes }, select } = this.props;
    const { title } = this.state;

    const controls = [];
    if (select === 'single') {
      controls.push( this.renderControl({ ...forHTML, props: { ...forHTML.props, title } }, forHTML.name, 0) );
    } else {
      children.forEach((name, index) => {
        controls.push( this.renderControl({ ...forHTML, props: { ...forHTML.props, title: name } }, name, index) );
      });
    }

    return (
      <div className={classes.forHTML}>
        {controls}
      </div>
    );
  }

  renderControl = (control, key, index) => {
    const { forHTML } = this.state;
    const { show } = forHTML[index];

    return (
      <QuestionComponent key={key}
        {...this.props}
        {...control}
        controlOrder={index}
        hideBody={!show}
        hideButtons={true}
        onChange={() => this.handleChange(index)}
        onRef={ref => this.handleRef(index, ref)}
      />
    );
  }

  handleNext = () => {
    const { forHTML } = this.state;
    const elShowed = this.controlsShowed();

    if (!forHTML.some(child => child.changed) || !elShowed) {
      return;
    };

    const {  createRecord, name, props: { children }, startLoading, stopLoading, onNext } = this.props;
    const hooks = forHTML
      .map(async (child, index) => {
        if (child.changed)
        {
          await child.ref.handleSubmit();
          return await createRecord('textinput', index, name, children[index]);
        }
      })
      .filter(child => child instanceof Promise);;

    startLoading();

    return Promise.all(hooks)
      .then(stopLoading)
      .then(onNext)
      .catch(error => console.error(error));
  }

  handleChange = (index) => {
    const { select } = this.props; 
    const { forHTML } = this.state;
    const indexForChanges = (select === 'single') ? 0 : index;

    forHTML[indexForChanges].changed = true;
    this.setState({
      forHTML: forHTML.slice()
    });
  }

  handleShow = (name, index) => () => {
    const { alwaysShow, select } = this.props; 
    const { forHTML, title } = this.state;
    const indexForChanges = (select === 'single') ? 0 : index;

    forHTML[indexForChanges].show = (alwaysShow) ? true : !forHTML[indexForChanges].show || title !== name;

    this.setState({
      forHTML: forHTML.slice(),
      title: name
    });
  }

  handleRef = (index, that) => {
    const { select } = this.props; 
    const { forHTML } = this.state;
    const indexForChanges = (select === 'single') ? 0 : index;

    forHTML[indexForChanges].ref = that;
    this.setState({
      forHTML: forHTML.slice()
    });
  }
}

export default QuestionLabel;
