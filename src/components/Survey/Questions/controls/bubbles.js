import React from 'react';
import PropTypes from 'prop-types';
import { getButtonStyle } from './helpers/style';
import Bubble from '../../../_general/Bubble';
import Spinner from '../../../_general/Loader';
import State from '../../../_general/State';
import { Post, Delete } from '../../../../helpers';
import { Trans } from 'react-i18next';

class QuestionBubble extends React.Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    token: PropTypes.string.isRequired,
    user: PropTypes.number.isRequired,
    survey: PropTypes.number.isRequired,
    question: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    controlType: PropTypes.string.isRequired,
    props: PropTypes.object.isRequired,
    hideButtons: PropTypes.bool,
    pullRecords: PropTypes.func,
    onChange: PropTypes.func,
    onNext: PropTypes.func,
    onRef: PropTypes.func
  }

  state = { 
    isLoading: true,
    isLoaded: false,
    options: []
  }

  componentDidMount(){
    const { onRef, props, pullRecords } = this.props;
    if (onRef){
      this.props.onRef(this);
    }

    if (props){
      const { options } = props;
      const preset = (options.preset && options.preset.map(value => ({value}))) || [];
      if (options.hasOwnProperty("queries")){
        Promise.all(options.queries.map(q => pullRecords(q, true)))
          .then(result => this.setOptions(result.reduce((acc, records) => [...acc, ...records], preset)))
      }
      else {
        this.setOptions(options);
      }
    }
  }

  setOptions(options){
    const newState = {
      options: options.map(item => item.value)
                      .filter((item, i, arr) => arr.indexOf(item) == i),
      isLoaded: true,
      isLoading: false
    };

    const { name } = this.props;
    options.forEach(item => {
      const option = item.value;
      const id = (this.state[option] || (item.name == name && item.id));
      if (id) {
        newState[option] = id;
      } else {
        newState[option] = null;
      }

      const isLoading = option + 'IsLoading';
      newState[isLoading] = false;
    });

    this.setState(newState);
  }

  componentWillUnmount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(undefined);
    }
  }

  render() {
    const { isLoaded } = this.state;

    if (!isLoaded) {
      return '';
    }

    const { hideButtons, onNext } = this.props;
    const buttonStyle = getButtonStyle(false);

    return (
      <div className='question-container'>
        <State {...this.props} />
        {
          this.renderBody()
        }
        {
          !hideButtons && (
            <div className='casper-question-buttons'>
              <button name='submit' className={buttonStyle} onClick={onNext}>
                <Trans>Next</Trans>
              </button>
            </div>
          )
        }
      </div>
    )
  }

  renderBody = () => {
    const { options } = this.state;
    const { isMobile, title, props } = this.props;
    const { color } = props;

    let isLoading = false;
    const checkboxes = options.map((name, i) => {
      const id = this.state[name];
      const title = (name.length) ? name[0].toUpperCase() + name.substr(1) : name;
      const onClick = (id) ? this.removeRecord(name, id) : this.addRecord(name);
      const scale = (isMobile || window.innerWidth < 460) ? 0.6 : 0.7;
      const loadingName = name + 'IsLoading';
      const isActive = !id ? false : true;

      if (this.state[loadingName]) {
        isLoading = true;
      }

      return <Bubble scale={scale} key={title} title={title} isActive={!id} color={color} onClick={onClick} />;
    });

    return (
      <div className='question-container-all'>
        
        { title && <h1 id='question-title-goodactivities'>{title}</h1> }
        <div className='question-container-checkers question-container-bubbles'>
          {checkboxes}
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  addRecord = (value) => (e) => {
    const loadingName = value + 'IsLoading';
    const isLoading = this.state[loadingName];

    if (isLoading) {
      return false;
    }

    this.setState({ [loadingName]: true });

    const { url, user, survey, question, progress, token, name, controlType } = this.props;
    const urlRubbishBin = `${url}/employees/${user}/surveys/${survey}/questions/${question}/controls?q=${JSON.stringify({controlType})}`;
    const indent = `${survey}_${+progress - 1}`;
    const body = { value, name, indent };

    Post(urlRubbishBin, token, body)
      .then(res => this.setState({ [loadingName]: false, [value]: res.id }));
  }

  removeRecord = (value, id) => (e) => {
    const loadingName = value + 'IsLoading';
    const isLoading = this.state[loadingName];

    if (isLoading) {
      return false;
    }

    this.setState({ [loadingName]: true });

    const { url, user, survey, question, token, controlType } = this.props;
    const urlRubbishBin = `${url}/employees/${user}/controls/${id}?q=${JSON.stringify({controlType})}`;

    Delete(urlRubbishBin, token)
      .then(res => this.setState({ [loadingName]: false, [value]: null }));
  }

  handleSubmit = () => {}
}

export default QuestionBubble;
