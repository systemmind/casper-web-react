import React from 'react';
import NextButton from './helpers/NextButton';
import PropTypes from 'prop-types';
import Checkbox from '../../../_general/Checkbox';
import State from '../../../_general/State';
import { Put } from '../../../../helpers';

class QuestionRadio extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    inputOption: PropTypes.string,
    props: PropTypes.object.isRequired,
    recordData: PropTypes.object,
    pullRecords: PropTypes.func,
    startLoading: PropTypes.func.isRequired,
    stopLoading: PropTypes.func.isRequired,
    hideButtons: PropTypes.bool,
    hideBody: PropTypes.bool,
    onChange: PropTypes.func,
    onNext: PropTypes.func,
    onRef: PropTypes.func
  }

  constructor(props) {
    super(props);

    this.state = {
      value: '',
      other: ''
    };

    this.options = [];
  }

  componentDidMount(){
    const { onRef, onError } = this.props;
    if (onRef){
      this.props.onRef(this);
    }

    this.initOptions()
      .catch(error => onError(error));
  }

  componentWillUnmount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(undefined);
    }
  }

  initOptions = async () => {
    const { props, pullRecords } = this.props;
    if (props){
      const { options } = props;
      if (options.hasOwnProperty("query")){
        const records = await pullRecords(options.query, true);
        this.setOptions(records.map(item => item[options.field]));
      }
      else {
        this.setOptions(options);
      }
    }
  }

  setOptions(options){
    this.options = options;

    let tmp = {}
    options.forEach((name) => {
      tmp[name] = false;
    });

    this.setState(tmp);

    const { recordData } = this.props.props;
    const { value } = (recordData && recordData.value) ? recordData : { value: undefined };

    if (typeof value === 'string'){
      if (options.find(el => el === value)){
        this.setState({ [value]: true, value });
      } else {
        this.setState({ other: value });
      }
    }
  }

  render() {
    const { value, other } = this.state;
    const { hideButtons, hideBody = false } = this.props;

    return (
      !hideBody && <div className='question-container'>
        <State {...this.props} />
        {
          this.renderBody()
        }
        <NextButton 
          onClick={this.handleNext} 
          isActive={value.length <2 && other.length < 2}
          show={!hideButtons} 
        />
      </div>
    )
  }

  renderBody = () => {
    const { title, inputOption } = this.props;
    const { other } = this.state;
    const checkboxes = this.options.map(name => {
      const value = this.state[name];
      return <Checkbox key={name} checked={value} handleChange={this.handleChange(name)}>{name}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        { title && (<h1 id='question-title-activity'>{title}</h1>) }
        <div className='question-container-checkers'>
          <div className='question-container-checkers-options'>
            {checkboxes}
          </div>
          {
            inputOption && (
              <label className='question-container-checkers-other'>
                { inputOption }
                <input type='text' value={other} onChange={this.handleText} />
              </label>
            )
          }
        </div>
      </div>
    )
  }

  handleChange = (name) => (e) => {
    const newState = this.options.reduce((acc, val) => {
      acc[val] = (val === name) ? true : false;
      return acc;
    }, {});

    newState.value = name;
    newState.other = '';

    this.setState(newState);

    const { onChange } = this.props;
    if (onChange){
      onChange({value: name});
    }
  }

  handleText = (e) => {
    const newState = this.options.reduce((acc, val) => {
      acc[val] = false;
      return acc;
    }, {});

    newState.value = '';
    newState.other = e.target.value;

    this.setState(newState)

    const { onChange } = this.props;
    if (onChange){
      onChange({value: e.target.value});
    }
  }

  handleSubmit = () => {
    const { value, other } = this.state;
    const { url, user, survey, question, token, recordData, name } = this.props;
    const { id } = recordData;
    const data = (other.length >= 2) ? other : value;

    const apiURL = `${url}/employees/${user}/surveys/${survey}/questions/${question}/controls/${id}`;
    const apiQuery = `q=${JSON.stringify({controlType: "textinput"})}`
    return Put(`${apiURL}?${apiQuery}`, token, { value: data, name });
  }

  handleNext = () => {
    const { value, other } = this.state;

    if (value || other) {
      const submit = this.handleSubmit();

      if (submit) {
        const { startLoading, stopLoading } = this.props;
        startLoading();
        
        submit
          .then(stopLoading)
          .then(this.props.onNext)
          .catch(error => console.error(error));
      }
    }
  }
}

export default QuestionRadio;
