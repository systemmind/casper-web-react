import React from 'react';
import { PropTypes } from 'prop-types';
import NextButton from './helpers/NextButton';
import State from '../../../_general/State';
import { Put } from '../../../../helpers';

class QuestionText extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    controlType: PropTypes.string.isRequired,
    props: PropTypes.object.isRequired,
    recordData: PropTypes.object,
    startLoading: PropTypes.func.isRequired,
    stopLoading: PropTypes.func.isRequired,
    initialValue: PropTypes.string,
    hideButtons: PropTypes.bool,
    hideBody: PropTypes.bool,
    onChange: PropTypes.func,
    onNext: PropTypes.func,
    onRef: PropTypes.func
  }

  constructor(props) {
    super(props);
    const { recordData } = props;
    const { initialValue } = props.props;
    const { value } = (recordData && recordData.value) ? recordData : { value: initialValue };

    this.state = {
      value: ''
    };

    if (typeof value === 'string') {
      this.state.value = value;
    }
  }

  componentDidMount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(this);
    }
  }

  componentWillUnmount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(undefined);
    }
  }

  renderBody = () => {
    const { value } = this.state;
    const { title, expand, props, hideBody = false } = this.props;
    const { placeholder } = props;

    return (
      !hideBody && <div className='question-container-all'>
        { title && <h1 id='q-reasonfear-h1'>{title}</h1> }
        <div className='question-container-checkers question-container-checkers-reason'>
          <label className='question-container-text-input question-container-text-input-reason'>
            {
              expand ? (
                <textarea className='survey-textarea' placeholder={placeholder} type='text' value={value} maxlength="2048" onChange={this.handleChange} onKeyUp={this.onPressEnter}/>
              ) : (
                <input placeholder={placeholder} type='text' value={value} onChange={this.handleChange} onKeyUp={this.onPressEnter}/>
              )
            }
          </label>
        </div>
      </div>
    )
  }

  render() {
    const { hideButtons } = this.props;
    const { value } = this.state;
    const body = this.renderBody();

    return (
      <div className='question-container'>
        <State {...this.props} />
        { body }
        <NextButton 
          onClick={this.handleNext} 
          isActive={value.length < 2}
          show={!hideButtons} 
        />
      </div>
    )
  }

  onPressEnter = (e) => {
    const { expand } = this.props;
    if (!expand){
      if (e.keyCode === 13) {
        e.preventDefault();
        this.handleNext(e);
      }
    }
  }

  handleSubmit = () => {
    const { value } = this.state;

    const { url, user, survey, question, token, recordData, name, controlType } = this.props;
    const { id } = recordData;

    const apiURL = `${url}/employees/${user}/surveys/${survey}/questions/${question}/controls/${id}`;
    const apiQuery = `q=${JSON.stringify({controlType})}`

    return Put(`${apiURL}?${apiQuery}`, token, { value, name });
  }

  handleNext = () => {
    const { value } = this.state;

    if (value.length < 2) {
      return false;
    }

    const submit = this.handleSubmit();

    if (submit) {
      const { startLoading, stopLoading } = this.props;
      startLoading();

      submit
        .then(stopLoading)
        .then(this.props.onNext)
        .catch(error => console.error(error));
    }
  }

  handleChange = e => {
    const { value } = e.target;
    this.setState({ value });

    const { onChange } = this.props;
    if (onChange){
      onChange(value);
    }
  }
}

export default QuestionText;