import React from 'react';
import PropTypes from 'prop-types';
import NextButton from './helpers/NextButton';
import RangeInput from '../../../_general/RangeInput';
import State from '../../../_general/State';
import { Put } from '../../../../helpers';
import { Trans } from 'react-i18next';

class QuestionRange extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    controlType: PropTypes.string.isRequired,
    props: PropTypes.object.isRequired,
    recordData: PropTypes.object,
    startLoading: PropTypes.func.isRequired,
    stopLoading: PropTypes.func.isRequired,
    hideButtons: PropTypes.bool,
    hideBody: PropTypes.bool,
    onChange: PropTypes.func,
    onNext: PropTypes.func,
    onRef: PropTypes.func
  }

  constructor(props) {
    super(props);
    const { recordData } = props;
    const { initialValue } = props.props;
    const { value } = (recordData && recordData.value) ? recordData : { value: initialValue };

    this.state = { value: +value, changed: false };
  }

  componentDidMount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(this);
    }
  }

  componentWillUnmount(){
    const { onRef } = this.props;
    if (onRef){
      this.props.onRef(undefined);
    }
  }

  render() {
    const { title, hideButtons, hideBody = false } = this.props;

    return (
      !hideBody && <div className='question-container'>
        <State {...this.props} />
        <div className='question-container-all'>
          { title && (<h1 id='question-title-productivity'>{title}</h1>) }
          { this.renderBody() }
          <NextButton 
            onClick={this.handleNext} 
            isActive={!this.state.changed}
            show={!hideButtons} 
          />
        </div>
      </div>
    )
  }

  renderBody(){
    const { value } = this.state;
    const { props } = this.props;
    const { min, max, titleMin, titleMax } = props;
    const title = props && props.title; // this.props.title && props.title !== undefined ? this.props.title : props.title;

    return (
      <div className='question-container-range-input'>
        { title && <h1>{ <Trans>{title}</Trans> }</h1> }
        <RangeInput
          titleMax={titleMax}
          titleMin={titleMin}
          min={min}
          max={max}
          value={value}
          onChange={this.handleChange}
        />
      </div>
    );
  }

  handleChange = (value) => {
    this.setState({ value: +value, changed: true });
    const { onChange } = this.props;
    
    if (typeof onChange === 'function'){
      onChange({value: +value});
    }
  }

  handleSubmit = () => {
    const { value } = this.state;
    const { url, user, survey, question, token, recordData, name, controlType } = this.props;
    const { id } = recordData;

    const apiURL = `${url}/employees/${user}/surveys/${survey}/questions/${question}/controls/${id}`;
    const apiQuery = `q=${JSON.stringify({controlType})}`
    return Put(`${apiURL}?${apiQuery}`, token, { value, name });
  }

  handleNext = () => {
    const { changed } = this.state;

    if (changed) {
      const submit = this.handleSubmit();

      if (submit) {
        const { startLoading, stopLoading } = this.props;
        startLoading();
        
        submit
          .then(stopLoading)
          .then(this.props.onNext)
          .catch(error => console.error(error));
      }
    }
  }
}

export default QuestionRange;
