import React from 'react';
import PropTypes from 'prop-types';
import QuestionLoader from '../../QuestionLoader';
import addQuestion from '../../Decorators/addQuestion';
import State from '../../../_general/State';
import NextButton from './helpers/NextButton';

const QuestionComponent = addQuestion(QuestionLoader);

class QuestionContainer extends React.Component {
  static propTypes = {
    user: PropTypes.number.isRequired,
    survey: PropTypes.number.isRequired,
    question: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    props: PropTypes.object.isRequired,
    startLoading: PropTypes.func.isRequired,
    stopLoading: PropTypes.func.isRequired,
    onNext: PropTypes.func.isRequired,
    track: PropTypes.func.isRequired
  }

  state = {
    childs: this.props.props.questions.map(() => ({ changed: false, ref: null }))
  };

  render() {
    const { title, hideButtons } = this.props;

    return (
      <div className='question-container'>
        <State {...this.props} />
        <div className='question-container-all'>
          { title && (<h1 id='question-title-container'>{title}</h1>) }
          { 
            this.renderBody()
          }
          <NextButton 
            onClick={this.handleNext} 
            isActive={this.state.childs.find(child => !child.changed)}
            show={!hideButtons} 
          />
        </div>
      </div>
    )
  }

  renderBody = () => {
    const { user, survey, question, name, props, createRecord, track } = this.props;
    const { questions } = this.props.props;
    return questions.map((item, index) => {
      const key = `${user}_${survey}_${name}_${index}`;
      const { type } = item;
      return (
        <div key={key}>
          <QuestionComponent
            { ...this.props }
            { ...item }
            controlOrder={index}
            hideButtons={true}
            onChange={() => this.handleChange(index)}
            onRef={ref => this.handleRef(index, ref)}
          />
        </div>
      )
    });
  }

  handleNext = () => {
    const { childs } = this.state;
    if (childs.find(child => !child.changed)) {
      return;
    };

    const { startLoading, stopLoading, onNext } = this.props;
    const hooks = childs
      .map(child => child.ref.handleSubmit())
      .filter(child => child instanceof Promise);

    startLoading();

    return Promise.all(hooks)
      .then(stopLoading)
      .then(onNext)
      .catch(error => console.error(error));
  }

  handleChange = (index) => {
    this.state.childs[index].changed = true;
    this.setState({
      childs: this.state.childs.slice()
    });
  }

  handleRef = (index, that) => {
    this.state.childs[index].ref = that;
    this.setState({
      childs: this.state.childs.slice()
    });
  }
}

export default QuestionContainer;