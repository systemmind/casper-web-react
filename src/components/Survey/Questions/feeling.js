import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import DropdownBlock from '../../_general/DropdownBlock';
import addQuestion from '../Decorators/addQuestion';
import PlusSVG from '../../../assets/svg/plus.svg';
import Spinner from '../../_general/Loader';

const emotions = [
	'Bored', 
	'Stressed', 
	'Grateful', 
	'Sad', 
	'Excited', 
	'Challenged', 
	'Frustrated', 
	'Tired', 
	'Anxious', 
	'Meaningful', 
	'Happy', 
	'Productive', 
	'Uncertain', 
	'Confused', 
	'Reassured', 
	'Calm', 
	'Lonely', 
	'Overwhelmed'
];

const options = emotions.map((name, i) => ({ key: name, text: name, value: name}));

class QuestionFeeling extends React.Component {
  constructor(props) {
    super(props);
    const { recordData } = props;

    this.state = { default: 5, blocks: [ '' ], isLoading: false, indent: recordData[0].indent };

    const records = recordData.reduce((acc, data) => {
      acc[data.id] = {
        emotion: data.emotion || '',
        value: data.value || 5
      };
      return acc;
    }, {});

    if (Object.keys(records).length) {
      const blocks = [];

      for (let id in records) {
        const record = records[id];
        if (record.emotion) {
          blocks.push(key);
        }
      }

      if (blocks.length) {
        this.state.blocks = blocks;
      }
    }

    this.state.records = records;



    /* console.log(recordData)

    const list = options.reduce((acc, el, i) => {
      if (+recordData[0][el] > 0) acc[el] = +recordData[0][el];
      
      return acc;
    }, {}); */


    /* options.forEach((obj) => {
      this.state[obj.key] = 0;
    }); */

    /* if (Object.keys(list).length) {
      const blocks = [];

      for (let key in list) {
        blocks.push(key);
        this.state[key] = list[key];
      }

      this.state.blocks = blocks;
    } */
  }

  handleInput = (id) => (num) => {
    const { records } = this.state;
    records[id].value = num;
    this.setState({ records });
  }

  handleChange = (id) => (name, hasAnyOptions = false) => {
    const { records } = this.state;

    if (records[id].emotion === name) {
      return null;
    }

    if (emotions.includes(name) || hasAnyOptions) {
      records[id].emotion = name;
      
      this.setState({ records });
    }
  }

  handleRemove = (id) => (e) => {
    const { url, survey, token } = this.props;
    const { indent } = this.state;

    const apiURL = `${url}/survey/${survey}/question/feeling/${id}`;
    const options = {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ indent })
    };

    this.startFetching(apiURL, options, () => {
      const { records } = this.state;
      delete records[id];
      this.setState({ records });
    });
  }

  addButton = (e) => {
    const { url, survey, token } = this.props;
    const { indent } = this.state;
    const apiURL = `${url}/survey/${survey}/question/feeling`;
    const options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ indent })
    };

    this.startFetching(apiURL, options, ({ id, emotion, value }) => {
      const { records } = this.state;
      records[id] = { emotion: emotion || '', value: value || 5 };
      this.setState({ records }); 
      console.log(records);
    });
  }

  getTitle = () => {
    const { questionType } = this.props;

    switch (questionType) {
      case 'past': return 'How did you feel during this activity?';
      default: return 'How does this activity make you feel?';
    }
  }

  renderBody = () => {
    const { currentActivity } = this.props;
    const { records } = this.state;
    const blocks = Object.values(records).map(data => data.emotion);

    const body = Object.keys(records).map((id, i) => {
      const { emotion = '', value = 5 } = records[id];

      return (
        <DropdownBlock 
          hasAnyOptions={true} 
          others={blocks} 
          max={9} 
          key={id + emotion} 
          handleRemove={this.handleRemove(id)} 
          removed={i > 0} 
          options={options} 
          listValue={emotion} 
          listChange={this.handleChange(id)} 
          inputValue={value} 
          inputChange={this.handleInput(id)} 
        />
      )
    });

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 id='question-title-feeling'>{this.getTitle()}</h1>
        <div className='question-container-dropdown-block'>
          {body}
          {blocks.length < 3 && (<button className='casper-btn casper-btn-plus' onClick={this.addButton}>
            <img src={PlusSVG} className='casper-btn-plus-icon' alt='add icon' />
            Add
          </button>)}
        </div>
      </div>
    )
  }

  render() {
    const { records } = this.state;
    const value = Object.values(records).filter(record => record.value > 0 && record.emotion.length);
    const body = this.renderBody();
    const style = (!value.length) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {this.state.isLoading && <Spinner />}
      </div>
    )
  }

  handleClick = async (e) => {
    const { isLoading, records } = this.state;
    const value = Object.values(records).filter(record => record.value > 0 && record.emotion.length);
    
    if (!value.length) {
      return false;
    }

    if (!isLoading) {
      this.setState({ isLoading: true });
      await this.putNewEmotions();
    }
  }

  async putNewEmotions() {
    const { records } = this.state;
    try {
      const ids = Object.keys(records);

      for (let i = 0; i < ids.length; i++) {
        const id = ids[i];
        const { url, survey, token, handleNext } = this.props;
        const { indent } = this.state;
        const { emotion, value } = records[id];

        const apiURL = `${url}/survey/${survey}/question/feeling/${id}`;
        const options = {
          method: 'PUT',
          headers: {
            "Content-Type": "application/json",
            "token-Authorization": token
          },
          body: JSON.stringify({ emotion, value, indent })
        };

        if (i !== ids.length - 1) {
          await this.startAwaitFetching(apiURL, options, null, false);
        } else {
          await this.startAwaitFetching(apiURL, options, handleNext);
        }
      }

      return true;
    } catch (err) {
      const { repeatFetch } = this.props;
      console.log(err);
      return await repeatFetch(this.getEmotionFromSurvey.bind(this));
    }
  }

  startFetching = (url, options, callback) => {
    const { repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        if (typeof callback === 'function') {
          callback(res);
        }
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options, callback));
      });
  }

  startAwaitFetching = async (url, options, callback, hasLoadingState = true) => {
    const { repeatFetch } = this.props;

    try {
      const res = await fetch(url, options);

      if (!res.ok) {
        throw new Error(res.statusText);
      }

      const json = res.json();

      if (hasLoadingState) {
        this.setState({ isLoading: false });
      }

      if (typeof callback === 'function') {
        await callback(res);
      }
    } catch (error) {
      console.error(err);
      await repeatFetch(this.startFetching.bind(this, url, options, callback, hasLoadingState));
    }
  }
}

const tooltipText = '<p>Use “Choose feeling” button and slider to indicate best how you felt. You can add up to 3 emotions</p>';

export default addQuestion(QuestionFeeling, QuestionLayout, tooltipText);