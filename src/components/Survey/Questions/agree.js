import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import RangeInput from '../../_general/RangeInput';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  {
    key: 'meaning',
    title: 'I feel this activity was worthwhile and meaningful'
  }/*,
  {
    key: 'useful',
    title: 'I feel this activity was useful for others'
  } ,
  {
    key: 'achievement',
    title: 'I feel this activity helped me to achieve important goles'
  } */
];

class QuestionAgree extends React.Component {
  constructor(props) {
    super(props);
    const { meaning, useful } = props.recordData[0];

    this.state = {
      "meaning": (+meaning > -1) ? +meaning : 5,
      "useful": -1,
      "achievement": -1,
      isLoading: false
    }
  }

  renderBody = () => {
    const { currentActivity } = this.props;
    const body = options.map(obj => {
      return (
        <div className='question-container-range-input' key={obj.key}>
          <h1>{obj.title}</h1>
          <RangeInput 
            titleMax='Absolutely'
            titleMin='Not at all'
            min={1}
            max={9}
            value={this.state[obj.key]}
            onChange={(val) => this.setState({ [obj.key]: val })}
          />
        </div>
      );
    })

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 id='question-title-agree'>Do you agree with this statement?</h1>
        <div className='question-container-range-inputs'>
          {body}
        </div>
      </div>
    )
  }

  render() {
    const { meaning, useful, isLoading } = this.state;
    const body = this.renderBody();
    const style = (meaning === 5) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => this.setState({ [name]: !this.state[name], value: name });

  handleClick = (e) => {
    const { meaning, useful, achievement, isLoading } = this.state;
    
    if (meaning === 5) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const data = {
      meaning: meaning + '',
      useful: useful + '',
      achievement: achievement + ''
    }

    const apiURL = `${url}/survey/${survey}/question/agree/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify(data)
    };

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Use the slidebars to indicate how strongly you agree or disagree with the statements about your activity</p>';

export default addQuestion(QuestionAgree, QuestionLayout, tooltipText);
