import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Checkbox from '../../_general/Checkbox/ResearchCheckbox';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  "18-24",
  "25-35",
  "36-45",
  "46-55",
  "56-65",
  "66 and over",
  "Prefer not to say"
]

class QuestionAge extends React.Component {
  constructor(props) {
    super(props);
    const { age } = props.recordData[0];

    this.state = {
      value: '',
      isLoading: false
    };

    options.forEach((name) => {
      this.state[name] = false;
    });

    if (age) {
      this.state.value = age;
      this.state[age] = true;
    }
  }

  renderBody = () => {
    const checkboxes = options.map(name => {
      const value = this.state[name];
      
      return <Checkbox key={name} checked={value} handleChange={this.handleChange(name)}>{name}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        <h1 id='question-title-age'>How old are you?</h1>
        <div className='question-container-checkers'>
          {checkboxes}
        </div>
      </div>
    )
  }

  render() {
    const { value, isLoading } = this.state;
    const body = this.renderBody();
    const style = (value.length < 2) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => {
    const value = options.find((event) => event === name);
    const newState = options.reduce((acc, val) => {
      acc[val] = (val === name) ? true : false;
      return acc;
    }, {})
    newState.value = value;

    this.setState(newState);
  }

  handleClick = (e) => {
    const { value, isLoading } = this.state;
    
    if (value.length < 2) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;
    const apiURL = `${url}/survey/${survey}/question/age/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ age: value })
    };

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

export default addQuestion(QuestionAge, QuestionLayout);