import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import RangeInput from '../../_general/RangeInput';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  {
    key: 'productivity'
  }
];

class QuestionProductivity extends React.Component {
  constructor(props) {
    super(props);
    const { productivity } = props.recordData[0];

    this.state = {
      productivity: 5,
      isLoading: false
    };

    if (+productivity > 0) {
      this.state.productivity = +productivity;
    }
  }

  renderBody = () => {
    const body = options.map(obj => {

      return (
        <div className='question-container-range-input' key={obj.key}>
          <RangeInput 
            titleMax='Extremely'
            titleMin='Not at all'
            min={1}
            max={9}
            value={this.state[obj.key]}
            onChange={(val) => this.setState({ [obj.key]: val })}
          />
        </div>
      );
    })

    return (
      <div className='question-container-all'>
        <h1 id='question-title-productivity'>How productive did you feel today?</h1>
        <div className='question-container-range-inputs'>
          {body}
        </div>
      </div>
    )
  }

  render() {
    const { productivity, isLoading } = this.state;
    const body = this.renderBody();
    const style = (productivity === 5) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => this.setState({ [name]: !this.state[name], value: name });

  handleClick = (e) => {
    const { productivity, isLoading } = this.state;
    
    if (productivity === 5) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const data = {
      productivity: productivity + ''
    }

    const apiURL = `${url}/survey/${survey}/question/productivity/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify(data)
    };
    
    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Knowing your own capabilities, indicate how productive did you feel today</p>';

export default addQuestion(QuestionProductivity, QuestionLayout, tooltipText);