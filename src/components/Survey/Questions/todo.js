import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import RangeInput from '../../_general/RangeInput';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  {
    key: 'have',
    title: 'Have to do'
  },
  {
    key: 'should',
    title: 'Should do'
  },
  {
    key: 'want',
    title: 'Want to do'
  }
];

class QuestionTodo extends React.Component {
  constructor(props) {
    super(props);
    const { have, want, should } = props.recordData[0];

    this.state = {
      "have": (+have > -1) ? +have : 5,
      "want": (+want > -1) ? +want : 5,
      "should": (+should > -1) ? +should : 5,
      isLoading: false
    }
  }

  renderBody = () => {
    const { currentActivity } = this.props;
    const body = options.map(obj => {
      const id = obj.key + '_tooltip_icon';

      return (
        <div className='question-container-range-input' key={obj.key}>
          <div className='question-container-range-text'>
            <h1>{obj.title}</h1>
          </div>
          <RangeInput
            titleMax='Absolutely'
            titleMin='Not at all'
            min={1}
            max={9}
            value={this.state[obj.key]}
            onChange={(val) => this.setState({ [obj.key]: val })}
          />
        </div>
      );
    })

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 id='question-title-todo'>Think about this activity in terms of:</h1>
        <div className='question-container-range-inputs'>
          {body}
        </div>
      </div>
    )
  }

  render() {
    const { have, want, should, isLoading } = this.state;
    const body = this.renderBody();
    const style = (have === 5 && want === 5 && should === 5) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => this.setState({ [name]: !this.state[name], value: name });

  handleClick = (e) => {
    const { have, want, should, isLoading } = this.state;
    
    if (have === 5 && want === 5 && should === 5) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const data = {
      have: have + '',
      want: want + '',
      should: should + ''
    }

    const apiURL = `${url}/survey/${survey}/question/todo/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify(data)
    };

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<div><p><b>Have to do:</b> You have no choice to avoid this activity. It is a responsibility or duty</p><p><b>Should do:</b> You have the required skills for this activity or made a promise to someone. The outcome of this activity matters to you</p><p><b>Want to do:</b> You enjoy the process of doing this activity. You would do it no matter what outcome you achieve or how skilled you are</p></div>';

export default addQuestion(QuestionTodo, QuestionLayout, tooltipText);