import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

class QuestionOverallDayEvaluation extends React.Component {
  constructor(props) {
    super(props);
    const { value } = props.recordData[0];

    this.state = {
      value: '',
      isLoading: false
    };

    if (typeof value === 'string') {
      this.state.value = value;
    }
  }

  renderBody = () => {
    const { value } = this.state;
    const { currentActivity } = this.props;

    return (
      <div className='question-container-all'>
        {currentActivity && <div className='question-currentactivity-block'>Your activity: <b>{currentActivity}</b></div>}
        <h1 id='q-reasonfear-h1'>Please provide a few lines about your day</h1>
        <div className='question-container-checkers question-container-checkers-reason'>
          <label className='question-container-text-input question-container-text-input-reason question-container-checkers-reason-textarea'>
            <textarea className='survey-textarea' placeholder={'e.g. I’m worried about my deadlines'} type='text' value={value} onChange={(e) => this.setState({ value: e.target.value })} onKeyUp={this.onPressEnter}/>
          </label>
        </div>
      </div>
    )
  }

  render() {
    const { value, isLoading } = this.state;
    const body = this.renderBody();
    const style = (value.length < 2) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  onPressEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.handleClick(e);
    }
  }

  handleClick = (e) => {
    const { value, isLoading } = this.state;
    
    if (value.length < 2) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const apiURL = `${url}/survey/${survey}/question/overalldayevaluation/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ value })
    };
    
    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>For example, you can share what was your greatest challenge and if you were able to find the way around it. Or feel free to share something you are proud of as your today’s achievements. You can also share if you are grateful, sensitive or feeling low. Anything that affects your mood in any way – write down in this box to free your mind, reflect on it and let it completely go</p>';

export default addQuestion(QuestionOverallDayEvaluation, QuestionLayout, tooltipText);