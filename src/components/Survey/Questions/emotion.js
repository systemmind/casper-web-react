import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Checkbox from '../../_general/Checkbox/ResearchCheckbox';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

class EmotionFeedback extends React.Component {
  state = { 
    isLoading: false,
    isLoaded: false,
    options: [],
    value: null,
    isLoadingFetch: false
  }

  getSurveys = async () => {
    const { url, token, survey, repeatFetch } = this.props;
    try {
      const urlActivity = `${url}/survey/${survey}`
      const response = await fetch(urlActivity, {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      });
  
      if (!response.ok) {
        throw new Error(response.statusText);
      }
  
      const result = await response.json();
      return result;
    } catch(err) {
      console.log(err);
      return await repeatFetch(this.getSurveys.bind(this));
    }
  }

  getDailySurveys = async () => {
    const surveys = await this.getSurveys();
    let date = new Date();
    date.setHours(0, 0, 0);
    let list = surveys.filter((survey)=>{
      return date < (new Date(survey.end));
    });
    return list;
  }

  getSurveyData = async (survey) => {
    const { url, token, repeatFetch } = this.props;
    try {
      const apiURL = `${url}/survey/${survey}/questions`;
      const options = {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      }

      const questions = await fetch(apiURL, options);

      if (!questions.ok) {
        throw new Error(response.statusText);
      } 

      const questionData = await questions.json();

      if (questionData.includes('feeling')) {
        const indents = questionData.reduce((acc, el, i) => {
          if (el === 'feeling') acc.push(i);  
          return acc;
        }, []);

        return await this.getEmotionFromSurvey(survey, indents);
      } else {
        return [];
      }
    } catch (err) {
      console.log(err);
      return await repeatFetch(this.getSurveyData.bind(this, survey));
    }
  }

  async getEmotionFromSurvey(survey, indents) {
    try {
      const arr = [];
      for (let i = 0; i < indents.length; i++) {
        await this.getEmotionByIndent(survey, arr, i, indents);
      }

      return arr;
    } catch (err) {
      const { repeatFetch } = this.props;
      console.log(err);
      return await repeatFetch(this.getEmotionFromSurvey.bind(this, survey, indents));
    }
  }

  getEmotionByIndent = async (survey, arr, i, indents) => {
    const { url, token, repeatFetch } = this.props;

    try {
      const index = indents[i];
      const indent = `${survey}_${index}`;
      const urlGetData = `${url}/survey/${survey}/question/feeling?indent=${indent}`;
      const options = {
        method: 'GET',
        headers: {
          "token-Authorization": token
        }
      }
      const response = await fetch(urlGetData, options);
  
      if (response.status == 404) {
        return [];
      }
  
      const res = await response.json();
      arr.push(res[0]);
    } catch(err) {
      console.log(err);
      return await repeatFetch(this.getEmotionByIndent.bind(this, survey, arr, i, indents));
    }
  }

  getEmotions(objectList){
    let list = objectList.map((item)=>{
      delete item.survey;
      delete item.id;
      delete item.date;
      delete item.time;
      return item;
    });

    list = list.reduce((accum, item)=>{
      return Object.keys(item).reduce((accum, key)=>{
        if (item[key] > 0){
          if(accum.indexOf(key) == -1){
            accum.push(key);
          }
        }

        return accum;
      }, accum);
    }, []);

    return list;
  }

  async componentDidMount() {
    const { startLoading } = this.props;

    startLoading();
    await this.getOptions();
  };  

  getOptions = async () => {
    const { stopLoading, repeatFetch } = this.props;

    try {
      let surveys = await this.getDailySurveys();
      surveys = surveys.map((survey) => survey["id"]);
      let objList = [];

      for(const survey of surveys){
        const data = await this.getSurveyData(survey);
        
        if (data.length) {
          for (let i = 0; i < data.length; i++) {
            objList.push(data[i]);
          }
        }
      }

      const options =  this.getEmotions(objList);

      const newState = {};
      newState.options = options;

      options.forEach((option) => {
        newState[option] = false;
      });

      newState.isLoaded = true;
      newState.isLoading = false;

      stopLoading();
      this.setState(newState);
    } catch(err) {
      console.error('feedback: ', err);
      repeatFetch(this.getOptions.bind(this));
    }
  }

  renderBody = () => {
    const { options } = this.state;
    const checkboxes = options.map(name => {
      const value = this.state[name];
      const title = name[0].toUpperCase() + name.substr(1);
      
      return <Checkbox key={name} checked={value} handleChange={this.handleChange(name)}>{title}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        <h1 id='question-title-emotion'>What emotion would you like to learn about?</h1>
        <div className='question-container-checkers'>
          {checkboxes}
        </div>
      </div>
    )
  }

  render() {
    const { isLoaded, value, isLoadingFetch } = this.state;

    if (!isLoaded) {
      return '';
    }

    const body = this.renderBody();
    const style = (!value) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoadingFetch && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => {
    const { options } = this.state;
    const value = options.find((event) => event === name);
    const newState = options.reduce((acc, val) => {
      acc[val] = (val === name) ? true : false;
      return acc;
    }, {})
    newState.value = value;

    this.setState(newState);
  }

  handleClick = (e) => {
    const { url, survey, token, progress } = this.props;
    const { value, isLoadingFetch } = this.state;

    if (!value) {
      return false;
    }

    const indent = `${survey}_${+progress - 1}`;
    const data = { emotion: value, indent };

    const apiURL = `${url}/survey/${survey}/feedback`
    const options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify(data)
    };

    if (!isLoadingFetch) {
      this.startFetching(apiURL, options);
      this.setState({ isLoadingFetch: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoadingFetch: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Select the emotion you would like to know more about. We will dive deeper into this feeling, providing you feedback about how it may you and tips to deal with it</p>';


export default addQuestion(EmotionFeedback, QuestionLayout, tooltipText, false);