import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import Checkbox from '../../_general/Checkbox/ResearchCheckbox';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const defaultOptions = [
  "Brainstorming",
  "Surfing the internet",
  "Meeting",
  "Answering e-mails",
  "Having a coffee / drink",
  "Learning / training",
  "Talking by phone",
  "Chatting with peers",
  "Eating",
  "Preparing a report" 
];

const workersOptions = [
  "Equipment inspection",
  "Operating machinery",
  "Monitoring production",
  "Manual handling",
  "Packaging of products",
  "Writing up reports",
  "Reporting faults",
  "Health & safety checks",
  "Team discussions",
  "Meeting management" 
];

class QuestionActivity extends React.Component {
  constructor(props) {
    super(props);
    const { activity } = props.recordData[0];

    this.state = {
      value: '',
      other: '',
      isLoading: false
    };

    this.options = (this.props.questionType !== 'past') ? defaultOptions : workersOptions;

    this.options.forEach((name) => {
      this.state[name] = false;
    });

    if (typeof activity === 'string') {
      if (this.options.find(el => el === activity)) {
        this.state.value = activity;
        this.state[activity] = true;
      } else {
        this.state.other = activity;
      }
    }
  }

  getTitle = () => {
    const { questionType } = this.props;

    switch (questionType) {
      case 'past': return 'What was your memorable activity today?';
      default: return 'What are you currently doing?';
    }
  }

  renderBody = () => {
    const { other } = this.state;
    const checkboxes = this.options.map(name => {
      const value = this.state[name];
      
      return <Checkbox key={name} checked={value} handleChange={this.handleChange(name)}>{name}</Checkbox>;
    });

    return (
      <div className='question-container-all'>
        <h1 id='question-title-activity'>{this.getTitle()}</h1>
        <div className='question-container-checkers'>
          <div className='question-container-checkers-options'>
            {checkboxes}
          </div>
          <label className='question-container-checkers-other'>
            Other
            <input type='text' value={other} onChange={this.handleText} />
          </label>
        </div>
      </div>
    )
  }

  render() {
    const { value, other, isLoading } = this.state;
    const body = this.renderBody();
    const style = (value.length <2 && other.length < 2) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick} onKeyUp={this.onPressEnter}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  onPressEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.handleClick(e);
    }
  }

  handleChange = (name) => (e) => {
    const value = this.options.find((event) => event === name);
    const newState = this.options.reduce((acc, val) => {
      acc[val] = (val === name) ? true : false;
      return acc;
    }, {})
    newState.value = value;
    newState.other = '';

    this.setState(newState);
  }

  handleText = (e) => {
    const newState = this.options.reduce((acc, val) => {
      acc[val] = false;
      return acc;
    }, {})
    newState.value = '';
    newState.other = e.target.value;

    this.setState(newState)
  }

  handleClick = (e) => {
    const { value, other, isLoading } = this.state;
    
    if (value.length <2 && other.length < 2) {
      return false;
    }

    const { url, survey, token, recordData } = this.props;
    const data = (other.length >= 2) ? other : value;

    const apiURL = `${url}/survey/${survey}/question/activity/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({ activity: data })
    };

    if (!isLoading) {
      this.startFetching(apiURL, options, data);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options, activityName) => {
    const { handleNext, repeatFetch, handleActivity } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        if (typeof handleActivity === 'function') {
          handleActivity(activityName);
        }
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options, activityName));
      });
  }
}

const tooltipText = '<p>Select an activity from the list or add one using “Other”</p>';

export default addQuestion(QuestionActivity, QuestionLayout, tooltipText);