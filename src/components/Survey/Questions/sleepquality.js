import React from 'react';
import QuestionLayout from '../Layout/QuestionLayout';
import RangeInput from '../../_general/RangeInput';
import addQuestion from '../Decorators/addQuestion';
import Spinner from '../../_general/Loader';

const options = [
  {
    key: 'sleepquality'
  }
];

class QuestionSleepQuality extends React.Component {
  constructor(props) {
    super(props);
    const { value } = props.recordData[0];

    this.state = {
      value: 5,
      isLoading: false
    };

    if (+value > 0) {
      this.state.value = +value;
    }
  }

  renderBody = () => {
    const body = options.map(obj => {

      return (
        <div className='question-container-range-input' key={obj.key}>
          <RangeInput 
            titleMax='Excellent'
            titleMin='Terrible'
            min={1}
            max={9}
            value={this.state.value}
            onChange={(value) => this.setState({ value })}
          />
        </div>
      );
    })

    return (
      <div className='question-container-all question-container-range-one'>
        <h1 id='question-title-sleepquality'>On average, how would you rate your sleep quality last night?</h1>
        <div className='question-container-range-inputs'>
          {body}
        </div>
      </div>
    )
  }

  render() {
    const { value, isLoading } = this.state;
    const body = this.renderBody();
    const style = (value === 5) ? 'casper-btn casper-btn-alt casper-btn-passive' : 'casper-btn casper-btn-alt';
    return (
      <div className='question-container'>
        {body}
        <div className='casper-question-buttons'>
          <button name='submit' className={style} onClick={this.handleClick}>
            Next
          </button>
        </div>
        {isLoading && <Spinner />}
      </div>
    )
  }

  handleChange = (name) => (e) => this.setState({ [name]: !this.state[name], value: name });

  handleClick = (e) => {
    const { value, isLoading } = this.state;

    if (value === 5) {
      return false;
    }

    const { url, survey, token, handleNext, recordData } = this.props;

    const data = {
      value: value + ''
    }

    const apiURL = `${url}/survey/${survey}/question/sleepquality/${recordData[0].id}`;
    const options = {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify(data)
    };

    if (!isLoading) {
      this.startFetching(apiURL, options);
      this.setState({ isLoading: true });
    }
  }

  startFetching = (url, options) => {
    const { handleNext, repeatFetch } = this.props;

    fetch(url, options)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        } else {
          return res.json();
        }
      })
      .then(res => {
        this.setState({ isLoading: false });
        handleNext();
      })
      .catch(err => {
        console.error(err);
        repeatFetch(this.startFetching.bind(this, url, options));
      });
  }
}

const tooltipText = '<p>Please think about the quality of your sleep overall, such as how many hours of sleep you got, how easily you fell asleep, how often you woke up during the night (except to go to the bathroom), how often you woke up earlier than you had to in the morning, and how refreshing your sleep was</p>';

export default addQuestion(QuestionSleepQuality, QuestionLayout, tooltipText);