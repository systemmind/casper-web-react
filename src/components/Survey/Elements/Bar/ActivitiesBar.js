import React from 'react';
import PropTypes from 'prop-types';
import CheckedSVG from '../../../../assets/svg/checkmark_checked.svg';
import IdleSVG from '../../../../assets/svg/checkmark_idle.svg';

export default class ActivitiesBar extends React.Component {
  static propTypes = {
    progress: PropTypes.number.isRequired,
    currentState: PropTypes.string,
    currentStateIndex: PropTypes.number.isRequired,
    statesNumber: PropTypes.number.isRequired,
    state: PropTypes.shape({
      bar: PropTypes.shape({
        title: PropTypes.string,
      }).isRequired,
      show: PropTypes.bool.isRequired
    })
  }

  bar = React.createRef();
  input = React.createRef();

  componentDidMount() {
    this.updateRects(false);

    if (typeof window === 'object') {
      window.addEventListener('orientationchange', this.updateRects);
      window.addEventListener('resize', this.updateRects);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ((prevProps.progress !== this.props.progress) ||
        (prevProps.currentStateIndex !== this.props.currentStateIndex)) {
      this.updateRects(false);
    }
  }

  updateRects = () => {
    const { currentStateIndex } = this.props;
    if (currentStateIndex < 0){
      return;
    }

    const { left: barLeft } = this.bar.current.getBoundingClientRect();
    const { left, width } = this.input.current.getBoundingClientRect();
    const titleEl = window.document.getElementById('progress-bar-title');
    const { width: titleWidth } = titleEl.getBoundingClientRect();
    titleEl.style.left = (left - barLeft) - ((titleWidth - width) / 2) + 'px';
    titleEl.style.top = '50px';
  }

  render() {
    const { currentStateIndex, statesNumber } = this.props;
    const indexes = statesNumber;
    const number = currentStateIndex;
    const title = this.getTitle();

    const barBody = [];
    const rectBody = [];
    for (let i = 0; i < indexes + 1; i++) {
      const el = (number >= i) 
        ? <img style={{ width: 24 }} key={i + 'a'} src={CheckedSVG} alt='completed' ref={(i === number) ? this.input : null} />
        : <img style={{ width: 24 }} key={i + 'a'} src={IdleSVG} alt='unfinished' />
      barBody.push(el);
    }

    for (let i = 0; i < indexes; i++) {
      const style = { width: 100 / indexes + '%' };
      const rectangle = (number >= i) 
        ? <div style={style} key={i + 'b'} className='progress-bar-line-full'></div>
        : <div style={style} key={i + 'b'} className='progress-bar-line'></div>;
      rectBody.push(rectangle);
    }

    return (
      <div className='casper-question-progress casper-question-activities-progress' ref={this.bar}>
        <div className='progress-bar-checkboxes'>{barBody}</div>
        <div className='progress-bar-lines'>{rectBody}</div>
        <p style={{ position: 'absolute' }} id='progress-bar-title'>{title}</p>
      </div>
    );
  }

  getTitle(){
    const { state } = this.props;
    if (state){
      const { title } = state.bar;
      if (!title){
        return undefined;
      }

      const { currentStateIndex, statesNumber, currentState } = this.props;
      return title.replace("${currentStateNumber}", currentStateIndex + 1)
                  .replace("${currentStateIndex}", currentStateIndex)
                  .replace("${statesNumber}", statesNumber)
                  .replace("${currentState}", currentState);
    }
  }
}
