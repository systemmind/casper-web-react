import React from 'react';
import { Trans } from 'react-i18next';

export default ({ total, progress }) => {
  const barBody = [];
  for (let i = 0; i < total; i++) {
    const mainStyle = 'progress-block';
    const activeStyle = 'progress-block progress-block-active';
    const currentStyle = (progress > i) ? activeStyle : mainStyle;
    const cssStyle = { width: 240 / +total };

    barBody[i] = <div key={i} className={currentStyle} style={cssStyle}></div>;
  }

  return (
    <div className='casper-question-progress'>
      <p>{progress} <Trans>out of</Trans> {total}</p>
      <div className='casper-question-progress-bar'>
        {barBody}
      </div>
    </div>
  )
}