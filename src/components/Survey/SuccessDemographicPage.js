import React from 'react';
import celebratorySVG from '../../assets/img/celebratory.png';

export default () => {
  return (
    <div className='casper-success-page'>
      <img className='casper-img-celebratory' src={celebratorySVG} />
      <div>
          <h1>Thank you for completing your demographics.</h1>
          <h1>Let’s get going with your first survey!</h1>
      </div>
    </div>
  )
}