import React from 'react';
import { Trans } from 'react-i18next';
import HeaderSRC from '../../../assets/img/demographic_header.png';

export default () => (
  <div id='demographic-header' className='casper-header' style={ {backgroundImage: 'url(' + HeaderSRC + ')'} }>
    <div className='casper-header-content'>
      <div className='casper-header-texts'>
        <h1><Trans>Your demographic survey</Trans></h1>
        <h2><Trans>We need your data to better analyze your mental wellbeing. Here’s our  <a href='/policy'>Privacy Policy</a></Trans></h2>
      </div>
    </div>
  </div>
);