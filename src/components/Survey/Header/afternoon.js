import React from 'react';
import { Trans } from 'react-i18next';
import addTooltipToHeader from '../Decorators/addTooltipToHeader';
import HeaderSRC from '../../../assets/img/afternoon_header.png';
import HelpSVG from '../../../assets/svg/help_tooltip.svg';

const Header = ({ tooltipText, handleEnter, handleLeave }) => (
  <div id='afternoon-header' className='casper-header' style={ {backgroundImage: 'url(' + HeaderSRC + ')'} }>
    <div className='casper-header-content'>
      <div className='casper-header-texts'>
        <h1><Trans>Your afternoon diary</Trans></h1>
        <h2><Trans>Hope you’re doing well!</Trans></h2>
      </div>
      {tooltipText && (
        <div className='casper-header-help-tooltip'>
          <p><Trans>Help</Trans></p> 
          <img id='casper-header-help-icon' src={HelpSVG} alt='help tooltip icon' onMouseEnter={handleEnter}  onMouseLeave={handleLeave} />
        </div>
      )}
    </div>
  </div>
);

export default addTooltipToHeader(Header);