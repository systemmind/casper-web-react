import React from 'react';
import { addRouting } from '../../decorators';
import Setup from './children/Setup';

const SetupPage = (props) => {
  const isMobile = (typeof window === 'object') ? (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(window.navigator.userAgent)) : false;
  let device = 'desktop';

  if (isMobile) {
    if (/iPad|iPhone|iPod/.test(navigator.userAgent)) {
      device = 'ios';
    } else {
      device = 'android';
    }
  }

  return <Setup measurement={props.id} device={device} />;
}

const route = [ SetupPage, '/mobilesetup/:id', false, false ];
const error = [ '/404', '/mobilesetup', true, true ];

export default addRouting([ route, error ]);