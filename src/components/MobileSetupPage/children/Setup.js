import React from 'react';
import Desktop from './Desktop';
import Android from './Android';
import IOS from './IOS';
import { PolicyModal } from '../../_general/Modal';
import greetingSVG from '../../../assets/img/greeting.png';
import AlertMessage from '../../_general/Alert';
import { appAuthURL, appQRCodeURL } from './constangs';
import YouTube from '../../_general/YouTube';


class SetupPage extends React.Component {
  state = {
    tokenAutorized: false,
    token: null,
    isOpen: false,
    isClose: false,
    isTermsAccepted: false,
    isClicked: false,
    isFailed: false,
    user: null,
    password: null,
    server: null
  }

  isDev = true;

  render = () => {
    const { isOpen, isClose, isTermsAccepted, isFailed } = this.state;
    const { generateToken, handleClose } = this;
    

    return (
      <div className='setup-page'>

        {isFailed && <AlertMessage>Sorry, but we are having trouble getting your request. Please try again later or contact us</AlertMessage>}
        {isClose && !isTermsAccepted && <AlertMessage>We need you to accept Data Privacy Policy in order to get surveys. You can accept them <b style={{cursor: 'pointer'}} onClick={this.handleOpen}>here</b></AlertMessage>}
        <PolicyModal handleClose={handleClose} acceptFunc={generateToken} isOpen={isOpen} />

        <div className='setup-img-block'>
          <img className='casper-img-greeting' src={greetingSVG} />
        </div>

        {this.renderTextComponent()}
      </div>
    )
  };

  renderTextComponent = () => {
    const { device } = this.props;
    const { password, login } = this.getUserData();

    switch (device) {
      case 'ios': return <IOS login={login} password={password} handleOpen={this.openPolicy} />;
      case 'android': return <Android login={login} password={password} handleOpen={this.openPolicy} />;
      default: return <Desktop login={login} password={password} handleOpen={this.openPolicy} />;
    };
  }

  openPolicy = (e) => {
    this.setState({ isOpen: true, isClose: false, isClicked: true });
  }

  generateToken = async (e) => {
    try {
      const { measurement } = this.props;
      const { token } = this.state;
      const response = await fetch(appQRCodeURL(measurement, this.isDev), {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({})
      });
  
      const json = await response.json();
  
      sessionStorage.setItem('casper_policy', 'true');

      const tmp = {
        "measurement": measurement,
        "user": {
          "id": json.id,
          "token": json.token
        }
      };
  
      if (JSON.stringify(token) !== JSON.stringify(tmp)) {
        this.setState({ token: JSON.stringify(tmp) });
      }
  
      const { password, server, user } = await this.registerXmppUser(json.id, json.token);
      await this.authorizeUser(json.id, json.token);

      this.setState({ isTermsAccepted: true, isOpen: false, isClose: true, password, server, user })
    } catch(err) {
      console.error(err);
      this.setState({ isFailed: true, isOpen: false, isClose: true });
    }
  };

  async registerXmppUser(user, token){
    const { measurement } = this.props;
    const url = `${process.env.casperApi}/measurements/${measurement}/users/${user}/xmpp`;
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({})
    });

    return await response.json();
  }

  async authorizeUser(user, token) {
    const { measurement } = this.props;
    const url = `${process.env.casperApi}/measurements/${measurement}/users/${user}`;
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "token-Authorization": token
      },
      body: JSON.stringify({authorized: true})
    });

    return await response.json();
  }

  getUserData = () => {
    const { password, server, user } = this.state;
    if (password && server && user) {
      return { login: `${user}@${server}`, password };
    } else {
      return { login: 'XXXXXX', password: 'XXXXXX' };
    }
  }

  handleClose = (e) => this.setState({ isOpen: false, isClose: true });
  handleOpen = (e) => this.setState({ isOpen: true, isClicked: true });
}

export default SetupPage;