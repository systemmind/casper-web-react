import React from 'react';
import Button from './Button';

export default ({ login, password, handleOpen }) => (
  <div className='setup-text-block'>
    <h1 id='setup-title'>Get surveys on your mobile.</h1>
    <h1>No personal data required</h1>
    <h2>Follow this 4-step guide to get started</h2>
    <div className='setup-mobile-text-block'>
      <h3 className='setup-mobile-text-1'>1. Read and accept Data Privacy Policy</h3>
      <button className='casper-btn setup-btn' onClick={handleOpen}>
        Read Policy
      </button>
      <h3 className='setup-mobile-text-2'>2. These are your login and password. You will need them to log into mobile app. <b>Do not close this page until you’ve completed all 4 steps!</b></h3>
      <p className='setup-user-data'><b>Your login:</b></p>
      <p className='setup-user-data setup-user-data-login'>{login}</p>
      <Button classes='mobilesetup-login-btn' value={login} />
      <p className='setup-user-data'><b>Your password:</b></p>
      <p className='setup-user-data'>{password}</p>
      <Button classes='mobilesetup-password-btn' value={password} />
      <h3 className='setup-mobile-text-3'>3. Open App Store, search for <b>ChatSecure</b> mobile app and install it</h3>
      <h3 className='setup-mobile-text-4'>4. Log into <b>ChatSecure</b> mobile app by using your login and password from step 2</h3>
      {/* <Divider />
            <div className='setup-tutor'>
              <p className='setup-tutor-p'>Still having trouble?</p>
              <p className='setup-tutor-p'>Watch our tutorial video</p>
            </div>
            <YouTube link='' /> */} {/* 'MGYj6jS55DE' */}
    </div>
  </div>
);