const host = (isDev) => (isDev) ? `${process.env.appDomain}` : window.location.host;

export const appURLMacOS = (isDev = false) => `https://${host(isDev)}/releases/Setup_CASPER_Ver_1.0.dmg`;
export const appURLWindowsOS = (isDev = false) => `https://${host(isDev)}/releases/Setup_CASPER_Ver_1.0.exe`;

export const appAuthURL = (id, isDev = false) => `https://${host(isDev)}:5000/api/users/${id}`;
export const appQRCodeURL = (id, isDev = false) => `https://${host(isDev)}:5000/api/measurements/${id}/users`;
