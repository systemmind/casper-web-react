import React from 'react';
import { copyToClipboard } from '../../../helpers';

export default ({ value, style, classes }) => (
  <button 
    style={style} 
    className={'casper-btn casper-btn-plus ' + classes} 
    onClick={() => copyToClipboard(value)}
  >
    Copy
  </button>
)