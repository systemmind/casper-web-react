import React from 'react';
import Button from './Button';

export default ({ login, password, handleOpen }) => (
  <div className='setup-text-block'>
    <h1 id='setup-title'>Get surveys on your mobile.</h1>
    <h1>No personal data required</h1>
    <h2>Follow this 5-step guide to get started</h2>
    <div className='setup-mobile-text-block'>
      <h3 className='setup-mobile-text-1'>1. Read and accept Data Privacy Policy</h3>
      <button className='casper-btn setup-btn' onClick={handleOpen}>
        Read Policy
      </button>
      <h3 className='setup-mobile-text-2'>2. These are your login and password. You will need them to log into mobile app. <b>Do not close this page until you’ve completed all 5 steps!</b></h3>
      <p className='setup-user-data'><b>Your login:</b> {login}</p>
      <Button classes='mobilesetup-login-btn' value={login} />
      <p className='setup-user-data'><b>Your password:</b> {password}</p>
      <Button classes='mobilesetup-password-btn' value={password} />
      <h3 className='setup-mobile-text-3'>3. If you’re using <b>Android phone</b>, open Play Market, search for Xabber mobile app and install it</h3>
      <h3 className='setup-mobile-text-4'>4. If you’re using <b>iPhone</b>, open App Store, search for ChatSecure mobile app and install it</h3>
      <h3 className='setup-mobile-text-5'>5. Log into the mobile app by using your login and password from step 2</h3>
      {/* <Divider />
            <div className='setup-tutor'>
              <p className='setup-tutor-p'>Still having trouble?</p>
              <p className='setup-tutor-p'>Watch our tutorial video</p>
            </div>
            <YouTube link='' /> */} {/* 'MGYj6jS55DE' */}
    </div>
  </div>
);