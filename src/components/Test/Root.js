import React from 'react';
import NotFoundPage from '../404/Root';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

export default class Root extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/test" render={RenderTest} />;
          <Route path="/404" component={NotFoundPage} />
        </Switch>
      </Router>
    )
  }
}

const RenderTest = () => {
  return (
    <div>Test</div>
  )
}