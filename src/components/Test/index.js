import React from 'react';
import ReactDOM from 'react-dom';
import '../../index.css';
import '../Survey/style.css';
import TestedComponent from './Root';

ReactDOM.render(
  <div>
    <TestedComponent />
  </div>, document.getElementById('root')
);