import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown';
import RangeInput from '../RangeInput';
import DeleteCircleSVG from './delete_circle.svg';
import './style.css';

export default class DropdownBlock extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired, //[ { key: string, value: any, text: string || html } ]
    classes: PropTypes.shape({
      select: PropTypes.string, 
      input: PropTypes.string,
      menu: PropTypes.string,
      option: PropTypes.string
    }),
    value: PropTypes.any,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    others: PropTypes.array,
    hasAnyOptions: PropTypes.bool
  }

  state = {
    options: this.props.options || []
  }

  componentDidMount() {
    this.setState({ options: this.updateOptions() });
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.others) !== JSON.stringify(this.props.others)) {
      this.setState({ options: this.updateOptions() });
    }
  }

  updateOptions() {
    const { options, others, listValue } = this.props;
    const availableOptions = options.filter(el => {
      if (!others.includes(el.key) || el.key === listValue) {
        return true;
      } else {
        return false;
      }
    });
    return availableOptions;
  }

  render() {
    const {
      listValue = '',
      listChange, 
      inputValue = 4, 
      inputChange, 
      min = 1, 
      titleMin = 'Very little', 
      max = 7, 
      titleMax = 'Extremely',
      removed = false,
      handleRemove,
      hasAnyOptions = false 
    } = this.props;

    const { options } = this.state;

    return (
      <div className='casper-dropdownblock'>
        <div className='casper-dropdownblock-head'>
          <Dropdown hasAnyOptions={hasAnyOptions} options={options} value={(listValue === 'default') ? '' : listValue} onChange={listChange} />
          {removed && <img className='casper-dropdownblock-img' src={DeleteCircleSVG} alt='Delete Button' onClick={(handleRemove) ? handleRemove : (e) => false} />}
        </div>
        <RangeInput value={inputValue} onChange={(inputChange) ? inputChange : (e) => inputValue} titleMin={titleMin} titleMax={titleMax} min={min} max={max} />
      </div>
    );
  }
}