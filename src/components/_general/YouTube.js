import React from 'react';
import PropTypes from 'prop-types';
import ReactYoutube from 'react-youtube';

const playerVars = {
  autoplay: 0,
  host: 'https://www.youtube.com',
  rel: 0,
  showinfo: 0
};

export default class YouTube extends React.Component {
  static propTypes = {
    link: PropTypes.string
  }

  state = { 
    filter: true, 
    width: (window.screen.width >= 767) ? '365' : '272', 
    height: (window.screen.width >= 767) ? '206' : '154', 
    orientation: 'landscape' 
  };

  changeOrientation = () => {
    if (window.screen.width >= 767) {
      this.setState({ width: '365', height: '206' });
    } else {
      this.setState({ width: '272', height: '154' });
    }
  }

  componentDidMount() {
    if (typeof window === 'object') {
      const orientation = window.screen.msOrientation || (window.screen.orientation || window.screen.mozOrientation || {}).type;

      if (orientation === "portrait-secondary" || orientation === "portrait-primary") {
        this.setState({ orientation: 'portrait' })    
        this.changeOrientation();
      } else {
        this.changeOrientation();
      }

      window.addEventListener("orientationchange", () => {
        if (this.state.orientation === 'landscape') {
          this.setState({ orientation: 'portrait' });
          this.changeOrientation();
        } else {
          this.setState({ orientation: 'landscape' });
          this.changeOrientation();
        }
      });

      window.addEventListener("resize", () => {
        this.changeOrientation();
      });
    }
  }

  render() {
    const { link } = this.props;
    const { filter, height, width } = this.state;
    const style = (filter) ? { filter: 'grayscale(1)' } : null;
    const opts = {};
    opts.height = height;
    opts.width = width;
    opts.playerVars = playerVars;

    return (
      <div className='youtube-filter' style={style}>
        {link && <ReactYoutube
          videoId={link}
          opts={opts}
          onReady={this._onReady}
          onStateChange={this.changeFilter}
        />}
      </div>
    )
  }

  changeFilter = ({target, data }) => {
    const state = (data === 1) ? false : true;
    this.setState({ filter: state });
  }

  _onReady = (event) => {
    event.target.pauseVideo();
  }
}