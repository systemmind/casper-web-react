import React from 'react';
import PropTypes from 'prop-types';

export default class SearchInput extends React.Component {
  static propTypes = {
    handleSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    value: PropTypes.any.isRequired,
    createRef: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired
  }

  handleChange = (e) => {
    const { onChange, handleSearch } = this.props;

    handleSearch(e);
    onChange(e, e.target.value);
  }

  render() {
    const { className = '', value, placeholder, createRef } = this.props;
    const classes = 'casper-search-input ' + className;

    return (
      <input
        aria-autocomplete='list'
        onChange={this.handleChange}
        value={value}
        className={classes}
        type='text'
        placeholder={placeholder}
        ref={(ref) => createRef(ref)}
      />
    )
  }
}