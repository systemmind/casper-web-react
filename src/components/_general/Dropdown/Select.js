import React from 'react';
import PropTypes from 'prop-types';
import SearchInput from './SearchInput';
import ArrowSVG from './arrow.svg';

export default class Select extends React.Component {
  static propTypes = {
    handleSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    className: PropTypes.shape({
      selectClass: PropTypes.string,
      inputClass: PropTypes.string
    }).isRequired,
    style: PropTypes.object,
    search: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    createRef: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired
  }

  render() {
    const { className, style, placeholder, onFocus, isOpen, handleSearch, search, text, createRef, onChange } = this.props;
    const { selectClass, inputClass } = className;

    return (
      <div style={style} role='search' tabIndex="0" className={'casper-dropdown-select ' + selectClass} onFocus={onFocus} onClick={onFocus}>
        {!isOpen && <div className='casper-dropdown-select-text'>{text}</div>}
        {isOpen && <SearchInput 
          value={search} 
          onChange={onChange}
          className={inputClass} 
          placeholder={placeholder}
          handleSearch={handleSearch}
          createRef={createRef}
        />}
        <img src={ArrowSVG} className="casper-dropdown-icon" style={(!isOpen) ? { transform: 'rotate(180deg)' } : null} />
      </div>
    )
  }
}