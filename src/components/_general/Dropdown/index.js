import React from 'react';
import PropTypes from 'prop-types';
import Select from './Select';
import './style.css';

export default class Dropdown extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired, //[ { key: string, value: any, text: string || html } ]
    classes: PropTypes.shape({
      select: PropTypes.string, 
      input: PropTypes.string,
      menu: PropTypes.string,
      option: PropTypes.string
    }),
    value: PropTypes.any,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    hasAnyOptions: PropTypes.bool
  }

  state = {
    isOpen: false,
    focus: false,
    options: this.props.options || [],
    value: (this.props.value) ? this.props.value : null,
    search: ''
  }

  searchRef = null;
  defaultPlaceholder = 'Choose feeling';

  componentDidUpdate(prevProps, prevState) {
    const { isOpen, value } = this.state;
    const { hasAnyOptions } = this.props;

    const state = {};
    let needUpdate = false;

    if (JSON.stringify(prevProps.options) !== JSON.stringify(this.props.options)) {      
      state.options = this.props.options || [];
      needUpdate = true;
    }

    if (!this.ref.contains(document.activeElement) && isOpen) {      
      state.options = this.props.options || [];
      state.search = '';
      state.isOpen = false;

      if (hasAnyOptions && value) {
        state.options.push({ text: value, key: value, value });
      }

      needUpdate = true;
    }

    if (needUpdate) {
      this.setState(state);
    }
  }

  getClassNames() {
    const { classes = {} } = this.props;
    const { select = '', input = '', menu = '', option = '' } = classes;
 
    return { selectClass: select, inputClass: input, menuClass: menu, optionClass: option };
  }

  render() {
    const { isOpen, options, search, value } = this.state;
    const { placeholder = this.defaultPlaceholder } = this.props;
    const { selectClass, inputClass, menuClass, optionClass } = this.getClassNames();
    const dropdownClasses = (!isOpen) ? 'casper-dropdown ' + menuClass : 'casper-dropdown casper-dropdown-open' + menuClass;
    const menuClasses = 'casper-menu ' + menuClass;
    const dropdownStyle = (!isOpen) ? { position: 'relative' } : { position: 'relative', borderBottomColor: 'transparent' };
    
    const menuItems = options.map(object => {
      const className = 'casper-option ' + optionClass;

      return (
        <div 
          style={{ zIndex: 11 }} 
          tabIndex="0" 
          className={className} 
          data-value={object.value} 
          key={object.key}
          onClick={(e) => this.handleValue(object.value)}
        >
          {object.text}
        </div>
      );
    })

    return (
      <div ref={(ref) => this.ref = ref} style={dropdownStyle} role='combobox' className={dropdownClasses} onFocus={this.handleFocus} onBlur={this.handleBlur}>
        <Select
          style={{ zIndex: 11 }}
          className={{ selectClass, inputClass }}
          onClick={this.handleClick}
          placeholder='Search here'
          onFocus={this.handleSearchFocus}
          onChange={(e, search) => this.setState({ search })}
          isOpen={isOpen}
          search={search}
          handleSearch={this.handleSearch}
          text={(value !== null) ? this.getText(value) : placeholder}
          createRef={(ref) => this.searchRef = ref}
        />
        {isOpen && <div style={{ zIndex: 11, borderTopColor: '#CCCCCC' }} role='menu' className={menuClasses}>
          {menuItems}
        </div>}
      </div>
    );
  }

  handleValue = (value) => {
    const { onChange, hasAnyOptions } = this.props;

    if (typeof onChange === 'function') {
      this.setState({ isOpen: false });
      onChange(value, hasAnyOptions);
    } else {
      this.setState({ value: value, isOpen: false });
    }
  }

  getText = (value) => {
    const { options = [], placeholder = this.defaultPlaceholder, hasAnyOptions } = this.props;


    let result = options.find(option => option.value === value);

    if (hasAnyOptions) {
      result = { value, text: value, key: value };
    }

    return (result) ? result.text : placeholder;
  }

  handleSearch = (e) => {
    const { value } = e.target;
    const { options = [], hasAnyOptions } = this.props;
    let matchedOptions;

    if (!options.find(option => option.text.toLowerCase() === value.toLowerCase()) && hasAnyOptions) {
      let text = value.toLowerCase();

      if (text.length) {
        text = text[0].toUpperCase() + text.substr(1);
      }
      
      matchedOptions = options.filter(option => option.text.toLowerCase().match(value.toLowerCase()));
      matchedOptions.push({ text, key: text, value: text });
      this.setState({ options: matchedOptions });
    } else {
      matchedOptions = options.filter(option => option.text.toLowerCase().match(value.toLowerCase()));
      this.setState({ options: matchedOptions });
    }
  }

  handleClick = (e) => {
    const { isOpen } = this.state;

    if (isOpen) {
      this.setState({ isOpen: false, searchOnFocus: false, itemOnFocus: false });
    } else {
      this.setState({ isOpen: true, searchOnFocus: true, itemOnFocus: false });
    }
  }

  handleFocus = (e) => {
    this.setState({ focus: true });
  }

  handleSearchFocus = (e) => {
    
    this.setState({ focus: true, isOpen: true });

    setTimeout(() => {
      const { searchRef } = this;

      if (searchRef) {
        searchRef.focus();
      }
    }, 10)
  }

  handleBlur = (e) => {   
    const { value } = e.target;
    const { hasAnyOptions } = this.props;
    setTimeout(() => {
      if (hasAnyOptions && value) {
        this.handleValue(value);
      }
      this.setState({ focus: false });
    }, 5);
  }
}