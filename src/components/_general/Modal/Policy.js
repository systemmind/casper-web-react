import React from 'react';
import PropTypes from 'prop-types';
import ModalWindow from './Window';
import Text from './text';

class PolicyModal extends React.Component {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    acceptFunc: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired
  }

  render() {
    return <ModalWindow {...this.props} content={<Text />} title={'Data Privacy Policy'} />
  }
}

export default PolicyModal;