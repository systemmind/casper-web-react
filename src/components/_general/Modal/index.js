import ModalWindow from './Window';
import PolicyModal from './Policy';
import './style.css';
import './media.css';

export default ModalWindow;
export { PolicyModal };