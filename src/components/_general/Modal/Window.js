import React from 'react';
import PropTypes from 'prop-types';
import CloseSVG from '../../../assets/svg/close.svg';

class ModalWindow extends React.Component {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    acceptFunc: PropTypes.func.isRequired,
    content: PropTypes.object,
    title: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired
  }

  render() {
    const { content, acceptFunc, handleClose, title, isOpen } = this.props;
    if (!isOpen) {
      return '';
    }
    return (
      <div className='modal-window'>
        <div className='modal-content-wrap'>
          <div className='modal-content-block'>
            <div className='modal-content-header'>
              <img src={CloseSVG} alt='close button' className='modal-close-icon' onClick={handleClose} />
              <h1>{title}</h1>
            </div>
            <div className='modal-content-text'>
              {content}
            </div>
            <div className='modal-buttons-block'>
              <button className='casper-btn' onClick={acceptFunc}>
                Accept
              </button>
              <button className='casper-btn-sec' onClick={handleClose}>
                Close
              </button>
            </div>
          </div>
        </div>
        <div className='modal-back'/>
      </div>
    )
  }
}

export default ModalWindow;