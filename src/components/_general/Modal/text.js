import React from 'react';

export default () => {
  const p1 = (
    <p>We take your privacy very seriously. Please read this privacy policy carefully as it contains important
  information on who we are and how and why we collect, store, use and share your personal information. It
  also explains your rights in relation to your personal information and how to contact us or supervisory
  authorities in the event you have a complaint.
    </p>
  );
  const p2 = (
    <p>We collect, use and are responsible for certain personal information about you. When we do so we are
  subject to the General Data Protection Regulation, which applies across the European Union (including in
  the United Kingdom) and we are responsible as ‘controller’ of that personal information for the purposes of
  those laws.
    </p>
  );
  
  const b1 = <p><b>Key terms</b></p>;
  const p3 = <p>It would be helpful to start by explaining some key terms used in this policy:</p>;
  const t1 = (
    <table>
      <tbody>
        <tr>
          <td>We, us, our</td>
          <td>Sonas-Behavioural Science LTD</td>
        </tr>
        <tr>
          <td>Our representative</td>
          <td>
            <p>Alena Rogozhkina, CEO</p>
            <p>Illia Rohozhkin, CTO</p>
          </td>
        </tr>
        <tr>
          <td>Personal information</td>
          <td>Any information relating to an identified or identifiable individual</td>
        </tr>
        <tr>
          <td>Special category personal information</td>
          <td>
            <p>Personal information revealing racial or ethnic origin, political opinions, religious beliefs, philosophical beliefs or trade union membership</p>
            <p>Genetic and biometric data</p>
            <p>Data concerning health, sex life or sexual orientation</p>
          </td>
        </tr>
      </tbody>
    </table>
  );
  
  const b2 = <p><b>Personal information we collect about you</b></p>;
  const p4 = <p>We may collect and use the following personal information about you:</p>;
  const ul1 = (
    <ul>
      <li>your name and contact information, including email address and/or telephone number</li>
      <li>Information to enable us to check and verify your identity, eg your date of birth</li>
      <li>your gender information[, if you choose to give this to us</li>
      <li>location data , if you choose to give this to us</li>
      <li>your personal or professional interests</li>
      <li>information about how you use our website, IT, communication and other systems your responses to surveys and questionnaires</li>
      <li>some sensitive data about your mental wellbeing such as your self reported emotions and feelings, and factors that make you feel the way you do</li>
    </ul>
  );
  const p4a = <p>This personal information is required to provide you with using CASPER – our digital platform to measure workplace wellbeing and productivity in the workplace. If you do not provide personal information we ask for, it may delay or prevent us from providing a sensible report about your own data to you since we can’t guarantee that data analysis would be significantly valid.</p>;
  
  const b2a = <p><b>How your personal information is collected</b></p>;
  const p5 = <p>We collect most of this personal information directly from you—via our webserver, push notifications directly to your browser and apps.</p>;
  
  const b3 = <p><b>How and why we use your personal information</b></p>;
  const p6 = <p>Under data protection law, we can only use your personal information if we have a proper reason for doing so, eg:</p>;
  const ul2 = (
    <ul>
      <li>to comply with our legal and regulatory obligations;</li>
      <li>for the performance of our contract with you or to take steps at your request before entering into a contract;</li>
      <li>where you have given consent.</li>
    </ul>
  );
  const p7 = <p>A legitimate interest is when we have a business or commercial reason to use your information, so long as this is not overridden by your own rights and interests.</p>;
  
  const p8 = <p>The table below explains what we use (process) your personal information for and our reasons for doing so:</p>;
  const t2 = (
    <table>
      <tbody>
        <tr>
          <th>What we use your personal information for</th>
          <th>Our reasons</th>
        </tr>
        <tr>
          <td>To provide CASPER as a measurement tool to you</td>
          <td>For data collection following Experience Sampling Methodology to develop an innovative software platform to evaluate mental wellbeing and productivity in the workplace</td>
        </tr>
        <tr>
          <td>Gathering and providing information required by or relating to audits, enquiries or investigations by regulatory bodies</td>
          <td>To comply with our legal and regulatory obligations such as Research and Development audit, due diligence to secure funding etc.</td>
        </tr>
        <tr>
          <td>Ensuring business policies are adhered to, eg policies covering security and internet use</td>
          <td>For our legitimate interests or those of a third party, ie to make sure we are following our own internal procedures so we can deliver the best service to you</td>
        </tr>
        <tr>
          <td>Ensuring the confidentiality of commercially sensitive information</td>
          <td>
            <p>For our legitimate interests or those of a third party, ie to protect trade secrets and other commercially valuable information</p>
            <p>To comply with our legal and regulatory obligations</p>
          </td>
        </tr>
        <tr>
          <td>Statistical analysis in relation to our data analysis framework</td>
          <td>For our legitimate interests to generate reports and feedback as efficient as we can so we can deliver the best service for you at the best price</td>
        </tr>
        <tr>
          <td>Preventing unauthorised access and modifications to systems</td>
          <td>
            <p>For our legitimate interests or those of a third party, ie to prevent and detect criminal activity that could be damaging for us and for you</p>
            <p>To comply with our legal and regulatory obligations</p>
          </td>
        </tr>
        <tr>
          <td>Ensuring safe working practices, staff administration and assessments</td>
          <td>
            <p>To comply with our legal and regulatory obligations</p>
            <p>For our legitimate interests or those of a third party, eg to make sure we are following our own internal procedures and working efficiently so we can deliver the best service to you</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Marketing our services and those of selected third parties to:</p>
            <ul>
              <li>existing and former customers;</li>
              <li>third parties who have previously expressed an interest in our services;</li>
              <li>third parties with whom we have had no previous dealings.</li>
            </ul>
          </td>
          <td>For our legitimate interests or those of a third party, ie to promote our business to existing and former customers</td>
        </tr>
        <tr>
          <td>External audits and quality checks</td>
          <td>
            <p>For our legitimate interests or a those of a third party, ie to maintain our accreditations so we can demonstrate we operate at the highest standards</p>
            <p>To comply with our legal and regulatory obligations</p>
          </td>
        </tr>
      </tbody>
    </table>
  );
  const i1 = <p><i>[The above table does not apply to special category personal information, which we will only process with your explicit consent.]</i></p>;
  
  const b4 = <p><b>Promotional communications</b></p>;
  const p9 = <p>We may use your personal information to send you updates (by email, text message, telephone or post) about our products AND/OR services, including exclusive offers, promotions or new products AND/OR services.</p>;
  const p10 = <p>We will always treat your personal information with the utmost respect and never sell OR share it with other organisations for marketing purposes.</p>;
  
  const b5 = <p><b>Who we share your personal information with</b></p>;
  const p11 = <p>We routinely share personal information with:</p>;
  const ul3 = (
    <ul>
      <li>third parties we use to help deliver our products and services to you, eg software development agencies, universities, research and development partners, universities, creative agencies;</li>
      <li>other third parties we use to help us run our business, eg marketing agencies or website hosts;</li>
      <li>our banks;</li>
      <li>our mentors and advisors from the entrepreneurial eco system.</li>
    </ul>
  );
  
  const p12 = <p>We only allow our service providers to handle your personal information if we are satisfied they take appropriate measures to protect your personal information. We also impose contractual obligations on service providers relating to ensure they can only use your personal information to provide services to us and to you. We may also share personal information with external auditors, Investors </p>;
  const p13 = <p>We may disclose and exchange information with law enforcement agencies and regulatory bodies to comply with our legal and regulatory obligations.</p>;
  const p14 = <p>We may also need to share some personal information with other parties, such as potential buyers of some or all of our business or during a re-structuring. Usually, information will be anonymised but this may not always be possible. The recipient of the information will be bound by confidentiality obligations.</p>;
  const p15 = <p>We will not share your personal information with any other third party.</p>
  
  const b6 = <p><b>Where your personal information is held</b></p>;
  const p16 = <p>Information may be held at our offices and those of our third party agencies, service providers, representatives and agents as described above (see above: <b>‘Who we share your personal information with’</b>).</p>;
  const p17 = <p>Some of these third parties may be based outside the European Economic Area. For more information, including on how we safeguard your personal information when this occurs, see below: <b>‘Transferring your personal information out of the EEA’</b>.</p>;
  
  const b7 = <p><b>How long your personal information will be kept</b></p>;
  const p18 = <p>We will keep your personal information while you have an account with us or we are providing products and/our services to you. Thereafter, we will keep your personal information for as long as is necessary:</p>;
  const ul4 = (
    <ul>
      <li>to respond to any questions, complaints or claims made by you or on your behalf;</li>
      <li>to show that we treated you fairly;</li>
      <li>to keep records required by law.</li>
    </ul>
  );
  const p19 = <p>We will not retain your personal information for longer than necessary for the purposes set out in this policy. Different retention periods apply for different types of personal information. When it is no longer necessary to retain your personal information, we will delete or anonymise it.</p>;
  
  const b8 = <p><b>Transferring your personal information out of the EEA</b></p>;
  const p20 = <p>To deliver services to you, it is sometimes necessary for us to share your personal information outside the European Economic Area (EEA), eg:</p>;
  const ul5 = (
    <ul>
      <li>with our software development consultancy outside the EEA (Ukraine);</li>
      <li>if you are based outside the EEA;</li>
      <li>where there is an international dimension to the services we are providing to you.</li>
    </ul>
  );
  const p21 = <p>These transfers are subject to special rules under European and UK data protection law.</p>;
  const p22 = <p>The following countries to which we may transfer personal information have been assessed by the European Commission as providing an adequate level of protection for personal information: Ukraine</p>;
  const p23 = <p>Except for the countries listed above, these non-EEA countries do not have the same data protection laws as the United Kingdom and EEA. We will, however, ensure the transfer complies with data protection law and all personal information will be secure. Our standard practice is to use standard data protection contract clauses that have been approved by the European Commission.</p>;
  const p24 = <p>If you would like further information please contact us (see ‘How to contact us’ below).</p>;
  
  const b9 = <p><b>Your rights</b></p>;
  const p24a = <p>You have the following rights, which you can exercise free of charge:</p>;
  const t3 = (
    <table>
      <tbody>
        <tr>
          <td>Access</td>
          <td>The right to be provided with a copy of your personal information (the right of access)</td>
        </tr>
        <tr>
          <td>Rectification</td>
          <td>The right to require us to correct any mistakes in your personal information</td>
        </tr>
        <tr>
          <td>To be forgotten</td>
          <td>The right to require us to delete your personal information—in certain situations</td>
        </tr>
        <tr>
          <td>Restriction of processing</td>
          <td>The right to require us to restrict processing of your personal information—in certain circumstances, eg if you contest the accuracy of the data</td>
        </tr>
        <tr>
          <td>Data portability</td>
          <td>The right to receive the personal information you provided to us, in a structured, commonly used and machine-readable format and/or transmit that data to a third party—in certain situations</td>
        </tr>
        <tr>
          <td>To object</td>
          <td>
            <p>The right to object:</p>
            <ul>
              <li>at any time to your personal information being processed for direct marketing (including profiling);</li>
              <li>in certain other situations to our continued processing of your personal information, eg processing carried out for the purpose of our legitimate interests.</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>Not to be subject to automated individual decision making</td>
          <td>The right not to be subject to a decision based solely on automated processing (including profiling) that produces legal effects concerning you or similarly significantly affects you</td>
        </tr>
      </tbody>
    </table>
  );
  const p25 = <p>For further information on each of those rights, including the circumstances in which they apply, please contact us or see the <i>Guidance from the UK Information Commissioner’s Office (ICO) on individuals’ rights under the General Data Protection Regulation.</i></p>;
  
  const p26 = <p>If you would like to exercise any of those rights, please:</p>;
  const ul6 = (
    <ul>
      <li>email, call or write to us OR  (see below: ‘How to contact us’;) and</li>
      <li>let us have enough information to identify you (please provide your full name, job role and an your employer name)</li>
      <li>let us have proof of your identity and address (a copy of your driving licence or passport and a recent utility or credit card bill); and</li>
      <li>let us know what right you want to exercise and the information to which your request relates.</li>
    </ul>
  );
  
  const b10 = <p><b>Keeping your personal information secure</b></p>;
  const p27 = <p>We have appropriate security measures to prevent personal information from being accidentally lost, or used or accessed unlawfully. We limit access to your personal information to those who have a genuine business need to access it. Those processing your information will do so only in an authorised manner and are subject to a duty of confidentiality. </p>;
  const p28 = <p>We also have procedures in place to deal with any suspected data security breach. We will notify you and any applicable regulator of a suspected data security breach where we are legally required to do so.</p>;
  const p29 = <p>If you want detailed information from Get Safe Online on how to protect your information and your computers and devices against fraud, identity theft, viruses and many other online problems, please visit <b>www.getsafeonline.org</b>. Get Safe Online is supported by HM Government and leading businesses.]</p>;
  
  const b11 = <p><b>How to complain</b></p>;
  const p30 = <p>We hope that we can resolve any query or concern you may raise about our use of your information. </p>;
  const p31 = <p>The <b>General Data Protection Regulation</b> also gives you right to lodge a complaint with a supervisory authority, in particular in the European Union (or European Economic Area) state where you work, normally live or where any alleged infringement of data protection laws occurred. The supervisory authority in the UK is the Information Commissioner who may be contacted at <b>https://ico.org.uk/concerns</b> or telephone: 0303 123 1113.</p>;
  
  const b12  = <p><b>Changes to this privacy policy</b></p>;
  const p32 = <p>This privacy notice was published on 23/04/2018 and last updated on [09/11/2019].</p>;
  const p33 = <p>We may change this privacy notice from time to time—when we do we will inform you via  our website or a our app notification.</p>;
  
  const b13  = <p><b>How to contact us</b></p>;
  const p34 = <p>Please contact us by post, email or telephone if you have any questions about this privacy policy or the information we hold about you.</p>;
  const p35 = <p>Our contact details are shown below:</p>;
  const t4 = (
    <table>
      <tbody>
        <tr>
          <th>Our contact details</th>
        </tr>
        <tr>
          <td>
            <p>Sonas-Behavioural Science LTD</p>
            <p>The Hive</p>
            <p>Suites 2-3, Scion House</p>
            <p>Stirling University Innovation Park</p>
            <p>Stirling, UK</p>
            <p>FK9 4NF</p>
            <a href='connect@thehappinessatworkproject.com'>connect@thehappinessatworkproject.com</a>
            <br />
            <a href="tel:+4407479822232">+44 (0) 7479 822232</a>
          </td>
        </tr>
      </tbody>
    </table>
  );
  
  const b14  = <p><b>Do you need extra help?</b></p>;
  const p36 = <p>If you would like this notice in another format (an email) please contact us (see ‘How to contact us’ above).</p>;

  return (
    <div>
      {p1}
      {p2}
      <br />
      {b1}
      {p3}
      {t1}
      <br />
      {b2}
      {p4}
      {ul1}
      {p4a}
      <br />
      {b2a}
      {p5}
      <br />
      {b3}
      {p6}
      {ul2}
      {p7}
      <br />
      {p8}
      {t2}
      {i1}
      {b4}
      {p9}
      {p10}
      <br />
      {b5}
      {p11}
      {ul3}
      <br />
      {p12}
      {p13}
      {p14}
      {p15}
      <br />
      {b6}
      {p16}
      {p17}
      <br />
      {b7}
      {p18}
      {ul4}
      {p19}
      <br />
      {b8}
      {p20}
      {ul5}
      {p21}
      {p22}
      {p23}
      {p24}
      <br />
      {b9}
      {p24a}
      {t3}
      {p25}
      <br />
      {p26}
      {ul6}
      <br />
      {b10}
      {p27}
      {p28}
      {p29}
      <br />
      {b11}
      {p30}
      {p31}
      <br />
      {b12}
      {p32}
      {p33}
      <br />
      {b13}
      {p34}
      {p35}
      {t4}
      <br />
      {b14}
      {p36}
    </div>
  );
}