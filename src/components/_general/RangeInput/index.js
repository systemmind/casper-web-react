import React from 'react';
import PropTypes from 'prop-types';
import { Slider, Rail, Handles, Tracks } from 'react-compound-slider';
import './style.css';

class RangeInput extends React.Component {
  static propTypes = {
    titleMin: PropTypes.string,
    titleMax: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
  }

  handleChange = ([ newValue ]) => {
    const { onChange, value } = this.props;

    if (typeof onChange === 'function') {
      if (value && newValue !== value) {
        onChange(newValue);
      } 
    }
  }

  render() {
    const { titleMin = 'Minimum', titleMax = 'Maximum' } = this.props; 

    return (
      <div className='range-input-block'>
        <div className='range-input-titles'>
          <span className='range-input-left-title'>{titleMin}</span>
          <span className='range-input-right-title'>{titleMax}</span>
        </div>
        {this.renderSlider()}
      </div>
    )
  }

  renderSlider() {
    const { min = 1, max = 10, value = 1, step = 1 } = this.props;
    const { sliderStyle, railStyle, Handle, Track } = this;
    const style = Object.assign({}, railStyle);

    if (!((min + max) % 2)) {
      const middle = Math.floor((min + max) / 2);
      if (value !== middle) {
        style.background = this.getBackColor(value, middle);
      }
    }

    return (
      <Slider
        rootStyle={sliderStyle}
        domain={[+min, +max]}
        step={step}
        mode={2}
        values={[value]}
        onChange={ this.handleChange }
        onUpdate={ this.handleChange }
      >
        <Rail>
          {({ getRailProps }) => <div className='range-input-rail' style={style} {...getRailProps()} /> }
        </Rail>
        <Handles>
          {({ handles, getHandleProps }) => (
            <div className="slider-handles">
              {handles.map(handle => (
                <Handle
                  key={handle.id}
                  handle={handle}
                  getHandleProps={getHandleProps}
                />
              ))}
            </div>
          )}
        </Handles>
        <Tracks right={false}>
          {({ tracks, getTrackProps }) => (
            <div className="slider-tracks">
              {tracks.map(({ id, source, target }) => (
                <Track
                  key={id}
                  source={source}
                  target={target}
                  getTrackProps={getTrackProps}
                  min={min} 
                  max={max}
                  value={value}
                />
              ))}
            </div>
          )}
        </Tracks>
      </Slider>
    );
  }

  sliderStyle = {
    position: 'relative',
    width: '100%',
  }

  railStyle = { 
    position: 'absolute',
    width: '100%'
  }

  Handle = ({ handle: { id, value, percent }, getHandleProps }) => {
    return (
      <div
        className='range-input-handle'
        style={{
          left: (percent === 0) ? '1.35%' : `${percent}%`,
          position: 'absolute',
          marginLeft: -15,
          marginTop: 4,
          zIndex: 2,
          border: 0,
          textAlign: 'center',
          cursor: 'pointer',
          borderRadius: '50%'
        }}
        {...getHandleProps(id)}
      >
      </div>
    )
  }

  Track = ({ source, target, getTrackProps, min, max, value }) => {
    const style = {
      position: 'absolute',
      zIndex: 1,
      cursor: 'pointer',
      left: `${source.percent}%`,
      width: `${target.percent - source.percent}%`,
    };

    if (!((min + max) % 2)) {
      const middle = Math.floor((min + max) / 2);
      if (value !== middle) {
        style.background = this.getBackColor(value, middle);
      }
    }

    return (
      <div
        className='range-input-track'
        style={style}
        {...getTrackProps()}
      />
    )
  }

  getBackColor(value, middle) {
    switch (true) {
      case (value === middle - 1): return '#BBB6BF';
      case (value === middle - 2): return '#ABA0B2';
      case (value <= middle - 3): return '#9C8CA5';
      case (value === middle + 1): return '#D9C882';
      case (value === middle + 2): return '#DAC15C';
      case (value >= middle + 3): return '#E9C946';
      default: return '#D3D6D4';
    }
  }
}

export default RangeInput;