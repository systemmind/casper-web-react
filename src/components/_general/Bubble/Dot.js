export default class Dot {
  constructor(x = 0, y = 0, radius = 2, color = "#ff6600") {
    this.x = x;
    this.y = y;
    this.initialX = x;
    this.initialY = y;
  
    this.radius = radius;
    this.color = color;
    this.velocityX = 0;
    this.velocityY = 0;
    this.friction = 0.29;
    this.springFactor = 0.2;
  }

  reactTo(mouse) {
    // we use center of the mouse position and the center of dot position
    // have to find line from a dot position x/y to the mouse position x/y
    let lineX = this.x - mouse.x;
    let lineY = this.y - mouse.y;

    // use Pythagorean theorem to find a distance from the mouse position to the dot position
    let distance = Math.sqrt(lineX * lineX + lineY * lineY);

    // interaction
    const reactionDistance = 30;
    if (distance < reactionDistance) {
      let angle = Math.atan2(lineY, lineX); //you can change this to arccos(cos) - Math.acos( Math.cos( lineY / distance ) );
      //delta - distance from the mouse position to the border of the reaction distance radius
      let deltaX = mouse.x + Math.cos(angle) * reactionDistance; 
      let deltaY = mouse.y + Math.sin(angle) * reactionDistance;
      
      //velocity - distance from the dot position to the border of the reaction distance radius
      this.velocityX += deltaX - this.x;
      this.velocityY += deltaY - this.y;
    }

    // spring back
    let dx1 = -(this.x - this.initialX);
    let dy1 = -(this.y - this.initialY);

    this.velocityX += dx1 * this.springFactor;
    this.velocityY += dy1 * this.springFactor;


    // friction
    this.velocityX *= this.friction;
    this.velocityY *= this.friction;

    // actual move
    this.x += this.velocityX;
    this.y += this.velocityY;
  }
}