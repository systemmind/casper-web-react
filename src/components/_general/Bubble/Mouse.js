export default class Mouse {
  constructor(canvas) {
    this.x = 0;
    this.y = 0;
    var rect = canvas.getBoundingClientRect();
    

    canvas.onmousemove = e => {
      this.x = e.clientX - rect.left;
      this.y = e.clientY - rect.top;
    };

    const changePosition = () => {
      var rect = canvas.getBoundingClientRect();
    
      canvas.onmousemove = e => {
        this.x = e.clientX - rect.left;
        this.y = e.clientY - rect.top;
      };
    };

    window.addEventListener('resize', changePosition, true);

    window.addEventListener('orientationchange', changePosition, true);

    window.addEventListener("scroll", changePosition, true);
  }
}