import React from 'react';
import PropTypes from 'prop-types';
import Mouse from './Mouse';
import Dot from './Dot';
import './style.css';

export default class Bubble extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onClick: PropTypes.func,
    color: PropTypes.string,
    isActive: PropTypes.bool
  }

  constructor(props) {
    super(props);
    const { isActive, color } = props;
    this.passiveColor = '#DADADA';
    this.scale = props.scale || 0.75;
    this.state = {
      isActive,
      rgbStart: (isActive) ? color : this.passiveColor,
      rgbEnd: (isActive) ? this.passiveColor : color
    }
  }

  generateId = () => new Array(10).fill(null).map((el) => Math.round(Math.random() * 10)).join('');

  id = 'bubble_' + this.generateId();
  

  componentDidMount() {
    let canvas = document.getElementById(this.id);
    let posX = canvas.width / 2;
    let posY = canvas.height / 2;
    let ctx = canvas.getContext("2d");
    let mousePos = new Mouse(canvas);
    let dots = [];
    const maximumDots = 20;

    for (var i = 0; i < maximumDots; i++) {
      const k = 70 + Math.random() * 14;
      dots.push(
        new Dot(
          100 * this.scale + k * Math.cos(i * 2 * Math.PI / maximumDots) * this.scale,
          100 * this.scale + k * Math.sin(i * 2 * Math.PI / maximumDots) * this.scale
        )
      );
    }

    this.renderAnim(ctx, dots, mousePos, posX, posY);
  }

  connectDots(ctx, dots) {
    const { rgbStart } = this.state;
    ctx.beginPath();
    ctx.fillStyle = rgbStart;

    for (var i = 0, jlen = dots.length; i <= jlen; ++i) {
      var p0 = dots[i + 0 >= jlen ? i + 0 - jlen : i + 0];
      var p1 = dots[i + 1 >= jlen ? i + 1 - jlen : i + 1];
      ctx.quadraticCurveTo(p0.x, p0.y, (p0.x + p1.x) * 0.5, (p0.y + p1.y) * 0.5);
    }

    ctx.closePath();
    ctx.fill();
  }

  getLines(ctx, text, maxWidth) {
    var words = text.split(" ");
    var lines = [];
    var currentLine = words[0];

    for (var i = 1; i < words.length; i++) {
      var word = words[i];
      var width = ctx.measureText(currentLine + " " + word).width;
      if (width < maxWidth) {
        currentLine += " " + word;
      } else {
        lines.push(currentLine);
        currentLine = word;
      }
    }
    lines.push(currentLine);
    return lines;
  }

  renderAnim(ctx, dots, mousePos, posX, posY) {
    const { title, textColor = '#000000', isActive } = this.props;
    let canvas = document.getElementById(this.id);
    if (!canvas){
      return ;
    }

    const min = 30 * this.scale;
    const max = 170 * this.scale;

    if (mousePos.x > min && mousePos.x < max && mousePos.y > min && mousePos.y < max) {
      canvas.style.cursor = 'pointer';
    } else {
      canvas.style.cursor = 'default';
    }

    window.requestAnimationFrame(this.renderAnim.bind(this, ctx, dots, mousePos, posX, posY));
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    if (isActive) {
      dots.forEach(dot => {
        dot.reactTo(mousePos);
      });
    }

    this.connectDots(ctx, dots);

    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = `${Math.floor(22 * this.scale)}px Lato`; //`bold ${Math.floor(22 * this.scale)}px Lato`;
    ctx.fillStyle = textColor;

    const lines = this.getLines(ctx, title, 120 * this.scale);
    lines.forEach((text, i) => {
      const ySize = (lines.length > 1) ? lines.length * (16 * this.scale) : 0;
      const linePosY = posY - ySize / 2 + (30 * this.scale) * i;
      ctx.fillText(text, posX, linePosY);
    })
  }

  render() {
    const { isActive } = this.props;
    

    return (
      <div className='casper-bubble'>
        <canvas id={this.id} width={200 * this.scale} height={200 * this.scale} onClick={this.handleClick} />
      </div>
    )
  }

  handleClick = (e) => {
    const { onClick, isActive, color } = this.props;
    const { rgbStart, rgbEnd } = this.state;
    this.setState({ 
      rgbStart: (!isActive) ? color : this.passiveColor,
      rgbEnd: (!isActive) ? this.passiveColor : color 
    });
    if (typeof onClick === 'function') {
      onClick(e);
    }
  }
}