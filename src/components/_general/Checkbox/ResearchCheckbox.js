import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

class ResearchCheckbox extends React.Component {
  static propTypes = {
    handleChecked: PropTypes.func,
    checked: PropTypes.bool,
    handleChange: PropTypes.func
  }

  state = {
    isChecked: false
  }

  render() {
    const { checked, children = 'Checkbox', handleChecked = () => null, handleChange = () => null } = this.props;
    const { isChecked } = this.state;
    const value = (checked === undefined) ? isChecked : checked;
    const func = (checked === undefined) ? () => this.setState({ isChecked: !isChecked }) : handleChecked;

    return (
      <label className='check option option_research'>
        <input 
          className='check__input' 
          checked={value}
          type='checkbox'
          ref={(el) => this.ref = el}
          onChange={handleChange}
        />
        <span className='check__box_research'
          onClick={func}
        />
        {children}
      </label>
    )
  }
}

export default ResearchCheckbox;