import React from 'react';
import AlertSVG from '../../../assets/svg/alert.svg';
import './style.css';

class AlertMessage extends React.Component {
  render() {
    const { children } = this.props;
    return (
      <div className='alert-window'>
        <img src={AlertSVG} alt='alert icon' className='alert-icon' />
        <p>{children}</p>
      </div>
    )
  }
}

export default AlertMessage;