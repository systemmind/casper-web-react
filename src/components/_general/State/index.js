import React from 'react';
import PropTypes from 'prop-types';

export default class State extends React.Component {
  static propTypes = {
    currentState: PropTypes.string,
    state: PropTypes.shape({
      show: PropTypes.bool.isRequired,
      label: PropTypes.string
    })
  }

  render(){
    const { state, currentState } = this.props;
    let label = null;
    let show = false;
    if (state){
      label = state.label;
      show  = state.show;
    }

    return currentState && show && (
      <div className='question-currentactivity-block'>{this.formatLabel(label)}</div>
    ) || (<div></div>);
  }

  formatLabel(label){
    const { currentState } = this.props;
    return label.replace("${currentState}", currentState);
  }
}
