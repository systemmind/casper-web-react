import React from 'react';
import { addRouting } from '../../decorators';
import Firebase from '../_Firebase';
import { PolicyModal } from '../_general/Modal';
import greetingSVG from '../../assets/img/greeting.png';
import celebratorySVG from '../../assets/img/celebratory.png';
import './style.css';
import Checkbox from '../_general/Checkbox';
import Divider from '../_general/Divider';
import Loader from '../_general/Loader';
import AlertMessage from '../_general/Alert';
import YouTube from '../_general/YouTube';

class Root extends React.Component {
  state = {
    status: 'waiting', // 'waiting', 'succeeded', 'failed', 'already_registered', 'notification_is_blocked'
    isOpen: false,
    isClose: false,
    loading: false,
    isClicked: false,
    isTermsAccepted: false,
    isAllowed: false
  }

  allowNotification = () => this.setState({ isAllowed: true });
  handleStatus = (status) => this.setState({ status, loading: false });
  firebase = new Firebase(this.handleStatus, this.allowNotification);

  render = () => {
    const { status } = this.state;
    return (status === 'succeeded') ? this.renderSuccessPage() : this.renderMainPage();
  };

  renderMainPage = () => {
    const { callSubscribe, handleClose } = this;
    const { isOpen, isClose, loading, isClicked, isAllowed, isTermsAccepted, status } = this.state;

    return (
      <div className='subscribe-page'>
        {(status === 'failed') && isAllowed && <AlertMessage>Sorry, but we are having trouble getting your request. Please try again later or contact us</AlertMessage>}
        {(status === 'already_registered') && <AlertMessage>You are already registered and you don't need to do it again</AlertMessage>}
        {isClicked && isClose && !isTermsAccepted && <AlertMessage>We need you to accept Data Privacy Policy in order to get surveys. You can accept them <b style={{cursor: 'pointer'}} onClick={this.handleOpen}>here</b></AlertMessage>}
        {isTermsAccepted && (status === 'notification_is_blocked') && <AlertMessage>Please enable browser notifications in order to get surveys. Enable them on the left from URL panel</AlertMessage>}
        <PolicyModal handleClose={handleClose} acceptFunc={callSubscribe} isOpen={isOpen} />
        {loading && <Loader />}
        <div className='subscribe-img-block'>
          <img className='casper-img-greeting' src={greetingSVG} />
        </div>
        <div className='subscribe-text-block'>
          <h1 id='subscribe-title'>Get surveys in your browser.</h1>
          <h1>No installation or personal data required</h1>
          <h2>Follow this 3-step guide to get started</h2>
          <div className='subscribe-checkboxes'>
            <h3>1. Click Subscribe button to start the process</h3>
            <button className='casper-btn subscr-btn' onClick={this.handleOpen}>
              Subscribe
            </button>
            <h3>2. Read and accept <a onClick={this.handleOpen}>Data Privacy Policy</a> on popup</h3>
            <h3>3. Click Allow button in notification popup</h3>
            {/* <Divider />
            <div className='subscribe-tutor'>
              <p className='subscribe-tutor-p'>Still having trouble?</p>
              <p className='subscribe-tutor-p'>Watch our tutorial video</p>
            </div>
            <YouTube link='' /> */} {/* 'MGYj6jS55DE' */}
          </div>
        </div>
      </div>
    )  
  }

  renderSuccessPage = () => {
    return (
      <div className='subscribe-success-page'>
        <img className='casper-img-celebratory' src={celebratorySVG} />
        <div>
          <h1>Thank you for subscribing!</h1>
          <h1>Your demographic surveys will be sent to your browser</h1>
        </div>
      </div>
    )
  }

  handleOpen = (e) => this.setState({ isOpen: true, isClicked: true });
  handleClose = (e) => this.setState({ isOpen: false, isClose: true });
  callSubscribe = (e) => {
    this.firebase.subscribe(this.props.id);
    this.setState({ isOpen: false, isTermsAccepted: true, loading: true });
  }
}

const route = [ Root, '/subscribe/:id', false, false ];
const error = [ '/404', '/subscribe', true, true ];

export default addRouting([ route, error ]);