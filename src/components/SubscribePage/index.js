import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import '../../index.css';

if ("serviceWorker" in navigator){
  let workerFile = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/firebase-messaging-sw.js`;
  navigator.serviceWorker
    .register(workerFile)
    .then((registration) => console.log("Registration successful, scope is: ", registration.scope))
    .catch((err) => console.log("Service worker registration failed, error: ", err));
}

try {
  ReactDOM.render(
    <Root />,
    document.getElementById('root')
  );
} catch(err) {
  alert(`${err}`);
}