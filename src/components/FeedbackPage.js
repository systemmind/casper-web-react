import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import '../index.css';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { relative } from 'path';
const queryString = require('query-string');

class Feedback extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      fact: null,
      tip: null,
      picture: null
    }
    const host = window.location.hostname;
    this._token = queryString.parse(props.location.search).token;
    this._feedback = queryString.parse(props.location.search).id;
    this._url = `https://${host}:5000/api`
  }

  async componentDidMount(){
    try{
      if(this._feedback){
        const response = await fetch(`${this._url}/feedback/${this._feedback}`, {
          method: 'GET',
          headers: {"token-Authorization": this._token},
        });
        if (!response.ok){
          throw new Error(response.statusText);
        }

        const json = await response.json();
        let picture = null;
        if(json.picture){
          picture = `data:image/png;${json.picture.encoding},${json.picture.data}`
        }

        this.setState({
          fact: json.fact,
          tip: json.tip,
          picture: picture
        });
      }
    }
    catch(err){
      console.error(err);
      alert(err);
    }
  }

  render(){
    let tmpFact = null;
    let tmpTip = null;
    if(this.state.fact != null  && this.state.tip != null){
      tmpFact = this.state.fact.match( /[^\.!\?]+[\.!\?]+/g );
      tmpTip = this.state.tip.match( /[^\.!\?]+[\.!\?]+/g );
    }
    return (
      <div style={{width: '100%', position: 'relative'}}>
        <center>
          <div style={{
              minWidth: '600px',
              maxWidth: '1248px',
              minHeight: '600px',
              fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif',
              fontSize: '18pt',
              borderWidth: 1,
              left: '0px',
              position: 'relative',
              marginTop: '5%'
            }}>
            <div style={{float: 'left'}}>
              {
              this.state.picture && (
                <img src={this.state.picture} style={{height: '600px'}}/>
                )
              }
            </div>
            <div style={{
                position: 'absolute',
                float: 'right',
                minWidth: '168px',
                maxWidth: '600px',
                right: '5%'
              }}>
              <div style={{
                  fontWeight: 'bold',
                  borderBottom: '2px solid black',
                  backgroundColor: 'white'
                }}>
                  FACT: {tmpFact ? tmpFact.shift() : '' }
              </div>
              <div style={{
                  backgroundColor: '#f7d4ae',
                  borderBottomLeftRadius: 20,
                  borderBottomRightRadius: 20,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10
                }}>
                <span style={{lineHeight: '40px'}}>
                  {
                    tmpFact ? tmpFact.join(" ") : ''
                  }
                </span>
              </div>
              <div style={{
                  fontWeight: 'bold',
                  paddingTop: '50px',
                  borderBottom: '2px solid black',
                  backgroundColor: 'white'
                }}>
                  TIP: {tmpTip ? tmpTip.shift() : ''}
              </div>
              <div style={{
                  backgroundColor: '#d6f3fe',
                  borderBottomLeftRadius: 20,
                  borderBottomRightRadius: 20,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10
                }}>
                <span style={{lineHeight: '40px'}}>
                  {
                    tmpTip ? tmpTip.join(" ") : ''
                  }
                </span>
              </div>
            </div>
          </div>
        </center>
      </div>
    );
  }
}

const AppRouter = () => (
  <Router>
      <Switch>
        <Route exact path='/feedback' component={Feedback}/>
      </Switch>
  </Router>
);

ReactDOM.render(
  <div>
  <AppRouter />
  </div>, document.getElementById('root'));
