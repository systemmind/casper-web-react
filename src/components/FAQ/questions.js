export default [
  { 
    question: 'What is CASPER?',
    answer: 'CASPER is a software that has been built to measure and evaluate employee wellbeing better and faster than surveys. Rather than asking how you felt 3 months ago, we ask you how are you feeling right now and coming up with a solution how to improve your wellbeing at work'
  },
  { 
    question: 'What do I have to do as a user?',
    answer: 'Your job is easy! 3 times a day you will get a desktop notification that directs you to a short and simple questionnaire that takes no more than 2 minutes to fill out. The questions are non-invasive and simply ask about your emotions and what you are currently doing at work'
  },
  { 
    question: 'How long will I have to use it?',
    answer: 'You will only use CASPER for 10 working days'
  },
  { 
    question: 'How long the setup will take?',
    answer: 'Up to 3 minutes. We\'ll also provide you with video tutorial and chat support if you need help'
  },
  { 
    question: 'What are the benefits for me?',
    answer: 'By inputting information about your day, in return we will deliver you with a daily feedback report that reflects your thoughts, feelings and emotions. By reflecting on your experiences, you can gain a better understanding of what emotions you commonly experience, why you feel that way, and how to overcome negative emotions'
  },
  { 
    question: 'What personal data do you collect?',
    answer: 'A short demographic survey is collected during the installation. We ask no more than age, gender and how long you have worked at your company. We cannot identify individuals through this. This is solely to improve the quality of our analysis and metrics. We are fully GDPR compliant and will not sell on or use personal data out-with this analysis'
  },
  { 
    question: 'Are my responces anonymous?',
    answer: 'Yes. We understand that you could be responding to some questions with sensitive information about how you feel in the workplace. We collect no data regarding people\'s names, removing our ability to single you out to your boss. Our system is "talking generation" which means neither us nor your employer can single you out, only the system identifies your demographics'
  },
  { 
    question: 'What if I miss a survey?',
    answer: 'This won\'t cause too much of an issue. Even though, each survey completion helps us use data more effectively and further improve your workplace wellbeing'
  }
]