export default [
  {
    answer: '<b>Important note:</b> currently CASPER works the best on Google Chrome browser. Try using this browser on your PC or mobile devices while completing your surveys.'
  },
  {
    question: 'I\'ve encountered an error. How can I report it?',
    answer: 'You can message our tech support via chat button in bottom right corner of the screen. If you\'d like to send us a screenshot with an issue, send them here: casperproject2019@gmail.com. Thank you for helping us make CASPER better!'
  },
  {
    question: 'I\'ve signed up for surveys but nothing happens.',
    answer: 'After you signed up, your demographic survey should appear in your CASPER desktop app or browser notifications. You can view browser notifications in notifications center on Windows or system tray on Mac OS. If you\'re using mobile device for completing surveys, check your push notifications. If demographic survey didn\'t appear instantly after signing up, we\'ll send it later while you\'re completing your surveys.'
  },
  {
    question: 'Notifications don\'t seem to work. What should I do?',
    answer: 'At first, check whether the notifications are enabled on your computer or mobile device. If notifications still don\'t pop up, try installing our desktop device or signing up with your mobile device. If these two options are unavailable to you, contact our tech support.'
  },
  {
    question: 'The survey doesn\'t load when I open it in my browser',
    answer: 'Try refreshing the page and clearing browser cache. If this doesn\'t help, reopen the survey later. In case nothing of the above helps, contact our tech support.'
  },
  {
    question: 'Notifications don\'t appear on my mobile device',
    answer: 'Apple devices do not currently support CASPER notifications. If you\'re using any other device, try choosing Google Chrome as a default browser.'
  },
  {
    question: 'I am no longer receiving new surveys, what\'s wrong?',
    answer: 'Our measurement usually last for 2 weeks. If you\'ve been completing surveys 2 weeks and then encountered this error, this means that your measurement is over. Also keep in mind that surveys are sent to the device you\'ve used to sign up. If you\'re still using the same device and you\'re not receiving new surveys, that means we\'re having issues with our server and trying to fix them as soon as possible.'
  },
  {
    question: 'I didn\'t receive daily feedback after evening survey',
    answer: 'You\'ll need to complete your evening survey first in order to get a feedback. Also keep your browser open after completing the evening survey. If none of that helped, message us about the problem.'
  },
  {
    question: 'I\'ve clicked the notification but nothing happened',
    answer: 'Check whether the notifications are allowed on your browser or reinstall the browser if possible. If nothing helps, contact our tech support.'
  },
  {
    question: 'I can\'t download CASPER app',
    answer: 'Check if your browser allows for the app to download. To do that, look for download panel at the bottom of the browser. If the application download is blocked, press Download or Allow button. If that doesn\'t seem to be the problem, check your internet connection. In case both solutions didn\'t work, contact us.'
  },
  {
    question: 'I\'m getting a message that the CASPER app is unsafe to install',
    answer: 'That\'s okay. CASPER app is completely safe to use. All applications which are downloaded from internet are marked as unsafe. If you see the message, click Allow button to continue installing CASPER app.'
  }
]