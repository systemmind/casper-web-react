import React from 'react';
import { addRouting } from '../../decorators';
import FaqPNG from '../../assets/img/faq.png';
import questions from './questions';
import techQuestions from './techQuestions';

function createMarkup(text) {
  return { __html: text };
}

const TextBlock = ({ title, text }) => {
  return (
    <div className='faq-page-content-block'>
      {title && <span className='checkmark' />}
      {title && (
        <div className='faq-page-text-block'>
          {title && <h2>{title}</h2>}
          {text && <p>{text}</p>}
        </div>
      )}
      {!title && (
        <div className='faq-page-text-block'>
          {text && <span dangerouslySetInnerHTML={createMarkup(text)} />}
        </div>
      )}
    </div>
  )
}

class FaqPage extends React.Component {
  render() {
    const questionsBlock = questions.map(question => <TextBlock title={question.question} text={question.answer} key={question.question} />);
    const techQuestionsBlock = techQuestions.map(question => <TextBlock title={question.question} text={question.answer} key={question.question} />);

    return (
      <div id='faq-page'>
        <header>
          <img className='faq-page-img' src={FaqPNG} alt='Preview Icon' />
          <div className='faq-page-h1'>
            <h1>Got questions about CASPER? </h1>
            <h1>We’re here to help</h1>
          </div>
        </header>
        <div className='questions-block'>
          <h1>About Casper</h1>
          {questionsBlock}
        </div>
        <div className='questions-block'>
          <h1>Technical issues</h1>
          {techQuestionsBlock}
        </div>
      </div>
    );
  }
}

const route = [ FaqPage, '/faq', true, false ];

export default addRouting([ route ]);