import React from 'react';
import ReactDOM from 'react-dom';
import '../../index.css';
import './style.css';
import Root from './Root.js';

ReactDOM.render(
  <div>
    <Root />
  </div>, document.getElementById('root')
);