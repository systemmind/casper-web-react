import React from 'react';
import _404SVG from '../../assets/svg/404.svg';

export default class NotFoundPage extends React.Component {
  render() {
    return (
      <div id='_404-page'>
        <img src={_404SVG} alt='Not Found Image' />
        <div>
          <h2>Page not found (404)</h2>
          <p>There are 2 explanations to this problem:</p>
          <p>1. The page address is incorrect</p>
          <p>2. The page has been deleted</p>
          <p>Try checking the page address or going back to homepage</p>
        </div>
      </div>
    );
  }
}