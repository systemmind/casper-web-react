import firebase from 'firebase/app';
import 'firebase/messaging';
import config from './config';

class Firebase {
  constructor(handleStatus, allowNotification){
    this.__app = firebase.initializeApp(config);
    this.__messaging = this.__app.messaging();
    this.__storageKey = 'sentFirebaseMessagingCasperToken';

    this.handleStatus = (typeof handleStatus === 'function') ? handleStatus : (status) => console.log(status);
    this.allowNotification = (typeof allowNotification === 'function') ? allowNotification : () => null;

    // handle catch the notification on current page
    this.__messaging.onMessage(function(payload) {
        console.log('Message received', payload);
        navigator.serviceWorker.getRegistration('/firebase-cloud-messaging-push-scope')
          .then((registration) => {
            
            payload.notification.data = payload.notification;
            registration.showNotification(payload.notification.title, payload.notification);
          });
    });
  }

  async subscribe(measurement){
    const { handleStatus } = this;
    try {
      this.allowNotification();
      await this.__messaging.requestPermission();
      const token = await this.__messaging.getToken();
      console.log('Messaging token: ', token);
      let obj = await this.__sendTokenToServer(token, measurement);
      if (obj){
        obj = await this.__authorizeUser(measurement, obj.id, obj.token);
      }

      navigator.serviceWorker.addEventListener("message", (msg) => console.log('message: ', msg));
      //handleStatus('succeeded');
    }
    catch(err){
      const error = JSON.parse(JSON.stringify(err)).code;
      if (error === 'messaging/permission-blocked') {
        handleStatus('notification_is_blocked');
      } else {
        handleStatus('failed');
      }

      console.error("Unable to get permission to notify: ", err)

      console.log(1, error)
    }
  }

  async __sendTokenToServer(token, measurement) {
    try {
      const { handleStatus } = this;
      if (!this.__isTokenSentToServer(token, measurement)) {
        const url = `${process.env.casperApi}/measurements/${measurement}/users`;
        let response = await fetch(url, {
          method: 'POST',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ devices: [token] })
        })

        let json = await response.json();

        this.__setTokenSentToServer(token, measurement);
        handleStatus('succeeded');
        return json;
      } else {
        handleStatus('already_registered');
      }
    } catch(err) {
      if (err) this.handleStatus('failed');
    }
  }

  __isTokenSentToServer(token, measurement) {
    try {  
      const item = window.sessionStorage.getItem(this.__storageKey);
      const obj = {
        token: token,
        measurement: measurement
      }

      return item === JSON.stringify(obj);
    } catch(err) {
      return this.handleStatus('failed');
      console.log('Error in __isTokenSentToServer', err);
    }
  }

  __setTokenSentToServer(token, measurement) {
    try {  
      const obj =
        token && measurement
        ?
          {
            token: token,
            measurement: measurement
          }
        :
          {
            token: "",
            measurement: ""
          };

      window.sessionStorage.setItem(this.__storageKey, JSON.stringify(obj));
    } catch(err) {
      this.handleStatus('failed');
      console.log('Error in __setTokenSentToServer', err);   
    }
  }

  async __authorizeUser(measurement, user, token) {
    try {
      const url = `${process.env.casperApi}/measurements/${measurement}/users/${user}`;
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json",
          "token-Authorization": token
        },
        body: JSON.stringify({authorized: true})
      });

      const json = await response.json();
      return json;
    } catch(err) {
      this.handleStatus('failed');
      console.log('Error in __authorizeUser', err);
    }
  }
}

export default Firebase;