class CasperError extends Error {
  constructor(msg, code, obj){
    super(msg);
    this.code = code;
    this.error = obj;
  }
}

const Fetch = async (url, body, errorCount = (process.env.errorCount || 10)) => {
  const response = await fetch(url, body);
  if (!response.ok){
    try{
      const json = await response.json();
      throw new CasperError(response.statusText, response.status, json);
    }
    catch(error){
      const { statusText } = response;
      if (statusText === "")
      {
        if (+errorCount){
          return await Fetch(url, body, --errorCount);
        }
        else{
          throw new Error(statusText);
        }
      }
      else
      {
        throw error;
      }
    }
  }

  return await response.json();
}

const Post = async (url, token, body) => {
  return await Fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      "token-Authorization": token,
      "Content-Type": "application/json"
    }
  });
}

const Delete = async (url, token, body) => {
  return await Fetch(url, {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers: {
      "token-Authorization": token,
      "Content-Type": "application/json"
    }
  });
}

const Put = async (url, token, body) => {
  return await Fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      "token-Authorization": token,
      "Content-Type": "application/json"
    }
  });
}

const Get = async (url, token) => {
  return await Fetch(url, {
    method: 'GET',
    headers: {
      "token-Authorization": token
    }
  });
}

export { Fetch, Post, Get, Delete, Put, CasperError };
