import React from 'react';
import NotFoundPage from '../components/404/Root';
import Survey from '../components/Survey';
import { withRouter } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

export default (Header, Intro) => class RouterComponent extends React.Component {
  static propTypes = {
    Header: PropTypes.object.isRequired,
    Intro: PropTypes.object.isRequired,
  }

  getQuery = (param, location) => {
    const urlParams = new URLSearchParams(location.search);

    return urlParams.get(param);
  };

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/survey" render={this.renderSurvey} />;
          <Route path="/404" component={NotFoundPage} />
        </Switch>
      </Router>
    )
  }

  renderSurvey = ({ location }) => {
    const WithRoute = withRouter(Survey);
    const survey = this.getQuery('survey', location);
    const token = this.getQuery('token', location);

    return (
      <WithRoute 
        {...this.props} 
        Header={Header} 
        Intro={Intro} 
        survey={survey} 
        token={token}
        location={location}
      />
    );
  }
}