import React from 'react';
import NotFoundPage from '../components/404/Root';
import { withRouter } from 'react-router';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

export default (components) => class RouterComponent extends React.Component {
    /**
      * @param { ...[ ...routes ] } components 
      * @desc components is an array of routes arrays
      * @desc routes array can be array of the route or redirect components
    */

    render() {
      const routes = components.map(( route ) => {
        const isRedirect = route[3] || false;

        if (isRedirect === false) { 
          const [ OriginalComponent, path, isExact = true ] = route; 
          return <Route key={path} exact={isExact} path={path} render={this.renderComponent(OriginalComponent, isExact)} />;
        } else { 
          const [ to, path, isExact = true ] = route; 
          return <Redirect key={path} exact={isExact} path={path} to={to} />;
        }
      })

      return (
        <Router>
          <Switch>
            {routes}
            <Route path="/404" component={NotFoundPage} />
          </Switch>
        </Router>
      )
    }

    renderComponent = (OriginalComponent, isExact) => ({ match }) => {
      const { id } = match.params;
      const WithRoute = withRouter(OriginalComponent);
      return (isExact) ? <WithRoute {...this.props} /> : <WithRoute {...this.props} id={id} />
    }
}