const webpack = require('webpack');
const path = require('path');
const HWP = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    admin: './src/components/AdminPage/index.js',
    setup: './src/components/SetupPage/index.js',
    mobilesetup: './src/components/MobileSetupPage/index.js',
    subscribe: './src/components/SubscribePage/index.js',
    survey: './src/components/Survey/index.js',
    feedback: './src/components/FeedbackPage.js',
    404: './src/components/404/index.js',
    faq: './src/components/FAQ/index.js',
    test: './src/components/Test/index.js'
  },
  module: {
    rules: [
      {
          loader: 'babel-loader',
          test: /\.js$/,
          exclude: /node_modules/
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif|ico)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new Dotenv(),
    new webpack.HotModuleReplacementPlugin(),
    new HWP(
      {
        title: 'Survey',
        chunks: ['survey'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'survey/index.html'
      }
    ),
    new HWP(
      {
        title: 'Setup Page',
        chunks: ['setup'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'setup/index.html'
      }
    ),
    new HWP(
      {
        title: 'Mobile Setup Page',
        chunks: ['mobilesetup'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'mobilesetup/index.html'
      }
    ),
    new HWP(
      {
        title: 'Subscribe Page',
        chunks: ['subscribe'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'subscribe/index.html'
      }
    ),
    new HWP(
      {
        title: 'Admin Page',
        chunks: ['admin'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'admin/index.html'
      }
    ),
    new HWP(
      {
        title: 'Feedback Page',
        chunks: ['feedback'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'feedback/index.html'
      }
    ),
    new HWP(
      {
        title: '404',
        chunks: ['404'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: '404/index.html'
      }
    ),
    new HWP(
      {
        title: 'FAQ Page',
        chunks: ['faq'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'faq/index.html'
      }
    ),
    new HWP(
      {
        title: 'Tested Component Page',
        chunks: ['test'],
        template: path.join(__dirname, '/public/index.html'),
        inject: 'body',
        filename: 'test/index.html'
      }
    ),
    new CopyWebpackPlugin(
      [
        {
          from: path.join(__dirname, 'public', 'firebase-messaging-sw.js')
        },
        {
          from: path.join(__dirname, 'public', 'locales'), to: "locales"
        },
        {
          from: path.join(__dirname, 'src', 'assets', 'favicon.ico')
        }
      ]
    )
  ],
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname + '/dist/'),
    filename: '[name]/bundle.js',
    chunkFilename: '[name]/bundle.js',
    publicPath: '/'
  },
  devtool: "#eval-source-map",
  devServer: {
    contentBase: path.resolve(__dirname + '/dist/'),
    hot: true,
    https: true,
    historyApiFallback: {
      rewrites: [
        { from: /^\/subscribe\/\d*$/, to: '/subscribe/index.html' },
        { from: /^\/setup\/\d*$/, to: '/setup/index.html' },
        { from: /^\/mobilesetup\/\d*$/, to: '/mobilesetup/index.html' },
        { from: /^\/admin\/\d*$/, to: '/admin/index.html' },
        { from: /^\/admin\/measurement\/\d\/users/, to: '/admin/index.html' },
        { from: /favicon.ico$/, to: '/favicon.ico' }
      ]
    }
  }
};
