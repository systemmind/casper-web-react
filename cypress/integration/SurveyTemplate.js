const Range = {
  "questions": [
    {
      "type": "range",
      "tooltip": "some tooltip",
      "title": "some question title",
      "name": "productivity",
      "controlType": "numberinput",
      "props": {
        "min": 1,
        "max": 9,
        "initialValue": 5,
        "title": "some little title",
        "titleMin": "title",
        "titleMax": "title"
      }
    }
  ]
};

const RadioPreset = {
  "questions": [
    {
      "type": "radio",
      "tooltip": "some tooltip",
      "title": "What are you doing?",
      "name": "activity",
      "controlType": "textinput",
      "inputOption": "Other",
      "props": {
        "options": ["Reading", "Emailing", "Watching TV", "Eating", "Playing", "Relaxing", "Have coffe", "Meeting", "Crying", "Running"]
      }
    }
  ]
};

const RadioDepends = {
  questions: [
    {
      "type": "radio",
      "tooltip": "some tooltip",
      "title": "Choose your emotion feedback",
      "name": "activity",
      "controlType": "textinput",
      props: {
        options: {
          query: {
            filters: [
              { "name": "value", "op": "=", "value": "emotions" },
              { "name": "timestamp", "op": ">=", "value": "${curdate}"}
            ],
            op: "and",
            controlType: "textinput"
          },
          field: "value"
        }
      }
    }
  ]
};

const Bubbles = {
  questions: [
    {
      type: "bubbles",
      tooltip: "some tooltip",
      title: "Which of the activities did you most enjoy?",
      name: "goodactivities",
      controlType: "textinput",
      needRecord: false,
      props: {
        color: "#FFCD00", // '#A379BD'
        options: {
          preset: ["preset option 1", "preset option 2"],
          queries: [
            {
              filters: [
                { name: "name", op: "=", value: "activities" },
                { name: "timestamp", op: ">=", value: "${curdate}"}
              ],
              op: "and",
              controlType: "textinput"
            },
            {
              filters: [
                { name: "name", op: "=", value: "goodactivities" },
                { name: "timestamp", op: ">=", value: "${curdate}"}
              ],
              op: "and",
              controlType: "textinput"
            }
          ]
        }
      }
    }
  ]
};

const Text = {
  questions: [
    {
      type: "text",
      tooltip: "some tooltip",
      name: "reasonfears",
      title: "some question title",
      controlType: "textinput",
      props: {
        placeholder: "some placeholder"
      }
    }
  ]
};

const Textarea = {
  questions: [
    {
      type: "text",
      tooltip: "some tooltip",
      name: "reasonfears",
      title: "some question title",
      controlType: "textinput",
      expand: true,
      props: {
        placeholder: "some placeholder"
      }
    }
  ]
};

const Container = {
  questions: [
    {
      type: "container",
      tooltip: "some tooltip",
      name: "should_have_want_todo",
      title: "some question title",
      needRecord: false,
      props: {
        questions: [
          {
            type: "range",
            needRecord: true,
            name: "should_todo",
            controlType: "numberinput",
            props: {
              min: 1,
              max: 9,
              initialValue: 5,
              title: "should to do",
              titleMin: "title",
              titleMax: "title"
            }
          }
        ]
      }
    }
  ]
};

export {
  RadioPreset,
  RadioDepends,
  Bubbles,
  Text,
  Textarea,
  Range,
  Container
};
