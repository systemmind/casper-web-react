describe('test morning survey', () => {
  const survey = 420
  const token = 'deadbeaf'
  const questions = [
                      'age',
                      'gender',
                      'experience',
                      'activity',
                      'duration',
                      'feeling',
                      'reason',
                      'agree',
                      'todo',
                      'happiness',
                      'productivity',
                      'rubbishbin',
                      'emotion'
                    ]
  const query = '&question=age&question=gender&question=experience&question=activity&question=duration&question=feeling&question=reason&question=agree&question=todo&question=happiness&question=productivity&question=rubbishbin&question=emotion'

  const activity = 'write tests';

  beforeEach(()=>{
    cy.server()
    for (let q of questions)
    {
      cy.route(
        'GET',
        `https://localhost:5000/api/survey/${survey}/question/${q}/answered`,
        {answered: false}
      ).as(`get${q}answered`)

      cy.route(
        'PUT',
        `https://localhost:5000/api/survey/${survey}/question/${q}/shown`,
         {}
      ).as(`put${q}shown`)
    }
  })

  // TODO: move this to the before() function
  it('check survey/index.js', () => {
    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/expired`,
      {expired: false}
    ).as('getSurveyExpired')

    cy.visit(
      `survey?survey=${survey}${query}&token=${token}`
    );

    cy.wait('@getSurveyExpired');

    for (let q of questions)
    {
      if(q !== 'emotion'){
        cy.wait(`@get${q}answered`);
      }
    }
  });

  it('check age question', () => {
    const age = '25-35';
    cy.wait('@putageshown');
    cy.get(`input[value="${age}"]`).click();
  });

  it('check age put, gender load', () => {
    const age = '25-35';

    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/age`,
      {age: `${age}`}
    ).as('putAge')

    cy.get('button[name=submit]').click();

    cy.wait('@putAge');
    cy.wait('@putgendershown');
  });

  it('check gender question', () => {
    const gender = 'Male';
    cy.get(`input[value="${gender}"]`).click();
  });

  it('check gender put, experience load', () => {
    const gender = 'Male';

    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/gender`,
      {"gender": `${gender}`}
    ).as('putGender')

    cy.get('button[name=submit]').click();

    cy.wait('@putGender').then((xhr) => (
      expect({gender: "Male"}).to.deep.equal(xhr.request.body)
    ))
    
    cy.wait('@putexperienceshown');
  });

  it('check experience question', () => {
    const experience = "1 - 3 years";
    cy.get(`input[value="${experience}"]`).click();
  });

  it('check experience put, activity load', () => {
    const experience = "1 - 3 years";
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/experience`,
      {"experience": `${experience}`}
    ).as('putExperience')

    cy.get('button[name=submit]').click();

    cy.wait('@putExperience').then((xhr) => (
      expect({experience: experience}).to.deep.equal(xhr.request.body)
    ))

    cy.wait('@putactivityshown');
  });

  it('check activity question', () => {
    const value = 'Research';
    cy.get(`input[value="${value}"]`).click();
  });

  it('check activity put, duration load', () => {
    const value = 'Research';
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/activity`,
      {activity: `${value}`}
    ).as('putValue')

    cy.get('button[name=submit]').click();

    cy.wait('@putValue');
    cy.wait('@getdurationanswered')
    cy.wait('@putdurationshown');
  });

  it('check duration question', () => {
    const value = '0-5m';
    cy.get(`input[value="${value}"]`).click();
  });

  it('check duration put, feeling load', () => {
    const value = '0-5m';
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/duration`,
      {duration: `${value}`}
    ).as('putValue')

    cy.get('button[name=submit]').click();

    cy.wait('@putValue');
    cy.wait('@putfeelingshown');
  });

  it('check feeling question', () => {
    const emotion_0 = "bored";
    const emotion_1 = "meaningful";
    const feeling = {}
    feeling[emotion_0] = 3;
    feeling[emotion_1] = 6;

    cy.get('button[name=emotionButton_0]').click();
    cy.get('button[name=sad_0]').click();
    cy.get('button[name=addButton]').click();

    cy.get('button[name=emotionButton_1]').click();
    cy.get('button[name=meaningful_1]').click();

    cy.get('input[name=input_0]').as('range').invoke('val', 3).trigger('input');
    cy.get('input[name=input_1]').as('range').invoke('val', 6).trigger('input');
  });

  it('check feeling put, reason load', () => {
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/feeling`,
      {}
    ).as('putValue')

    cy.get('button[name=submit]').click();
    cy.wait('@putValue').then((data)=>{
      expect(data.request.body).to.include({sad: '3', meaningful: '6'});
    });
    cy.wait('@putreasonshown');
  });

  it('check reason question', () => {
    const value = 'bla-bla';
    cy.get(`input[name=reason]`).type("value");
  });
  
  it('check reason put, agree load', () => {
    const value = 'bla-bla';
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/reason`,
      {reason: `${value}`}
    ).as('putValue')

    cy.get('button[name=submit]').click();

    cy.wait('@putValue');
    cy.wait('@putagreeshown');
  });

  it('check agree question', () => {
    const meaning = 'meaning';
    const useful = 'useful';
    const achievement = 'achievement';

    cy.get(`input[id=${meaning}]`).as('range').invoke('val', 2).trigger('input');
    cy.get(`input[id=${useful}]`).as('range').invoke('val', 3).trigger('input');
    cy.get(`input[id=${achievement}]`).as('range').invoke('val', 4).trigger('input');
  });

  it('check agree put, todo load', () => {
    const meaning = 'meaning';
    const useful = 'useful';
    const achievement = 'achievement';

    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/agree`,
      {agree: `${meaning}`}
    ).as('putValue');

    cy.get('button[name=submit]').click();

    cy.wait('@putValue');
    cy.wait('@puttodoshown');
  });

  it('check todo question', () => {
    const value = 'have';
    cy.get(`input[id="${value}"]`).as('range').invoke('val', 5).trigger('input');
  });
  
  it('check todo put, happiness load', () => {
    const value = 'have';
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/todo`,
      {todo: `${value}`}
    ).as('putValue')

    cy.get('button[name=submit]').click();

    cy.wait('@putValue');
    cy.wait('@puthappinessshown');
  });

  it('check happiness question', () => {
    const happiness = '5';
    cy.get('input[id=happiness]').as('range').invoke('val', 5).trigger('input');
  });
  
  it('check happiness put, productivity load', () => {
    const happiness = '5';
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/happiness`,
      {"happiness": `${happiness}`}
    ).as('putHappiness')

    cy.get('button[name=submit]').click();

    cy.wait('@putHappiness').then((xhr) => (
      expect({happiness: happiness}).to.deep.equal(xhr.request.body)
    ))

    cy.wait('@putproductivityshown');
  });

  it('check productivity question', () => {
    const productivity = "5";
    cy.get('input[id=productivity]').as('range').invoke('val', 5).trigger('input');
  });

  it('check productivity put, rubbish bin load', () => {
    const productivity = "5";
    cy.route(
      'PUT',
      `https://localhost:5000/api/survey/${survey}/question/productivity`,
      {"productivity": productivity}
    ).as('putHappiness')

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}`,
      response: [{
          'begin': 'Fri, 20 Sep 2019 02:24:40 GMT',
          'end': 'Fri, 20 Sep 20199 03:24:40 GMT',
          'id': survey,
          'measurement': 1,
          'user': 1
        }],
        headers: {
          "token-Authorization": token
        }
      }).as('getSurveys');

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}/question/activity`,
      response: {
        'id': 1,
        'activity': activity,
        'date': '2019-09-20',
        'time': '1:50:05',
        'shown': false,
        'survey': survey
      },
      headers: {
        "token-Authorization": token
      }
    }).as('getRubbishbinQuestionData');

    cy.get('button[name=submit]').click();

    cy.wait('@putHappiness').then((xhr) => (
      expect({productivity: productivity}).to.deep.equal(xhr.request.body)
    ));

    cy.wait('@putrubbishbinshown');
    cy.wait('@getSurveys')
    cy.wait('@getRubbishbinQuestionData')
  });

  it('check rubbishbin question', () => {
    cy.get('input[name=writetests]').click();
  });

  it('check rubbish bin submit, emotion loading', () => {
    cy.route({
      method: 'PUT',
      url: `https://localhost:5000/api/survey/${survey}/question/rubbishbin`,
      response: [],
      headers: {
        "token-Authorization": token
      }
    }).as('putQuestionData');

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}`,
      response: [{
          'begin': 'Fri, 20 Sep 2019 02:24:40 GMT',
          'end': new Date(),
          'id': survey,
          'measurement': 1,
          'user': 1
        }],
        headers: {
          "token-Authorization": token
        }
      }).as('getDailySurveys');

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}/question/feeling`,
      response: {
        'id': 1,
        'bored': 7,
        'lazy': 7,
        'grateful': 7,
        'productive': 7,
        'sad': 0,
        'stressed': 0,
        'depressed': 0,
        'excited': 0,
        'challenged': 0,
        'frustrated': 0,
        'tired': 0,
        'coerced': 0,
        'bullied': 0,
        'manipulated': 0,
        'anxious': 0,
        'curious': 0,
        'indifferent': 0,
        'apathetic': 0,
        'resentful': 0,
        'important': 0,
        'empathetic': 0,
        'meaningful': 0,
        'responsible': 0,
        'happy': 7,
        'date': '2019-09-20',
        'time': '12:25:42',
        'shown': false,
        'survey': 3
      },
      headers: {
        "token-Authorization": token
      }
    }).as('getQuestionEmotionData');

    cy.get('button[name=submit]').click();

    cy.wait('@putQuestionData').then((xhr) => (
      expect({"write tests": true}).to.deep.equal(xhr.request.body)
    ));

    cy.wait('@getQuestionEmotionData')
  });

  it('check emotion question', () => {
    cy.route({
      method: 'POST',
      url: `https://localhost:5000/api/survey/${survey}/feedback`,
      response: {
        'date': 'Fri, 20 Sep 2019 00:00:00 GMT',
        'emotion': 5,
        'fact': 6,
        'id': 1,
        'picture': 'sad.jpg',
        'response': null,
        'tip': 20,
        'user': 1
      },
      headers: {
        "token-Authorization": token
      }
    }).as('putQuestionData');

    cy.get('input[name=happy]').click();
    cy.get('button[name=submit]').click();
    cy.wait('@putQuestionData');
  });

  it('check final screen', () => {
    const value = 'Thank you, this survey has been completed!';

    cy.get('h1')
      .should('have.text', value);
  });

});
