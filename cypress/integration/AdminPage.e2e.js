describe('The Admin Login Page', function() {

  const tokenAuthorization = "abcd"

  const validMeasurement = {
    "id": 1,
    "start_date": '2019-01-24',
    "stop_date": '2019-11-22',
    "name": "a valid company name",
    "company_url": "some.URL",
    "manager_email": "manager@email",
    "started": 0,
    "welcome_email": "welcome2@email.com"
  }

  const activeMeasurement = {
    "id": 1,
    "start_date": '2019-01-24',
    "stop_date": '2019-11-22',
    "name": "a valid company name",
    "company_url": "some.URL",
    "manager_email": "manager@email",
    "started": 1,
    "welcome_email": "welcome2@email.com"
  }

  const newMeasurement = {
    "start_date": '2119-01-24',
    "stop_date": '2119-11-22',
    "name": "a new name",
    "company_url": "a.new.URL",
    "manager_email": "new@email",
    "welcome_email": "welcome@email.com"
  }

  it('successfully loads', function() {
    cy.visit('/admin')
    cy.url().should('include', '/admin')
    cy.get('h3')
      .should('have.text', 'Please, login as Administrator');
    cy.get('button')
      .should('have.text', 'Login');
  })

  it('invalid login', function() {
    cy.get('button').click()
    cy.contains('Login and password are required')
    cy.contains("×").click()
    cy.url().should('include', '/admin')
    cy.get('input[name=login]')
      .type("user")
      .should('have.value', 'user')
    cy.get('button').click()
    cy.contains('Login and password are required')
    cy.contains("×").click()
    cy.url().should('include', '/admin')

    cy.get('input[name=password]')
      .type("password")
      .should('have.value', 'password')

    cy.server()

    cy.route({
      method: 'POST',
      url: 'https://localhost:5000/api/login',
      response: {ok: false},
      status: 401
    }).as('invalidPassword')

    cy.get('button').click()
    cy.wait('@invalidPassword')
    cy.contains('Invalid credentials')
    cy.contains("×").click()
    cy.url().should('include', '/admin')

    cy.route({
      method: 'POST',
      url: 'https://localhost:5000/api/login',
      response: {ok: false},
      status: 404
    }).as('invalidUser')

    cy.get('button').click()
    cy.wait('@invalidUser')
    cy.contains('Invalid credentials')
    cy.contains("×").click()
    cy.url().should('include', '/admin')
  })

  it('successful login', function() {
    cy.visit('/admin')

    cy.get('input[name=login]')
      .type("user")
      .should('have.value', 'user')

    cy.get('input[name=password]')
      .type("password")
      .should('have.value', 'password')

    cy.server()

    cy.route({
      method: 'POST',
      url: 'https://localhost:5000/api/login',
      response: {token: tokenAuthorization}
    }).as('getLogin')

    cy.route({
      method: 'GET',
      url: 'https://localhost:5000/api/measurements',
      response: [],
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('getMeasurements');

    cy.get('button').click()

    cy.wait('@getLogin').then((xhr) => (
      expect({login: "user", password: "password"}).to.deep.equal(xhr.request.body)
    ))

    cy.wait('@getMeasurements')

    cy.url().should('include', '/measurement')

  })

  it('add measurement', function() {

    const newMeasurement = {
      "start_date": validMeasurement["start_date"],
      "stop_date": validMeasurement["stop_date"],
      "name": validMeasurement["name"],
      "company_url": "",
      "manager_email": "",
      "welcome_email": validMeasurement["welcome_email"]
    }

    const stub = cy.stub()
    cy.on('window:alert', stub)
    cy.server()

    cy.route({
      method: 'GET',
      url: 'https://localhost:5000/api/measurements',
      response: [validMeasurement],
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('getMeasurements');

    cy.route({
      method: 'POST',
      url: 'https://localhost:5000/api/measurements',
      response: newMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization,
        "Content-Type": "application/json"
      }
    }).as('addMeasurement');

    cy.contains('Add').click()
    cy.contains('Create').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith("Both the starting and ending date, and the company's name are required")
    })

    cy.contains('Add').click()
    cy.get('input[name=name]')
      .type(newMeasurement["name"])
      .should('have.value', newMeasurement["name"])
    cy.contains('Create').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith("Both the starting and ending date, and the company's name are required")
    })

    cy.contains('Add').click()
    cy.get('input[name=start_date]')
      .type(newMeasurement["start_date"])
      .should('have.value', newMeasurement["start_date"])
    cy.contains('Create').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith("Both the starting and ending date, and the company's name are required")
    })

    cy.contains('Add').click()
    cy.get('input[name=stop_date]')
      .type(newMeasurement["stop_date"])
      .should('have.value', newMeasurement["stop_date"])
    cy.contains('Create').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith("Both the starting and ending date, and the company's name are required")
    })

    cy.contains('Add').click()
    cy.get('input[name=name]')
      .type(newMeasurement["name"])
    cy.get('input[name=start_date]')
      .type(newMeasurement["start_date"])
    cy.get('input[name=stop_date]')
      .type(newMeasurement["stop_date"])
    cy.contains('Create').click()
    cy.wait('@addMeasurement').then((xhr) => (
      expect(newMeasurement).to.deep.equal(xhr.request.body)
    ))
    cy.wait('@getMeasurements')
    cy.contains(validMeasurement["id"])
    cy.contains(validMeasurement["name"])
    cy.contains(validMeasurement["start_date"])
    cy.contains(validMeasurement["stop_date"])
    cy.contains("inactive")
    cy.get('button').contains("Start")
    cy.get('button').contains("Edit")
    cy.get('button').contains("Delete")
  })

  it('start-stop measurement', function() {
    cy.server()

    cy.route({
      method: 'POST',
      url: `https://localhost:5000/api/measurement/${validMeasurement["id"]}/start`,
      response: activeMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization,
        "Content-Type": "application/json"
      }
    }).as('startMeasurement');

    cy.route({
      method: 'POST',
      url: `https://localhost:5000/api/measurement/${validMeasurement["id"]}/stop`,
      response: validMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization,
        "Content-Type": "application/json"
      }
    }).as('stopMeasurement');

    cy.get('button').contains("Stop").should('not.exist')
    cy.contains("ACTIVE").should('not.exist')
    cy.contains("inactive")

    cy.get('button').contains("Start").click()
    cy.wait('@startMeasurement')

    cy.get('button').contains("Start").should('not.exist')
    cy.contains("ACTIVE")

    cy.get('button').contains("Stop").click()
    cy.wait('@stopMeasurement')

    cy.get('button').contains("Stop").should('not.exist')
    cy.contains("ACTIVE").should('not.exist')
    cy.contains("inactive")
    cy.get('button').contains("Start")
  })


  it('edit measurement', function() {

    const stub = cy.stub()
    cy.on('window:alert', stub)
    cy.server()

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/measurements/${validMeasurement["id"]}`,
      response: validMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('getMeasurement');

    cy.get('button').contains('Edit').click()
    cy.wait('@getMeasurement')

    cy.url().should('include', `measurement/${validMeasurement["id"]}`)
    cy.contains("Id")
    cy.contains(validMeasurement["id"])
    cy.contains("Name")
    cy.contains(validMeasurement["name"])
    cy.contains("Link")
    cy.contains(validMeasurement["company_url"])
    cy.contains("Start")
    cy.contains(validMeasurement["start_date"])
    cy.contains("End")
    cy.contains(validMeasurement["stop_date"])
    cy.contains("Manager Email")
    cy.contains(validMeasurement["manager_email"])
    cy.contains("Employee Emails")
    cy.contains("Destination")
    cy.contains(`Measurement #${validMeasurement["id"]} Settings`)
    cy.get('button').contains("Apply Changes")
    cy.get('button').contains("Send Welcome Message")
    cy.get('button').contains("Send Alarm Message")
    cy.contains("Company's Name")
    cy.get('input[name=name]')
      .should('have.value', validMeasurement["name"])
    cy.contains("Company's Website")
    cy.get('input[name=company_url]')
      .should('have.value', validMeasurement["company_url"])
    cy.contains("Measurement's Starting Date")
    cy.get('input[name=start_date]')
      .should('have.value', validMeasurement["start_date"])
    cy.contains("Measurement's Ending Date")
    cy.get('input[name=end_date]')
      .should('have.value', validMeasurement["stop_date"])
    cy.contains("Company's Manager E-mail")
    cy.get('input[name=manager_email]')
      .should('have.value', validMeasurement["manager_email"])
    cy.contains("Employee Email List")
    cy.contains("Welcome Message Destination:")
    cy.contains("Setup page")

    cy.get('input[name=name]').clear()
      .type(newMeasurement["name"])
      .should('have.value', newMeasurement["name"])
    cy.get('input[name=company_url]').clear()
      .type(newMeasurement["company_url"])
      .should('have.value', newMeasurement["company_url"])
    cy.get('input[name=start_date]')
      .type(newMeasurement["start_date"])
      .should('have.value', newMeasurement["start_date"])
    cy.get('input[name=end_date]')
      .type(newMeasurement["stop_date"])
      .should('have.value', newMeasurement["stop_date"])
    cy.get('input[name=manager_email]').clear()
      .type(newMeasurement["manager_email"])
      .should('have.value', newMeasurement["manager_email"])

    cy.route({
      method: 'PUT',
      url: `https://localhost:5000/api/measurements/${validMeasurement["id"]}`,
      response: newMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization
      },
      status: 401
    }).as('sessionExpired');
    cy.get('button').contains("Apply Changes").click()
    cy.wait('@sessionExpired').then(() => {
      expect(stub.getCall(0)).to.be.calledWith("session expired")
    })

    cy.route({
      method: 'PUT',
      url: `https://localhost:5000/api/measurements/${validMeasurement["id"]}`,
      response: newMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('putMeasurement');
    cy.get('button').contains("Apply Changes").click()
    cy.wait('@putMeasurement').then((xhr) => (
      expect(newMeasurement).to.deep.equal(xhr.request.body)
    ))

    cy.route({
      method: 'GET',
      url: 'https://localhost:5000/api/measurements',
      response: [validMeasurement],
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('getMeasurements');
    cy.contains("Back to the Measurement List").click()
    cy.wait('@getMeasurements')
    cy.url().should('include', '/measurement')

  })

  it('delete measurement', function() {
    cy.server()

    cy.route({
      method: 'GET',
      url: 'https://localhost:5000/api/measurements',
      response: [],
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('getMeasurements');

    cy.route({
      method: 'POST',
      url: `https://localhost:5000/api/measurement/${validMeasurement["id"]}/stop`,
      response: validMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization,
        "Content-Type": "application/json"
      }
    }).as('stopMeasurement');

    cy.route({
      method: 'DELETE',
      url: `https://localhost:5000/api/measurements/${validMeasurement["id"]}`,
      response: validMeasurement,
      headers: {
        "token-Authorization": tokenAuthorization
      }
    }).as('deleteMeasurement');

    cy.get('button').contains('Delete').click()
    cy.wait('@stopMeasurement')
    cy.wait('@deleteMeasurement')
    cy.wait('@getMeasurements')
    cy.get('button').contains("Start").should('not.exist')
    cy.get('button').contains("Edit").should('not.exist')
    cy.get('button').contains("Delete").should('not.exist')
  })

})
