const StateBegin = {
  state: {
    query: {
      filters: [
        { "name": "name", "op": "=", "value": "activity" },
        { "name": "survey", "op": "=", "value": "${survey}"},
        { "name": "employeequestions.ordern", "op": "<", "value": "${questionOrder}"},
      ],
      op: "and",
      controlType: "textinput"
    },
    field: "value",        // the name of field that should be extracted to get current state
    selfName: "activity" // name of the question when state should not be displayed, required to calcuate amount of controls which cnange state
  },
  questions: [
    {
      type: "radio",
      tooltip: "some tooltip",
      title: "What are you doing?",
      name: "activity",
      controlType: "textinput",
      inputOption: "Other",
      state: {
        show: true,
        label: null,
        bar: {
          title: "Your activity (${currentStateNumber}/${statesNumber})"
        }
      },
      props: {
        options: ["Reading", "Emailing"]
      }
    },
    {
      type: "range",
      tooltip: "some tooltip",
      title: "some question title",
      name: "productivity",
      controlType: "numberinput",
      state: {
        show: true,
        label: "Your activity ${currentState}",
        bar: {
          title: "Your activity (${currentStateNumber}/${statesNumber})"
        }
      },
      props: {
        min: 1,
        max: 9,
        initialValue: 5,
        title: "some little title",
        titleMin: "title",
        titleMax: "title"
      }
    },
    {
      type: "radio",
      tooltip: "some tooltip",
      title: "What are you doing?",
      name: "activity",
      controlType: "textinput",
      inputOption: "Other",
      state: {
        show: true,
        label: null,
        bar: {
          title: "Your activity (${currentStateNumber}/${statesNumber})"
        }
      },
      props: {
        options: ["Reading", "Emailing"]
      }
    },
    {
      type: "range",
      tooltip: "some tooltip",
      title: "some question title",
      name: "productivity",
      controlType: "numberinput",
      state: {
        show: true,
        label: "Your activity: ${currentState}",
        bar: {
          title: "Your activity (${currentStateNumber}/${statesNumber})"
        }
      },
      props: {
        min: 1,
        max: 9,
        initialValue: 5,
        title: "some little title",
        titleMin: "title",
        titleMax: "title"
      }
    },
  ]
};

const StateMiddle = {
  state: {
    query: {
      filters: [
        { "name": "name", "op": "=", "value": "activity" },
        { "name": "survey", "op": "=", "value": "${survey}"},
        { "name": "employeequestions.ordern", "op": "<", "value": "${questionOrder}"},
      ],
      op: "and",
      controlType: "textinput"
    },
    field: "name",        // the name of field that should be extracted to get current state
    selfName: "activity", // name of the question when state should not be displayed - remove this paramenter?
    // title: "Your activity:",
    indexField: 'question.order' // the name of field that should be used to extract current index of state
  },
  questions: [
    {
      type: "range",
      tooltip: "some tooltip",
      title: "some question title",
      name: "productivity",
      controlType: "numberinput",
      state: {
        show: true,
        label: "Your activity ${currentState}",
        bar: {
          title: "Your activity ${currentStateNumber}/${statesNumber}"
        }
      },
      props: {
        min: 1,
        max: 9,
        initialValue: 5,
        title: "some little title",
        titleMin: "title",
        titleMax: "title"
      }
    }
  ]
};

const StateEnd = {
  state: {
    query: {
      filters: [
        { "name": "name", "op": "=", "value": "activity" },
        { "name": "survey", "op": "=", "value": "${survey}"},
        { "name": "employeequestions.ordern", "op": "<", "value": "${questionOrder}"},
      ],
      op: "and",
      controlType: "textinput"
    },
    field: "name",        // the name of field that should be extracted to get current state
    selfName: "activity", // name of the question when state should not be displayed - remove this paramenter?
    // title: "Your activity:",
    indexField: 'question.order' // the name of field that should be used to extract current index of state
  },
  questions: [
    {
      type: "bubbles",
      tooltip: "some tooltip",
      title: "Which of the activities whould you push to rubbish bin?",
      name: "badactivities",
      controlType: "textinput",
      needRecord: false,
      state: {
        show: true,
        label: null,
        bar: {
          title: "Your day reflection"
        }
      },
      props: {
        color: '#A379BD',
        options: {
          preset: ["activity 1", "activity 2"],
        }
      }
    }
  ]
};

export {
  StateBegin,
  StateMiddle,
  StateEnd
};
