import { StateBegin, StateMiddle, StateEnd } from './State';

export default {
  StateBegin,
  StateMiddle,
  StateEnd
}
