describe('test evening survey', () => {
  const survey = 1674
  const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8'
  const questions = [
                      'happiness'
                    ]
  const query = '&loader=evening' // &loader=evening

  const activity = 'write tests';

  beforeEach(()=>{
    cy.viewport(360, 640)
    cy.server()
    for (let q of questions)
    {
      cy.route(
        'GET',
        `https://localhost:5000/api/survey/${survey}/question/${q}/answered`,
        {answered: false}
      ).as(`get${q}answered`)

      cy.route(
        'PUT',
        `https://localhost:5000/api/survey/${survey}/question/${q}/shown`,
         { shown: false }
      ).as(`put${q}shown`)
    }
  })

  // TODO: move this to the before() function
  it('check survey/index.js', () => {
    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/questions`,
      questions
    ).as(`getQuestions`)

    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/expired`,
      {expired: false}
    ).as('getSurveyExpired')

    cy.visit(
      `survey?survey=${survey}${query}&token=${token}`
    );

    cy.wait('@getSurveyExpired');
  });

  it('check question', () => {
    const url = `https://localhost:5000/api/survey/${survey}/question/happiness`;
    const key = "happiness";
    const value = '5';
    const method = 'putValue';

    let isMobile = false;
    let isRotate = true;
    let toNext = false;
    cy.goNext = () => toNext = true;
    cy.rotateView = () => {
      try {
        isRotate = !isRotate;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be rotate soon`)
      }  
    }
    cy.changeView = () => {
      try {
        isMobile = !isMobile;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be changed soon`)
      }  
    }
    

    cy.route(
      'PUT',
      url,
      { [key] : value }
    ).as(method);

    cy.wait(800);
    cy.get('button').click({ force: true });

    const checkForResult = (btn) => {
      const classes = btn.attr('class');
      const isActive = classes.search(/passive/) < 0;

      if (toNext && isActive) {
        cy.get('button[name=submit]').click({ force: true });

        cy.wait('@' + method).then((xhr) => (
          expect({ [key] : value }).to.deep.equal(xhr.request.body)
        ));
      } else {
        cy.wait(1000);
        cy.get('button[name=submit]').then(checkForResult);
      } 
    }

    cy.get('button[name=submit]').then(checkForResult);
  });
});
