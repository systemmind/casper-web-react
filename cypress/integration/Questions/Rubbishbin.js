describe('test evening survey', () => {
  const survey = 1674
  const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8'
  const questions = [
                      'rubbishbin'
                    ]
  const query = '&loader=evening' // &loader=evening

  const activity = 'write tests';

  beforeEach(()=>{
    cy.viewport(360, 640)
    cy.server()
    /* for (let q of questions)
    {
      cy.route(
        'PUT',
        `https://localhost:5000/api/survey/${survey}/question/${q}/shown`,
         { shown: false }
      ).as(`put${q}shown`)
    } */
  })

  // TODO: move this to the before() function
  it('check survey/index.js', () => {
    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/expired`,
      {expired: false}
    ).as('getSurveyExpired')

    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/questions`,
      [ 'rubbishbin', 'activity', 'activity', 'activity', 'activity', 'activity' ]
    ).as(`getQuestions`)

    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/question/rubbishbin/answered?indent=${survey}_0`,
      {answered: false}
    ).as(`getrubbishbinanswered`)

    cy.visit(
      `survey?survey=${survey}${query}&token=${token}`
    );

    cy.wait('@getSurveyExpired');
    cy.wait('@getQuestions')
    
    cy.wait(200);
    cy.get('button').click({ force: true });
    cy.wait('@getrubbishbinanswered');
  });

  it('check question', () => {  
    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/questions`,
      [ 'rubbishbin', 'activity', 'activity', 'activity', 'activity', 'activity' ]
    ).as(`getQuestions`)

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}/question/rubishbin`,
      response: [
        {"id": 8, "activity": "brainstorm", "date": "2019-12-27", "time": "21:29:51", "shown": null, "survey": survey, "indent": null},
        {"id": 9, "activity": "painting", "date": "2019-12-27", "time": "21:29:51", "shown": null, "survey": survey, "indent": null},
        {"id": 10, "activity": "writing", "date": "2019-12-27", "time": "21:29:51", "shown": null, "survey": survey, "indent": null}
      ],
      headers: {
        "token-Authorization": token
      }
    }).as('getRubbishbinQuestionRecords');

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}`,
      response: [{
          'begin': 'Fri, 20 Sep 2019 02:24:40 GMT',
          'end': 'Fri, 20 Sep 20199 03:24:40 GMT',
          'id': survey,
          'measurement': 1,
          'user': 1
        }],
        headers: {
          "token-Authorization": token
        }
      }).as('getSurveys');

      const activities = [ activity, "brainstorm", "painting", "writing", "something else"];
      for (let i = 1; i < 6; i++) {
        const indent = survey + '_' + i;

        cy.route({
          method: 'GET',
          url: `https://localhost:5000/api/survey/${survey}/question/activity?indent=${indent}`,
          response: [
            { 
              indent,
              activity: activities[i - 1],
              id: i,
              date: '2019-09-20',
              time: '1:50:05',
              shown: false,
              survey: survey
            }
          ],
          headers: {
            "token-Authorization": token
          }
        }).as('getActivity_' + [i - 1]);
      }

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}/question/activity`,
      response: [
        {
          'id': 1,
          'activity': activity,
          'date': '2019-09-20',
          'time': '1:50:05',
          'shown': false,
          'survey': survey
        },
        {
          'id': 2,
          'activity': "brainstorm",
          'date': '2019-09-20',
          'time': '1:50:05',
          'shown': false,
          'survey': survey
        },
        {
          'id': 3,
          'activity': "painting",
          'date': '2019-09-20',
          'time': '1:50:05',
          'shown': false,
          'survey': survey
        },
        {
          'id': 4,
          'activity': "writing",
          'date': '2019-09-20',
          'time': '1:50:05',
          'shown': false,
          'survey': survey
        },
        {
          'id': 5,
          'activity': "something else",
          'date': '2019-09-20',
          'time': '1:50:05',
          'shown': false,
          'survey': survey
        }
      ],
      headers: {
        "token-Authorization": token
      }
    }).as('getRubbishbinQuestionData');

    const url = `https://localhost:5000/api/survey/${survey}/question/rubbishbin`;
    const key = "rubbishbin";
    const value = '5';
    const method = 'putQuestionData';

    let isMobile = false;
    let isRotate = true;
    let toNext = false;
    cy.goNext = () => toNext = true;
    cy.rotateView = () => {
      try {
        isRotate = !isRotate;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be rotate soon`)
      }  
    }
    cy.changeView = () => {
      try {
        isMobile = !isMobile;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be changed soon`)
      }  
    }

    cy.route(
      'PUT',
      url,
      []
    ).as(method);  

    cy.wait('@getRubbishbinQuestionRecords');
    cy.wait('@getSurveys');
    activities.forEach((el, i) => {
      cy.wait('@getActivity_' + i);
    })
    //cy.wait('@getRubbishbinQuestionData');

    const checkForResult = (btn) => {
      const classes = btn.attr('class');
      const isActive = classes.search(/passive/) < 0;

      if (toNext && isActive) {
        cy.get('button[name=submit]').click({ force: true });

        cy.wait('@' + method).then((xhr) => expect({"write tests": true}).to.deep.equal(xhr.request.body));
      } else {
        cy.wait(1000);
        cy.get('button[name=submit]').then(checkForResult);
      } 
    }

    cy.get('button[name=submit]').then(checkForResult);
  });
});
