describe('test evening survey', () => {
  const survey = 1674
  const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8'
  const questions = [
                      'emotion'
                    ]
  const query = '&loader=evening' // &loader=evening

  const activity = 'write tests';

  beforeEach(()=>{
    cy.viewport(360, 640)
    cy.server()
    for (let q of questions)
    {
      cy.route(
        'GET',
        `https://localhost:5000/api/survey/${survey}/question/${q}/answered`,
        {answered: false}
      ).as(`get${q}answered`)

      cy.route(
        'PUT',
        `https://localhost:5000/api/survey/${survey}/question/${q}/shown`,
         { shown: false }
      ).as(`put${q}shown`)
    }
  })

  // TODO: move this to the before() function
  it('check survey/index.js', () => {
    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/questions`,
      questions
    ).as(`getQuestions`)

    cy.route(
      'GET',
      `https://localhost:5000/api/survey/${survey}/expired`,
      {expired: false}
    ).as('getSurveyExpired')

    cy.visit(
      `survey?survey=${survey}${query}&token=${token}`
    );

    cy.wait('@getSurveyExpired');
  });

  it('check question', () => {
    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}`,
      response: [{
          'begin': 'Fri, 20 Sep 2019 02:24:40 GMT',
          'end': new Date(),
          'id': survey,
          'measurement': 1,
          'user': 1
        }],
        headers: {
          "token-Authorization": token
        }
      }).as('getDailySurveys');

    cy.route({
      method: 'GET',
      url: `https://localhost:5000/api/survey/${survey}/question/feeling`,
      response: {
        'id': 1,
        'bored': 7,
        'lazy': 7,
        'grateful': 7,
        'productive': 7,
        'sad': 0,
        'stressed': 0,
        'depressed': 0,
        'excited': 0,
        'challenged': 0,
        'frustrated': 0,
        'tired': 0,
        'coerced': 0,
        'bullied': 0,
        'manipulated': 0,
        'anxious': 0,
        'curious': 0,
        'indifferent': 0,
        'apathetic': 0,
        'resentful': 0,
        'important': 0,
        'empathetic': 0,
        'meaningful': 0,
        'responsible': 0,
        'happy': 7,
        'date': '2019-09-20',
        'time': '12:25:42',
        'shown': false,
        'survey': 3
      },
      headers: {
        "token-Authorization": token
      }
    }).as('getQuestionEmotionData');

    const url = `https://localhost:5000/api/survey/${survey}/feedback`;
    const method = 'putQuestionData';

    let isMobile = false;
    let isRotate = true;
    let toNext = false;
    cy.goNext = () => toNext = true;
    cy.rotateView = () => {
      try {
        isRotate = !isRotate;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be rotate soon`)
      }  
    }
    cy.changeView = () => {
      try {
        isMobile = !isMobile;
        if (isMobile) {
          (isRotate) ? cy.viewport(1600, 900) : cy.viewport(900, 1600);
        } else {
          (isRotate) ? cy.viewport(360, 640) : cy.viewport(640, 360);
        }
      } catch (error) {
        console.log(`Viewport will be changed soon`)
      }  
    }

    cy.route({
      method: 'POST',
      url,
      response: {
        'date': 'Fri, 20 Sep 2019 00:00:00 GMT',
        'emotion': 5,
        'fact': 6,
        'id': 1,
        'picture': 'sad.jpg',
        'response': null,
        'tip': 20,
        'user': 1
      },
      headers: {
        "token-Authorization": token
      }
    }).as(method);

    cy.wait(800);
    cy.get('button').click({ force: true });

    cy.wait('@getDailySurveys')
    cy.wait('@getQuestionEmotionData');

    const checkForResult = (btn) => {
      const classes = btn.attr('class');
      const isActive = classes.search(/passive/) < 0;

      if (toNext && isActive) {
        cy.get('button[name=submit]').click({ force: true });

        cy.wait('@' + method);
      } else {
        cy.wait(1000);
        cy.get('button[name=submit]').then(checkForResult);
      } 
    }

    cy.get('button[name=submit]').then(checkForResult);
  });
});
