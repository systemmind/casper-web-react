import { StateBegin, StateMiddle, StateEnd } from "./Templates";

const measurement = 1;
const user = 2;
const survey = 3;
const begin = "1971-01-01";
const end = "2120-12-31";
const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8';

const apiUrl = "https://localhost:5000/api";
const userUrl = `employees/${user}`
const surveyUrl = `/${userUrl}/surveys/${survey}`;
const query = '&loader=singleevening';

describe('test range', () => {
  const question = {id: 4, survey, ordern: null, answered: false, skipped: false, postponed: false};
  const control = {id: 5, value: null, question: 4, ordern: null, name: null, indent: null, timestamp: "2020-08-14T01:53:30"};
  const surveyR = {id: survey, measurement, user, begin, end, template: JSON.stringify(StateBegin)};

  const qControlType = {controlType: "textinput"}
  const qControls = Object.assign(
    { filters:[{name:"ordern",op:"=",value:0},{name:"indent",op:"=",value:`${survey}_0`}] },
    qControlType
  );

  //const qActivities = StateBegin.state.query;
  const activityControls = [
    {id: 5, value: "Dancing", question: 4, ordern: null, name: "activity", indent: null, timestamp: "2020-08-14T01:53:30", "question.order": 0}
  ];

  beforeEach(()=>{
    cy.server()

    cy.route('GET', `${apiUrl}${surveyUrl}`, surveyR).as("getSurvey");
  });

  it('check survey/index.js', () => {
    cy.visit(`survey?user=${user}&survey=${survey}${query}&token=${token}`);
    cy.wait('@getSurvey');
  });

  it('skip intro page', () => {
    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, []).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControls)}`, []).as("getControls");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControlType)}`, {id: control.id}).as("postControls");
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("getControl");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("putControl");

    cy.get('button[name=start]').click();

    cy.wait(`@getQuestions`);
    cy.wait(`@postQuestions`);

    cy.wait(`@getControls`);
    cy.wait(`@postControls`);
    cy.wait(`@getControl`);
  });

  it('check text question', () => {
    alert("click radio button")
    cy.wait(5000);

    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("putControl");

    const qResponse = Object.assign({}, question, {answered: true});
    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, [qResponse]).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    const cType = {controlType: "numberinput"};
    const controlResponse = Object.assign(JSON.parse(JSON.stringify(qControls)), cType);
    controlResponse.filters[1].value = `${survey}_1`;
    controlResponse.controlType = "numberinput";
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(controlResponse)}`, []).as("getControls");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(cType)}`, {id: control.id}).as("postControls");
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(cType)}`, control).as("getControl");

    const qActivities = JSON.parse(JSON.stringify(StateBegin.state.query));
    qActivities.filters[1].value = survey;
    qActivities.filters[2].value = 1;
    cy.route('GET', `${apiUrl}/${userUrl}/controls?q=${JSON.stringify(qActivities)}`, activityControls).as("getActivities");

    cy.get('button[name=submit]').click();
    cy.wait("@putControl");

    cy.wait("@putQuestion");

    cy.wait("@getActivities");

    cy.wait("@getQuestions")
    cy.wait("@postQuestions")
    
    cy.wait("@getControls")
    cy.wait("@postControls")
    cy.wait("@getControl")
  });

  it('check text question', () => {
    alert("change slider")
    cy.wait(5000);

    const cType = {controlType: "numberinput"};
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(cType)}`, control).as("putControl");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    const qActivities = JSON.parse(JSON.stringify(StateBegin.state.query));
    qActivities.filters[1].value = survey;
    qActivities.filters[2].value = 2;
    cy.route('GET', `${apiUrl}/${userUrl}/controls?q=${JSON.stringify(qActivities)}`, activityControls).as("getActivities");

    const qResponse = Object.assign({}, question, {answered: true});
    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, [qResponse, qResponse]).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");

    const controlResponse = Object.assign(JSON.parse(JSON.stringify(qControls)), qControlType);
    controlResponse.filters[1].value = `${survey}_2`;
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(controlResponse)}`, []).as("getControls");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControlType)}`, {id: control.id}).as("postControls");
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("getControl");

    cy.get('button[name=submit]').click();
    cy.wait("@putControl");

    cy.wait("@putQuestion");
    cy.wait("@getQuestions")
    cy.wait("@postQuestions")

    cy.wait("@getControls")
    cy.wait("@postControls")
    cy.wait("@getControl")
  });

  it('check bar progress, it should be changed', () => {
    alert("click radio button")
    cy.wait(5000);

    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("putControl");

    const qResponse = Object.assign({}, question, {answered: true});
    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, [qResponse, qResponse, qResponse]).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    const cType = {controlType: "numberinput"};
    const controlResponse = Object.assign(JSON.parse(JSON.stringify(qControls)), cType);
    controlResponse.filters[1].value = `${survey}_3`;
    controlResponse.controlType = "numberinput";
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(controlResponse)}`, []).as("getControls");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(cType)}`, {id: control.id}).as("postControls");
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(cType)}`, control).as("getControl");

    const qActivities = JSON.parse(JSON.stringify(StateBegin.state.query));
    qActivities.filters[1].value = survey;
    qActivities.filters[2].value = 3;
    const ac1 = activityControls[0];
    const ac2 = Object.assign({}, ac1, {"question.order": 2})
    const actControls = [ac1, ac2]
    cy.route('GET', `${apiUrl}/${userUrl}/controls?q=${JSON.stringify(qActivities)}`, actControls).as("getActivities");

    cy.get('button[name=submit]').click();
    cy.wait("@putControl");

    cy.wait("@putQuestion");

    cy.wait("@getActivities")

    cy.wait("@getQuestions")
    cy.wait("@postQuestions")
    
    cy.wait("@getControls")
    cy.wait("@postControls")
    cy.wait("@getControl")
  });
});
