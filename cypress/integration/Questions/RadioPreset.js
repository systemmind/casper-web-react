import { RadioPreset as Template } from "../SurveyTemplate";

describe('test radio preset', () => {
  const measurement = 1;
  const user = 2;
  const survey = 3;
  const begin = "1971-01-01";
  const end = "2120-12-31";
  const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8';

  const apiUrl = "https://localhost:5000/api";
  const surveyUrl = `/employees/${user}/surveys/${survey}`;
  const query = '&loader=evening';

  const question = {id: 4, survey, ordern: null, answered: false, skipped: false, postponed: false};
  const control = {id: 5, value: null, question: 4, ordern: null, name: null, indent: null, timestamp: "2020-08-14T01:53:30"};
  const surveyR = {id: survey, measurement, user, begin, end, template: JSON.stringify(Template)};

  const qControlType = {controlType: "textinput"}
  const qControls = Object.assign(
    { filters:[{name:"ordern",op:"=",value:0},{name:"indent",op:"=",value:`${survey}_0`}] },
    qControlType
  );

  beforeEach(()=>{
    cy.server()

    cy.route('GET', `${apiUrl}${surveyUrl}`, surveyR).as("getSurvey");

    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, []).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControls)}`, []).as("getControls");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControlType)}`, {id: control.id}).as("postControls");
    cy.route('GET', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("getControl");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/${control.id}?q=${JSON.stringify(qControlType)}`, control).as("putControl");
  });

  it('check survey/index.js', () => {
    cy.visit(`survey?user=${user}&survey=${survey}${query}&token=${token}`);
    cy.wait('@getSurvey');
  });

  it('check radio question record creation', () => {
    cy.get('button[name=submit]').click();

    cy.wait(`@getQuestions`);
    cy.wait(`@postQuestions`);

    cy.wait(`@getControls`);
    cy.wait(`@postControls`);
    cy.wait(`@getControl`);
  });

  it('check radio question modifying', () => {
    alert("select any option or type something to input")
    cy.wait(5000);

    cy.get('button[name=submit]').click();
    cy.wait("@putControl");

    cy.wait("@putQuestion");
  });
});
