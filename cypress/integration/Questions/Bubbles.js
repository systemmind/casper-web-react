import { Bubbles as Template } from "../SurveyTemplate";

describe('test bubbles', () => {
  const measurement = 1;
  const user = 2;
  const survey = 3;
  const begin = "1971-01-01";
  const end = "2120-12-31";
  const token = '545TOY43FRJEZZVWAAJTS5VCAYMFXG3VG5691CSXJKQO216JPP1O16L7LN9STXC0J8I41Q96XTNKXMCWFFHA7A8I0ANPP5TBP9TQO05NN3L1SV8JBOYDYEVBD02HF0X8';

  const apiUrl = "https://localhost:5000/api";
  const userUrl = `/employees/${user}`
  const surveyUrl = `${userUrl}/surveys/${survey}`;
  const query = '&loader=evening';

  const question = {id: 4, survey, ordern: null, answered: false, skipped: false, postponed: false};
  const surveyR = {id: survey, measurement, user, begin, end, template: JSON.stringify(Template)};

  const qControlType = {controlType: "textinput"}

  const dataResponseDeps = [
    {id: 100,  value: "Working"},
    {id: 100, value: "Working"},
    {id: 100, value: "Emailing"},
    {id: 100, value: "Eating"}
  ];

  const dataResponseSelf = [
    {id: 100, value: "Eating"}
  ];

  beforeEach(()=>{
    cy.server();

    cy.route('GET', `${apiUrl}${surveyUrl}`, surveyR).as("getSurvey");

    cy.route('GET', `${apiUrl}${surveyUrl}/questions`, []).as("getQuestions");
    cy.route('POST', `${apiUrl}${surveyUrl}/questions`, {id: question.id}).as("postQuestions");
    cy.route('PUT', `${apiUrl}${surveyUrl}/questions/${question.id}`, {}).as("putQuestion");

    cy.route('POST', `${apiUrl}${surveyUrl}/questions/${question.id}/controls?q=${JSON.stringify(qControlType)}`, {id: 100}).as("postControls");
    cy.route('DELETE', `${apiUrl}${surveyUrl}/questions/${question.id}/controls/100?q=${JSON.stringify(qControlType)}`, {}).as("delControls");

    const today = Cypress.moment().utc().format('YYYY-MM-DD');
    const queryDeps = JSON.parse(JSON.stringify(Template.questions[0].props.options.queries[0]));
    queryDeps.filters[1].value = today;
    cy.route('GET', `${apiUrl}${userUrl}/controls?q=${JSON.stringify(queryDeps)}`, dataResponseDeps).as("pullDepsData");

    const querySelf = JSON.parse(JSON.stringify(Template.questions[0].props.options.queries[1]));
    querySelf.filters[1].value = today;
    cy.route('GET', `${apiUrl}${userUrl}/controls?q=${JSON.stringify(querySelf)}`, dataResponseSelf).as("pullSelfData");
  });

  it('check survey/index.js', () => {
    cy.visit(`survey?user=${user}&survey=${survey}${query}&token=${token}`);
    cy.wait('@getSurvey');
  });

  it('check bubbles question record creation', () => {
    cy.get('button[name=submit]').click();

    cy.wait("@pullDepsData");
    cy.wait("@pullSelfData");
  });

  it('check radio question modifying', () => {
    alert("select any 2 bubbles and then unselect them");
    cy.wait("@postControls");
    cy.wait("@postControls");
    cy.wait("@delControls");
    cy.wait("@delControls");

    cy.wait(500);
    cy.get('button[name=submit]').click();

    cy.wait("@putQuestion");
  });
});
