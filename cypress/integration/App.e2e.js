describe('test admin', () => {

  it('check index', () => {
    cy.server()
    cy.route('POST', 'https://localhost:5000/api/login', {token: "abcd"}).as('getLogin')

    cy.visit('https://localhost:8080/admin');

    cy.get('h3')
      .should('have.text', 'Please, login as Administrator');

    cy.get('button')
      .should('have.text', 'Login');

    cy.get('input[name=login]')
      .type("user")
      .should('have.value', 'user')
    cy.get('input[name=password]')
      .type("password")
      .should('have.value', 'password')
    cy.get('button').click()

    cy.wait('@getLogin').then((xhr) => (
      expect({login: "user", password: "password"}).to.deep.equal(xhr.request.body)
    ))

    cy.url().should('include', '/measurement')

    cy.contains('Add').click()

    cy.get('input[name=name]')
      .type("mock name")
      .should('have.value', 'mock name')

    cy.get('input[name=start_date]')
      .type("2019-01-24")
      .should('have.value', '2019-01-24')

    cy.get('input[name=stop_date]')
      .type("2019-11-22")
      .should('have.value', '2019-11-22')

    cy.contains('Create').click()

    cy.contains('inactive')
    cy.contains('Start').click()
    cy.contains('ACTIVE')

  });

  it('check login', () => {
    cy.visit('https://localhost:8080/admin');

    cy.get('input[name=login]')
      .type("admin")
      .should('have.value', 'admin')
    cy.get('input[name=password]')
      .type("rSsmp#1")
      .should('have.value', 'rSsmp#1')
    cy.get('button').click()

    cy.url().should('include', '/measurement')

  });

  it('check add new measurement', () => {

    cy.contains('Sonas-')

    cy.contains('Add').click()

    cy.get('input[name=name]')
      .type("mock name")
      .should('have.value', 'mock name')

    cy.get('input[name=start_date]')
      .type("2019-01-24")
      .should('have.value', '2019-01-24')

    cy.get('input[name=stop_date]')
      .type("2019-11-22")
      .should('have.value', '2019-11-22')

    cy.contains('Create').click()

    cy.contains('mock name')

    cy.contains('mock name')
    cy.contains('Start').click()

  });

  it('check start-stop', () => {

    cy.contains('inactive')
    cy.contains('Start').click()
    cy.contains('ACTIVE')

  });

});
